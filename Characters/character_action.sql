/*
Navicat MySQL Data Transfer

Source Server         : MySQL
Source Server Version : 50153
Source Host           : localhost:3306
Source Database       : character

Target Server Type    : MYSQL
Target Server Version : 50153
File Encoding         : 65001

Date: 2011-11-30 14:45:02
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `character_action`
-- ----------------------------
DROP TABLE IF EXISTS `character_action`;
CREATE TABLE `character_action` (
  `guid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'Global Unique Identifier',
  `button` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `action` int(11) unsigned NOT NULL DEFAULT '0',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`guid`,`button`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='Player System';

-- ----------------------------
-- Records of character_action
-- ----------------------------
INSERT INTO `character_action` VALUES ('1', '0', '6603', '0');
INSERT INTO `character_action` VALUES ('1', '1', '2973', '0');
INSERT INTO `character_action` VALUES ('1', '2', '75', '0');
INSERT INTO `character_action` VALUES ('1', '3', '20580', '0');
INSERT INTO `character_action` VALUES ('1', '10', '159', '128');
INSERT INTO `character_action` VALUES ('1', '11', '117', '128');
INSERT INTO `character_action` VALUES ('2', '0', '6603', '0');
INSERT INTO `character_action` VALUES ('2', '1', '27209', '0');
INSERT INTO `character_action` VALUES ('2', '2', '696', '0');
INSERT INTO `character_action` VALUES ('2', '10', '159', '128');
INSERT INTO `character_action` VALUES ('2', '11', '4604', '128');
