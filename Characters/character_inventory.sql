/*
Navicat MySQL Data Transfer

Source Server         : MySQL
Source Server Version : 50153
Source Host           : localhost:3306
Source Database       : character

Target Server Type    : MYSQL
Target Server Version : 50153
File Encoding         : 65001

Date: 2011-11-30 14:45:36
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `character_inventory`
-- ----------------------------
DROP TABLE IF EXISTS `character_inventory`;
CREATE TABLE `character_inventory` (
  `guid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'Global Unique Identifier',
  `bag` int(11) unsigned NOT NULL DEFAULT '0',
  `slot` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `item` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'Item Global Unique Identifier',
  `item_template` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'Item Identifier',
  PRIMARY KEY (`item`),
  KEY `idx_guid` (`guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='Player System';

-- ----------------------------
-- Records of character_inventory
-- ----------------------------
INSERT INTO `character_inventory` VALUES ('1', '0', '15', '2', '2092');
INSERT INTO `character_inventory` VALUES ('1', '0', '3', '4', '148');
INSERT INTO `character_inventory` VALUES ('1', '0', '6', '6', '147');
INSERT INTO `character_inventory` VALUES ('1', '0', '7', '8', '129');
INSERT INTO `character_inventory` VALUES ('1', '0', '23', '10', '159');
INSERT INTO `character_inventory` VALUES ('1', '0', '17', '12', '2504');
INSERT INTO `character_inventory` VALUES ('1', '0', '19', '14', '2101');
INSERT INTO `character_inventory` VALUES ('1', '14', '0', '16', '2512');
INSERT INTO `character_inventory` VALUES ('1', '0', '24', '18', '117');
INSERT INTO `character_inventory` VALUES ('1', '0', '25', '20', '6948');
INSERT INTO `character_inventory` VALUES ('2', '0', '4', '22', '57');
INSERT INTO `character_inventory` VALUES ('2', '0', '3', '24', '6097');
INSERT INTO `character_inventory` VALUES ('2', '0', '6', '26', '1396');
INSERT INTO `character_inventory` VALUES ('2', '0', '15', '28', '2092');
INSERT INTO `character_inventory` VALUES ('2', '0', '7', '30', '59');
INSERT INTO `character_inventory` VALUES ('2', '0', '23', '32', '4604');
INSERT INTO `character_inventory` VALUES ('2', '0', '24', '34', '159');
INSERT INTO `character_inventory` VALUES ('2', '0', '25', '36', '6948');
INSERT INTO `character_inventory` VALUES ('2', '0', '26', '37', '28071');
INSERT INTO `character_inventory` VALUES ('2', '0', '27', '38', '28072');
INSERT INTO `character_inventory` VALUES ('2', '0', '28', '39', '28073');
INSERT INTO `character_inventory` VALUES ('2', '0', '29', '40', '25469');
INSERT INTO `character_inventory` VALUES ('2', '0', '30', '41', '16322');
INSERT INTO `character_inventory` VALUES ('2', '0', '31', '42', '16321');
INSERT INTO `character_inventory` VALUES ('2', '0', '32', '43', '16323');
