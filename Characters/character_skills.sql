/*
Navicat MySQL Data Transfer

Source Server         : MySQL
Source Server Version : 50153
Source Host           : localhost:3306
Source Database       : character

Target Server Type    : MYSQL
Target Server Version : 50153
File Encoding         : 65001

Date: 2011-11-30 14:46:05
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `character_skills`
-- ----------------------------
DROP TABLE IF EXISTS `character_skills`;
CREATE TABLE `character_skills` (
  `guid` int(11) unsigned NOT NULL COMMENT 'Global Unique Identifier',
  `skill` mediumint(9) unsigned NOT NULL,
  `value` mediumint(9) unsigned NOT NULL,
  `max` mediumint(9) unsigned NOT NULL,
  PRIMARY KEY (`guid`,`skill`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='Player System';

-- ----------------------------
-- Records of character_skills
-- ----------------------------
INSERT INTO `character_skills` VALUES ('1', '45', '1', '5');
INSERT INTO `character_skills` VALUES ('1', '51', '1', '1');
INSERT INTO `character_skills` VALUES ('1', '95', '1', '5');
INSERT INTO `character_skills` VALUES ('1', '98', '300', '300');
INSERT INTO `character_skills` VALUES ('1', '113', '300', '300');
INSERT INTO `character_skills` VALUES ('1', '162', '1', '5');
INSERT INTO `character_skills` VALUES ('1', '163', '1', '1');
INSERT INTO `character_skills` VALUES ('1', '173', '1', '5');
INSERT INTO `character_skills` VALUES ('1', '414', '1', '1');
INSERT INTO `character_skills` VALUES ('1', '415', '1', '1');
INSERT INTO `character_skills` VALUES ('2', '95', '1', '350');
INSERT INTO `character_skills` VALUES ('2', '98', '300', '300');
INSERT INTO `character_skills` VALUES ('2', '162', '1', '350');
INSERT INTO `character_skills` VALUES ('2', '173', '1', '350');
INSERT INTO `character_skills` VALUES ('2', '228', '1', '350');
INSERT INTO `character_skills` VALUES ('2', '354', '1', '1');
INSERT INTO `character_skills` VALUES ('2', '415', '1', '1');
INSERT INTO `character_skills` VALUES ('2', '593', '1', '1');
INSERT INTO `character_skills` VALUES ('2', '762', '75', '75');
