/*
Navicat MySQL Data Transfer

Source Server         : MySQL
Source Server Version : 50153
Source Host           : localhost:3306
Source Database       : character

Target Server Type    : MYSQL
Target Server Version : 50153
File Encoding         : 65001

Date: 2011-11-30 14:46:15
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `character_spell`
-- ----------------------------
DROP TABLE IF EXISTS `character_spell`;
CREATE TABLE `character_spell` (
  `guid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'Global Unique Identifier',
  `spell` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'Spell Identifier',
  `active` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `disabled` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`guid`,`spell`),
  KEY `Idx_spell` (`spell`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='Player System';

-- ----------------------------
-- Records of character_spell
-- ----------------------------
INSERT INTO `character_spell` VALUES ('2', '126', '1', '0');
INSERT INTO `character_spell` VALUES ('2', '132', '1', '0');
INSERT INTO `character_spell` VALUES ('2', '696', '1', '0');
INSERT INTO `character_spell` VALUES ('2', '698', '1', '0');
INSERT INTO `character_spell` VALUES ('2', '5500', '1', '0');
INSERT INTO `character_spell` VALUES ('2', '5697', '1', '0');
INSERT INTO `character_spell` VALUES ('2', '5784', '1', '0');
INSERT INTO `character_spell` VALUES ('2', '6215', '1', '0');
INSERT INTO `character_spell` VALUES ('2', '11719', '1', '0');
INSERT INTO `character_spell` VALUES ('2', '11726', '1', '0');
INSERT INTO `character_spell` VALUES ('2', '17928', '1', '0');
INSERT INTO `character_spell` VALUES ('2', '18647', '1', '0');
INSERT INTO `character_spell` VALUES ('2', '27209', '1', '0');
INSERT INTO `character_spell` VALUES ('2', '27212', '1', '0');
INSERT INTO `character_spell` VALUES ('2', '27213', '1', '0');
INSERT INTO `character_spell` VALUES ('2', '27215', '1', '0');
INSERT INTO `character_spell` VALUES ('2', '27216', '1', '0');
INSERT INTO `character_spell` VALUES ('2', '27217', '1', '0');
INSERT INTO `character_spell` VALUES ('2', '27218', '1', '0');
INSERT INTO `character_spell` VALUES ('2', '27220', '1', '0');
INSERT INTO `character_spell` VALUES ('2', '27222', '1', '0');
INSERT INTO `character_spell` VALUES ('2', '27223', '1', '0');
INSERT INTO `character_spell` VALUES ('2', '27226', '1', '0');
INSERT INTO `character_spell` VALUES ('2', '27228', '1', '0');
INSERT INTO `character_spell` VALUES ('2', '27230', '1', '0');
INSERT INTO `character_spell` VALUES ('2', '27238', '1', '0');
INSERT INTO `character_spell` VALUES ('2', '27243', '1', '0');
INSERT INTO `character_spell` VALUES ('2', '27250', '1', '0');
INSERT INTO `character_spell` VALUES ('2', '27259', '1', '0');
INSERT INTO `character_spell` VALUES ('2', '27260', '1', '0');
INSERT INTO `character_spell` VALUES ('2', '28172', '1', '0');
INSERT INTO `character_spell` VALUES ('2', '28189', '1', '0');
INSERT INTO `character_spell` VALUES ('2', '28610', '1', '0');
INSERT INTO `character_spell` VALUES ('2', '29858', '1', '0');
INSERT INTO `character_spell` VALUES ('2', '29893', '1', '0');
INSERT INTO `character_spell` VALUES ('2', '30459', '1', '0');
INSERT INTO `character_spell` VALUES ('2', '30545', '1', '0');
INSERT INTO `character_spell` VALUES ('2', '30908', '1', '0');
INSERT INTO `character_spell` VALUES ('2', '30909', '1', '0');
INSERT INTO `character_spell` VALUES ('2', '30910', '1', '0');
INSERT INTO `character_spell` VALUES ('2', '32231', '1', '0');
