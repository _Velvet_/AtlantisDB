/*
Navicat MySQL Data Transfer

Source Server         : MySQL
Source Server Version : 50153
Source Host           : localhost:3306
Source Database       : character

Target Server Type    : MYSQL
Target Server Version : 50153
File Encoding         : 65001

Date: 2011-11-30 14:47:54
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `instance_reset`
-- ----------------------------
DROP TABLE IF EXISTS `instance_reset`;
CREATE TABLE `instance_reset` (
  `mapid` int(11) unsigned NOT NULL DEFAULT '0',
  `resettime` bigint(40) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`mapid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of instance_reset
-- ----------------------------
INSERT INTO `instance_reset` VALUES ('249', '1322971200');
INSERT INTO `instance_reset` VALUES ('269', '1322712000');
INSERT INTO `instance_reset` VALUES ('309', '1322798400');
INSERT INTO `instance_reset` VALUES ('409', '1323144000');
INSERT INTO `instance_reset` VALUES ('469', '1323144000');
INSERT INTO `instance_reset` VALUES ('509', '1322798400');
INSERT INTO `instance_reset` VALUES ('531', '1323144000');
INSERT INTO `instance_reset` VALUES ('532', '1323144000');
INSERT INTO `instance_reset` VALUES ('533', '1323144000');
INSERT INTO `instance_reset` VALUES ('534', '1323144000');
INSERT INTO `instance_reset` VALUES ('540', '1322712000');
INSERT INTO `instance_reset` VALUES ('542', '1322712000');
INSERT INTO `instance_reset` VALUES ('543', '1322712000');
INSERT INTO `instance_reset` VALUES ('544', '1323144000');
INSERT INTO `instance_reset` VALUES ('545', '1322712000');
INSERT INTO `instance_reset` VALUES ('546', '1322712000');
INSERT INTO `instance_reset` VALUES ('547', '1322712000');
INSERT INTO `instance_reset` VALUES ('548', '1323144000');
INSERT INTO `instance_reset` VALUES ('550', '1323144000');
INSERT INTO `instance_reset` VALUES ('552', '1322712000');
INSERT INTO `instance_reset` VALUES ('553', '1322712000');
INSERT INTO `instance_reset` VALUES ('554', '1322712000');
INSERT INTO `instance_reset` VALUES ('555', '1322712000');
INSERT INTO `instance_reset` VALUES ('556', '1322712000');
INSERT INTO `instance_reset` VALUES ('557', '1322712000');
INSERT INTO `instance_reset` VALUES ('558', '1322712000');
INSERT INTO `instance_reset` VALUES ('560', '1322712000');
INSERT INTO `instance_reset` VALUES ('564', '1323144000');
INSERT INTO `instance_reset` VALUES ('565', '1323144000');
INSERT INTO `instance_reset` VALUES ('568', '1322798400');
INSERT INTO `instance_reset` VALUES ('580', '1323144000');
INSERT INTO `instance_reset` VALUES ('585', '1322712000');
