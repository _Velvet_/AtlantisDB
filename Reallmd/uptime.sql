/*
Navicat MySQL Data Transfer

Source Server         : MySQL
Source Server Version : 50153
Source Host           : localhost:3306
Source Database       : realmd

Target Server Type    : MYSQL
Target Server Version : 50153
File Encoding         : 65001

Date: 2011-11-30 14:49:41
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `uptime`
-- ----------------------------
DROP TABLE IF EXISTS `uptime`;
CREATE TABLE `uptime` (
  `realmid` int(11) unsigned NOT NULL,
  `starttime` bigint(20) unsigned NOT NULL DEFAULT '0',
  `startstring` varchar(64) NOT NULL DEFAULT '',
  `uptime` bigint(20) unsigned NOT NULL DEFAULT '0',
  `maxplayers` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`realmid`,`starttime`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='Uptime system';

-- ----------------------------
-- Records of uptime
-- ----------------------------
INSERT INTO `uptime` VALUES ('1', '1322607402', '2011-11-30 00:56:42', '601', '0');
INSERT INTO `uptime` VALUES ('1', '1322609752', '2011-11-30 01:35:52', '0', '0');
INSERT INTO `uptime` VALUES ('1', '1322610511', '2011-11-30 01:48:31', '0', '0');
INSERT INTO `uptime` VALUES ('1', '1322611471', '2011-11-30 02:04:31', '0', '0');
INSERT INTO `uptime` VALUES ('1', '1322619672', '2011-11-30 04:21:12', '0', '0');
INSERT INTO `uptime` VALUES ('1', '1322652774', '2011-11-30 13:32:54', '1201', '0');
