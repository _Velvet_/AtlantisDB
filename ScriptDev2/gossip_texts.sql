/*
Navicat MySQL Data Transfer

Source Server         : Hometown
Source Server Version : 50523
Source Host           : localhost:3306
Source Database       : scriptdev2

Target Server Type    : MYSQL
Target Server Version : 50523
File Encoding         : 65001

Date: 2012-11-24 09:12:06
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `gossip_texts`
-- ----------------------------
DROP TABLE IF EXISTS `gossip_texts`;
CREATE TABLE `gossip_texts` (
  `entry` mediumint(8) NOT NULL,
  `content_default` text NOT NULL,
  `content_loc1` text,
  `content_loc2` text,
  `content_loc3` text,
  `content_loc4` text,
  `content_loc5` text,
  `content_loc6` text,
  `content_loc7` text,
  `content_loc8` text,
  `comment` text,
  PRIMARY KEY (`entry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='Gossip Texts';

-- ----------------------------
-- Records of gossip_texts
-- ----------------------------
INSERT INTO `gossip_texts` VALUES ('-3000000', '[PH] SD2 unknown text', null, null, null, null, null, null, null, null, 'GOSSIP_ID_UNKNOWN_TEXT');
INSERT INTO `gossip_texts` VALUES ('-3000105', 'Ezekiel said that you might have a certain book...', null, null, null, null, null, null, null, null, 'dirty larry GOSSIP_ITEM_BOOK');
INSERT INTO `gossip_texts` VALUES ('-3033000', 'Please unlock the courtyard door.', null, null, null, null, null, null, null, null, 'deathstalker adamant/ sorcerer ashcrombe - GOSSIP_ITEM_DOOR');
INSERT INTO `gossip_texts` VALUES ('-3043000', 'Let the event begin!', null, null, null, null, null, null, null, null, 'Disciple of Naralex - GOSSIP_ITEM_BEGIN');
INSERT INTO `gossip_texts` VALUES ('-3090000', 'I am ready to begin.', null, null, null, null, null, null, null, null, 'emi shortfuse GOSSIP_ITEM_START');
INSERT INTO `gossip_texts` VALUES ('-3409000', 'Tell me more.', null, null, null, null, null, null, null, null, 'majordomo_executus GOSSIP_ITEM_SUMMON_1');
INSERT INTO `gossip_texts` VALUES ('-3409001', 'What else do you have to say?', null, null, null, null, null, null, null, null, 'majordomo_executus GOSSIP_ITEM_SUMMON_2');
INSERT INTO `gossip_texts` VALUES ('-3409002', 'You challenged us and we have come. Where is this master you speak of?', null, null, null, null, null, null, null, null, 'majordomo_executus GOSSIP_ITEM_SUMMON_3');
INSERT INTO `gossip_texts` VALUES ('-3469000', 'I\'ve made no mistakes.', null, null, null, null, null, null, null, null, 'victor_nefarius GOSSIP_ITEM_NEFARIUS_1');
INSERT INTO `gossip_texts` VALUES ('-3469001', 'You have lost your mind, Nefarius. You speak in riddles.', null, null, null, null, null, null, null, null, 'victor_nefarius GOSSIP_ITEM_NEFARIUS_2');
INSERT INTO `gossip_texts` VALUES ('-3469002', 'Please do.', null, null, null, null, null, null, null, null, 'victor_nefarius GOSSIP_ITEM_NEFARIUS_3');
INSERT INTO `gossip_texts` VALUES ('-3469003', 'I cannot, Vaelastrasz! Surely something can be done to heal you!', null, null, null, null, null, null, null, null, 'vaelastrasz GOSSIP_ITEM_VAEL_1');
INSERT INTO `gossip_texts` VALUES ('-3469004', 'Vaelastrasz, no!!!', null, null, null, null, null, null, null, null, 'vaelastrasz GOSSIP_ITEM_VAEL_2');
INSERT INTO `gossip_texts` VALUES ('-3560000', 'We are ready to get you out of here, Thrall. Let\'s go!', null, null, null, null, null, null, null, null, 'thrall GOSSIP_ITEM_START');
INSERT INTO `gossip_texts` VALUES ('-3000106', 'Let Marshal Windsor know that I am ready.', null, null, null, null, null, null, null, null, 'squire rowe GOSSIP_ITEM_WINDSOR');
INSERT INTO `gossip_texts` VALUES ('-3560001', 'I need a pack of Incendiary Bombs.', null, null, null, null, null, null, null, null, 'erozion GOSSIP_ITEM_NEED_BOMBS');
INSERT INTO `gossip_texts` VALUES ('-3560002', 'Taretha cannot see you, Thrall.', null, null, null, null, null, null, null, null, 'thrall GOSSIP_ITEM_SKARLOC1');
INSERT INTO `gossip_texts` VALUES ('-3560003', 'The situation is rather complicated, Thrall. It would be best for you to head into the mountains now, before more of Blackmoore\'s men show up. We\'ll make sure Taretha is safe.', null, null, null, null, null, null, null, null, 'thrall GOSSIP_ITEM_SKARLOC2');
INSERT INTO `gossip_texts` VALUES ('-3560004', 'We\'re ready, Thrall.', null, null, null, null, null, null, null, null, 'thrall GOSSIP_ITEM_TARREN');
INSERT INTO `gossip_texts` VALUES ('-3560005', 'Strange wizard?', null, null, null, null, null, null, null, null, 'taretha GOSSIP_ITEM_EPOCH1');
INSERT INTO `gossip_texts` VALUES ('-3560006', 'We\'ll get you out. Taretha. Don\'t worry. I doubt the wizard would wander too far away.', null, null, null, null, null, null, null, null, 'taretha GOSSIP_ITEM_EPOCH2');
INSERT INTO `gossip_texts` VALUES ('-3649001', 'Bring forth the first challenge!', null, null, null, null, null, null, null, null, 'barrett GOSSIP_ITEM_BEAST_START');
INSERT INTO `gossip_texts` VALUES ('-3649002', 'We want another shot at those beasts!', null, null, null, null, null, null, null, null, 'barrett GOSSIP_ITEM_BEAST_WIPE_INIT');
INSERT INTO `gossip_texts` VALUES ('-3649003', 'What new challenge awaits us?', null, null, null, null, null, null, null, null, 'barrett GOSSIP_ITEM_JARAXXUS_INIT');
INSERT INTO `gossip_texts` VALUES ('-3649004', 'We\'re ready to fight the sorceror again.', null, null, null, null, null, null, null, null, 'barrett GOSSIP_ITEM_JARAXXUS_WIPE_INIT');
INSERT INTO `gossip_texts` VALUES ('-3649005', 'Of course!', null, null, null, null, null, null, null, null, 'barrett GOSSIP_ITEM_PVP_INIT');
INSERT INTO `gossip_texts` VALUES ('-3649006', 'Give the signal! We\'re ready to go!', null, null, null, null, null, null, null, null, 'barrett GOSSIP_ITEM_PVP_START');
INSERT INTO `gossip_texts` VALUES ('-3649007', 'That tough, huh?', null, null, null, null, null, null, null, null, 'barrett GOSSIP_ITEM_TWINS_INIT');
INSERT INTO `gossip_texts` VALUES ('-3649008', 'Val\'kyr? We\'re ready for them', null, null, null, null, null, null, null, null, 'barrett GOSSIP_ITEM_TWINS_START');
INSERT INTO `gossip_texts` VALUES ('-3649009', 'Your words of praise are appreciated, Coliseum Master.', null, null, null, null, null, null, null, null, 'barrett GOSSIP_ITEM_ANUB_INIT');
INSERT INTO `gossip_texts` VALUES ('-3649010', 'That is strange...', null, null, null, null, null, null, null, null, 'barrett GOSSIP_ITEM_ANUB_START');
INSERT INTO `gossip_texts` VALUES ('-3000107', 'I am ready, as are my forces. Let us end this masquerade!', null, null, null, null, null, null, null, null, 'reginald windsor GOSSIP_ITEM_START');
INSERT INTO `gossip_texts` VALUES ('-3509000', 'Let\'s find out.', null, null, null, null, null, null, null, null, 'andorov GOSSIP_ITEM_START');
INSERT INTO `gossip_texts` VALUES ('-3509001', 'Let\'s see what you have.', null, null, null, null, null, null, null, null, 'andorov GOSSIP_ITEM_TRADE');
INSERT INTO `gossip_texts` VALUES ('-3534000', 'My companions and I are with you, Lady Proudmoore.', null, null, null, null, null, null, null, null, 'jaina GOSSIP_ITEM_JAIN_START');
INSERT INTO `gossip_texts` VALUES ('-3534001', 'We are ready for whatever Archimonde might send our way, Lady Proudmoore.', null, null, null, null, null, null, null, null, 'jaina GOSSIP_ITEM_ANATHERON');
INSERT INTO `gossip_texts` VALUES ('-3534002', 'Until we meet again, Lady Proudmoore.', null, null, null, null, null, null, null, null, 'jaina GOSSIP_ITEM_SUCCESS');
INSERT INTO `gossip_texts` VALUES ('-3534003', 'I am with you, Thrall.', null, null, null, null, null, null, null, null, 'thrall GOSSIP_ITEM_THRALL_START');
INSERT INTO `gossip_texts` VALUES ('-3534004', 'We have nothing to fear.', null, null, null, null, null, null, null, null, 'thrall GOSSIP_ITEM_AZGALOR');
INSERT INTO `gossip_texts` VALUES ('-3534005', 'Until we meet again, Thrall.', null, null, null, null, null, null, null, null, 'thrall GOSSIP_ITEM_SUCCESS');
INSERT INTO `gossip_texts` VALUES ('-3534006', 'I would be grateful for any aid you can provide, Priestess.', null, null, null, null, null, null, null, null, 'tyrande GOSSIP_ITEM_AID');
INSERT INTO `gossip_texts` VALUES ('-3000108', 'I need a moment of your time, sir.', null, null, null, null, null, null, null, null, 'prospector anvilward GOSSIP_ITEM_MOMENT');
INSERT INTO `gossip_texts` VALUES ('-3000110', 'Why... yes, of course. I\'ve something to show you right inside this building, Mr. Anvilward.', null, null, null, null, null, null, null, null, 'prospector anvilward GOSSIP_ITEM_SHOW');
INSERT INTO `gossip_texts` VALUES ('-3230000', 'You\'re free, Dughal! Get out of here!', null, null, null, null, null, null, null, null, 'dughal GOSSIP_ITEM');
INSERT INTO `gossip_texts` VALUES ('-3230001', 'Get out of here, Tobias, you\'re free!', null, null, null, null, null, null, null, null, 'tobias GOSSIP_ITEM');
INSERT INTO `gossip_texts` VALUES ('-3230002', 'Your bondage is at an end, Doom\'rel. I challenge you!', null, null, null, null, null, null, null, null, 'doomrel GOSSIP_ITEM_CHALLENGE');
INSERT INTO `gossip_texts` VALUES ('-3560007', 'Tarren Mill.', null, null, null, null, null, null, null, null, 'thrall GOSSIP_ITEM_TARREN_1');
INSERT INTO `gossip_texts` VALUES ('-3532000', 'Teleport me to the Guardian\'s Library', null, null, null, null, null, null, null, null, 'berthold GOSSIP_ITEM_TELEPORT');
INSERT INTO `gossip_texts` VALUES ('-3532001', 'I\'m not an actor.', null, null, null, null, null, null, null, null, 'barnes GOSSIP_ITEM_OPERA_1');
INSERT INTO `gossip_texts` VALUES ('-3532002', 'Ok, I\'ll give it a try, then.', null, null, null, null, null, null, null, null, 'barnes GOSSIP_ITEM_OPERA_2');
INSERT INTO `gossip_texts` VALUES ('-3532003', 'I\'ve never been more ready.', null, null, null, null, null, null, null, null, 'barnes GOSSIP_ITEM_OPERA_JULIANNE_WIPE');
INSERT INTO `gossip_texts` VALUES ('-3532004', 'The wolf\'s going down.', null, null, null, null, null, null, null, null, 'barnes GOSSIP_ITEM_OPERA_WOLF_WIPE');
INSERT INTO `gossip_texts` VALUES ('-3532005', 'What phat lewtz you have grandmother?', null, null, null, null, null, null, null, null, 'grandma GOSSIP_ITEM_GRANDMA');
INSERT INTO `gossip_texts` VALUES ('-3564000', 'I\'m with you, Akama.', null, null, null, null, null, null, null, null, 'akama(shade) GOSSIP_ITEM_START_ENCOUNTER');
INSERT INTO `gossip_texts` VALUES ('-3564001', 'I\'m ready, Akama.', null, null, null, null, null, null, null, null, 'akama(illidan) GOSSIP_ITEM_PREPARE');
INSERT INTO `gossip_texts` VALUES ('-3564002', 'We\'re ready to face Illidan.', null, null, null, null, null, null, null, null, 'akama(illidan) GOSSIP_ITEM_START_EVENT');
