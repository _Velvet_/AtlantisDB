/*
Navicat MySQL Data Transfer

Source Server         : Hometown
Source Server Version : 50523
Source Host           : localhost:3306
Source Database       : mangos

Target Server Type    : MYSQL
Target Server Version : 50523
File Encoding         : 65001

Date: 2013-09-26 18:44:30
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `areatrigger_involvedrelation`
-- ----------------------------
DROP TABLE IF EXISTS `areatrigger_involvedrelation`;
CREATE TABLE `areatrigger_involvedrelation` (
  `id` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT 'Identifier',
  `quest` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT 'Quest Identifier',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='Trigger System';

-- ----------------------------
-- Records of areatrigger_involvedrelation
-- ----------------------------
INSERT INTO `areatrigger_involvedrelation` VALUES ('78', '155');
INSERT INTO `areatrigger_involvedrelation` VALUES ('88', '62');
INSERT INTO `areatrigger_involvedrelation` VALUES ('98', '201');
INSERT INTO `areatrigger_involvedrelation` VALUES ('196', '578');
INSERT INTO `areatrigger_involvedrelation` VALUES ('197', '62');
INSERT INTO `areatrigger_involvedrelation` VALUES ('216', '870');
INSERT INTO `areatrigger_involvedrelation` VALUES ('225', '944');
INSERT INTO `areatrigger_involvedrelation` VALUES ('230', '954');
INSERT INTO `areatrigger_involvedrelation` VALUES ('302', '1265');
INSERT INTO `areatrigger_involvedrelation` VALUES ('87', '76');
INSERT INTO `areatrigger_involvedrelation` VALUES ('362', '1448');
INSERT INTO `areatrigger_involvedrelation` VALUES ('482', '1699');
INSERT INTO `areatrigger_involvedrelation` VALUES ('1205', '2989');
INSERT INTO `areatrigger_involvedrelation` VALUES ('2327', '4842');
INSERT INTO `areatrigger_involvedrelation` VALUES ('2486', '4811');
INSERT INTO `areatrigger_involvedrelation` VALUES ('2946', '6421');
INSERT INTO `areatrigger_involvedrelation` VALUES ('3986', '8286');
INSERT INTO `areatrigger_involvedrelation` VALUES ('4301', '9786');
INSERT INTO `areatrigger_involvedrelation` VALUES ('168', '287');
INSERT INTO `areatrigger_involvedrelation` VALUES ('3367', '6025');
INSERT INTO `areatrigger_involvedrelation` VALUES ('235', '984');
INSERT INTO `areatrigger_involvedrelation` VALUES ('169', '287');
INSERT INTO `areatrigger_involvedrelation` VALUES ('4071', '9193');
INSERT INTO `areatrigger_involvedrelation` VALUES ('4200', '9607');
INSERT INTO `areatrigger_involvedrelation` VALUES ('4201', '9608');
INSERT INTO `areatrigger_involvedrelation` VALUES ('4291', '9701');
INSERT INTO `areatrigger_involvedrelation` VALUES ('4293', '9716');
INSERT INTO `areatrigger_involvedrelation` VALUES ('4298', '9731');
INSERT INTO `areatrigger_involvedrelation` VALUES ('4300', '9752');
INSERT INTO `areatrigger_involvedrelation` VALUES ('1388', '3505');
INSERT INTO `areatrigger_involvedrelation` VALUES ('4473', '10269');
INSERT INTO `areatrigger_involvedrelation` VALUES ('4475', '10275');
