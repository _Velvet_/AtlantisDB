/*
Navicat MySQL Data Transfer

Source Server         : Hometown
Source Server Version : 50523
Source Host           : localhost:3306
Source Database       : mangos

Target Server Type    : MYSQL
Target Server Version : 50523
File Encoding         : 65001

Date: 2013-09-26 18:44:38
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `areatrigger_tavern`
-- ----------------------------
DROP TABLE IF EXISTS `areatrigger_tavern`;
CREATE TABLE `areatrigger_tavern` (
  `id` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT 'Identifier',
  `name` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='Trigger System';

-- ----------------------------
-- Records of areatrigger_tavern
-- ----------------------------
INSERT INTO `areatrigger_tavern` VALUES ('71', 'Westfall - Sentinel Hill Inn');
INSERT INTO `areatrigger_tavern` VALUES ('4375', 'Garadar Inn');
INSERT INTO `areatrigger_tavern` VALUES ('178', 'Strahnbrad');
INSERT INTO `areatrigger_tavern` VALUES ('562', 'Lion\'s Pride Inn');
INSERT INTO `areatrigger_tavern` VALUES ('682', 'Lakeshire Inn');
INSERT INTO `areatrigger_tavern` VALUES ('707', 'Scarlet Raven Tavern');
INSERT INTO `areatrigger_tavern` VALUES ('708', 'Southshore');
INSERT INTO `areatrigger_tavern` VALUES ('710', 'Thunderbrew Distillery');
INSERT INTO `areatrigger_tavern` VALUES ('712', 'Southlager Inn');
INSERT INTO `areatrigger_tavern` VALUES ('713', 'Deepwater Tavern');
INSERT INTO `areatrigger_tavern` VALUES ('719', 'Gallows\' End Tavern');
INSERT INTO `areatrigger_tavern` VALUES ('721', 'Tarren Mill');
INSERT INTO `areatrigger_tavern` VALUES ('844', 'Stonard');
INSERT INTO `areatrigger_tavern` VALUES ('862', 'Booty Bay');
INSERT INTO `areatrigger_tavern` VALUES ('1042', 'Wildhammer Keep');
INSERT INTO `areatrigger_tavern` VALUES ('1606', 'Kargath');
INSERT INTO `areatrigger_tavern` VALUES ('1646', 'Hammerfall');
INSERT INTO `areatrigger_tavern` VALUES ('3690', 'Revantusk Village');
INSERT INTO `areatrigger_tavern` VALUES ('3886', 'Grom\'gol Base Camp');
INSERT INTO `areatrigger_tavern` VALUES ('4058', 'Light\'s Hope Chapel');
INSERT INTO `areatrigger_tavern` VALUES ('709', 'Theramore Isle');
INSERT INTO `areatrigger_tavern` VALUES ('715', 'Dolanaar');
INSERT INTO `areatrigger_tavern` VALUES ('716', 'Auberdine');
INSERT INTO `areatrigger_tavern` VALUES ('717', 'Astranaar');
INSERT INTO `areatrigger_tavern` VALUES ('722', 'Bloodhoof Village');
INSERT INTO `areatrigger_tavern` VALUES ('742', 'The Crossroads');
INSERT INTO `areatrigger_tavern` VALUES ('743', 'Ratchet');
INSERT INTO `areatrigger_tavern` VALUES ('843', 'Razor Hill');
INSERT INTO `areatrigger_tavern` VALUES ('982', 'Camp Taurajo');
INSERT INTO `areatrigger_tavern` VALUES ('1022', 'Sun Rock Retreat');
INSERT INTO `areatrigger_tavern` VALUES ('1023', 'Gadgetzan');
INSERT INTO `areatrigger_tavern` VALUES ('1024', 'Feathermoon Stronghold');
INSERT INTO `areatrigger_tavern` VALUES ('1025', 'Camp Mojache');
INSERT INTO `areatrigger_tavern` VALUES ('2266', 'Nijel\'s Point');
INSERT INTO `areatrigger_tavern` VALUES ('2267', 'Shadowprey Village');
INSERT INTO `areatrigger_tavern` VALUES ('2286', 'Freewind Post');
INSERT INTO `areatrigger_tavern` VALUES ('2287', 'Everlook');
INSERT INTO `areatrigger_tavern` VALUES ('2610', 'Splintertree Post');
INSERT INTO `areatrigger_tavern` VALUES ('3985', 'Cenarion Hold');
INSERT INTO `areatrigger_tavern` VALUES ('4090', 'Stonetalon Peak');
INSERT INTO `areatrigger_tavern` VALUES ('4336', 'Thrallmar Inn');
INSERT INTO `areatrigger_tavern` VALUES ('4337', 'Honor Hold Inn');
INSERT INTO `areatrigger_tavern` VALUES ('4373', 'Zabra jin Inn');
INSERT INTO `areatrigger_tavern` VALUES ('4383', 'Orebor Harborage Inn');
INSERT INTO `areatrigger_tavern` VALUES ('4377', 'Allerian Stronghold Inn');
INSERT INTO `areatrigger_tavern` VALUES ('4382', 'Cenarion Refuge');
INSERT INTO `areatrigger_tavern` VALUES ('4374', 'Telredor Inn');
INSERT INTO `areatrigger_tavern` VALUES ('4240', 'Auzre Watch Inn');
INSERT INTO `areatrigger_tavern` VALUES ('4486', 'Falconwing Square Inn');
INSERT INTO `areatrigger_tavern` VALUES ('4526', 'Shadowmoon Village Inn');
INSERT INTO `areatrigger_tavern` VALUES ('4528', 'Wildhammer Stronghold Inn');
INSERT INTO `areatrigger_tavern` VALUES ('4607', 'Sanctum of the Stars Inn');
INSERT INTO `areatrigger_tavern` VALUES ('4241', 'Bloodmyst Isle Blood Watch Inn');
INSERT INTO `areatrigger_tavern` VALUES ('4595', 'Mok\'Nathal Village Inn');
INSERT INTO `areatrigger_tavern` VALUES ('4108', 'Tranquillien Inn');
INSERT INTO `areatrigger_tavern` VALUES ('720', 'Silverpine Forest');
INSERT INTO `areatrigger_tavern` VALUES ('2786', 'Stormwind backup rest');
INSERT INTO `areatrigger_tavern` VALUES ('4376', 'Telaar Inn');
INSERT INTO `areatrigger_tavern` VALUES ('4381', 'Temple Of Thelamat Inn');
INSERT INTO `areatrigger_tavern` VALUES ('4499', 'Sylvanaar Inn');
INSERT INTO `areatrigger_tavern` VALUES ('4558', 'Toshlay\'s Station Inn');
INSERT INTO `areatrigger_tavern` VALUES ('4494', 'Thunderlord Stronghold Inn');
INSERT INTO `areatrigger_tavern` VALUES ('4640', 'Evergrove Inn');
INSERT INTO `areatrigger_tavern` VALUES ('4521', 'Area 52 Inn');
INSERT INTO `areatrigger_tavern` VALUES ('4555', 'The Stormspire Inn');
INSERT INTO `areatrigger_tavern` VALUES ('4577', 'Altar of Sha\'tar Inn');
INSERT INTO `areatrigger_tavern` VALUES ('3547', 'The Undercity');
INSERT INTO `areatrigger_tavern` VALUES ('4769', 'The City of Ironforge');
INSERT INTO `areatrigger_tavern` VALUES ('4714', 'Mudsprocket Inn');
INSERT INTO `areatrigger_tavern` VALUES ('4847', 'Isle of Quel\'Danas, Sun\'s Reach Harbor Inn');
