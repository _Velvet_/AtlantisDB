/*
Navicat MySQL Data Transfer

Source Server         : Hometown
Source Server Version : 50523
Source Host           : localhost:3306
Source Database       : mangos

Target Server Type    : MYSQL
Target Server Version : 50523
File Encoding         : 65001

Date: 2013-09-26 18:44:58
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `battleground_template`
-- ----------------------------
DROP TABLE IF EXISTS `battleground_template`;
CREATE TABLE `battleground_template` (
  `id` mediumint(8) unsigned NOT NULL,
  `MinPlayersPerTeam` smallint(5) unsigned NOT NULL DEFAULT '0',
  `MaxPlayersPerTeam` smallint(5) unsigned NOT NULL DEFAULT '0',
  `MinLvl` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `MaxLvl` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `AllianceStartLoc` mediumint(8) unsigned NOT NULL,
  `AllianceStartO` float NOT NULL,
  `HordeStartLoc` mediumint(8) unsigned NOT NULL,
  `HordeStartO` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of battleground_template
-- ----------------------------
INSERT INTO `battleground_template` VALUES ('1', '8', '40', '51', '70', '611', '2.72532', '610', '2.27452');
INSERT INTO `battleground_template` VALUES ('2', '3', '10', '10', '70', '769', '3.14159', '770', '3.14159');
INSERT INTO `battleground_template` VALUES ('3', '3', '15', '20', '70', '890', '3.40156', '889', '0.263892');
INSERT INTO `battleground_template` VALUES ('7', '3', '15', '61', '70', '1103', '3.40156', '1104', '0.263892');
INSERT INTO `battleground_template` VALUES ('4', '0', '2', '10', '70', '929', '0', '936', '3.14159');
INSERT INTO `battleground_template` VALUES ('5', '0', '2', '10', '70', '939', '0', '940', '3.14159');
INSERT INTO `battleground_template` VALUES ('6', '0', '2', '10', '70', '0', '0', '0', '0');
INSERT INTO `battleground_template` VALUES ('8', '0', '2', '10', '70', '1258', '0', '1259', '3.14159');
