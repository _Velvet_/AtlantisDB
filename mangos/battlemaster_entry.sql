/*
Navicat MySQL Data Transfer

Source Server         : Hometown
Source Server Version : 50523
Source Host           : localhost:3306
Source Database       : mangos

Target Server Type    : MYSQL
Target Server Version : 50523
File Encoding         : 65001

Date: 2013-09-26 18:45:03
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `battlemaster_entry`
-- ----------------------------
DROP TABLE IF EXISTS `battlemaster_entry`;
CREATE TABLE `battlemaster_entry` (
  `entry` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT 'Entry of a creature',
  `bg_template` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT 'Battleground template id',
  PRIMARY KEY (`entry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of battlemaster_entry
-- ----------------------------
INSERT INTO `battlemaster_entry` VALUES ('347', '1');
INSERT INTO `battlemaster_entry` VALUES ('5118', '1');
INSERT INTO `battlemaster_entry` VALUES ('7410', '1');
INSERT INTO `battlemaster_entry` VALUES ('7427', '1');
INSERT INTO `battlemaster_entry` VALUES ('11946', '1');
INSERT INTO `battlemaster_entry` VALUES ('11947', '1');
INSERT INTO `battlemaster_entry` VALUES ('11948', '1');
INSERT INTO `battlemaster_entry` VALUES ('11949', '1');
INSERT INTO `battlemaster_entry` VALUES ('12197', '1');
INSERT INTO `battlemaster_entry` VALUES ('13217', '1');
INSERT INTO `battlemaster_entry` VALUES ('13219', '1');
INSERT INTO `battlemaster_entry` VALUES ('22101', '6');
INSERT INTO `battlemaster_entry` VALUES ('22015', '7');
INSERT INTO `battlemaster_entry` VALUES ('22013', '7');
INSERT INTO `battlemaster_entry` VALUES ('21235', '6');
INSERT INTO `battlemaster_entry` VALUES ('20499', '6');
INSERT INTO `battlemaster_entry` VALUES ('20497', '6');
INSERT INTO `battlemaster_entry` VALUES ('20390', '7');
INSERT INTO `battlemaster_entry` VALUES ('20388', '7');
INSERT INTO `battlemaster_entry` VALUES ('20386', '7');
INSERT INTO `battlemaster_entry` VALUES ('20384', '7');
INSERT INTO `battlemaster_entry` VALUES ('20383', '7');
INSERT INTO `battlemaster_entry` VALUES ('20382', '7');
INSERT INTO `battlemaster_entry` VALUES ('20381', '7');
INSERT INTO `battlemaster_entry` VALUES ('20374', '7');
INSERT INTO `battlemaster_entry` VALUES ('20362', '7');
INSERT INTO `battlemaster_entry` VALUES ('20276', '1');
INSERT INTO `battlemaster_entry` VALUES ('20274', '3');
INSERT INTO `battlemaster_entry` VALUES ('20273', '3');
INSERT INTO `battlemaster_entry` VALUES ('20272', '2');
INSERT INTO `battlemaster_entry` VALUES ('20271', '1');
INSERT INTO `battlemaster_entry` VALUES ('20269', '2');
INSERT INTO `battlemaster_entry` VALUES ('20120', '3');
INSERT INTO `battlemaster_entry` VALUES ('20119', '1');
INSERT INTO `battlemaster_entry` VALUES ('13616', '1');
INSERT INTO `battlemaster_entry` VALUES ('13617', '1');
INSERT INTO `battlemaster_entry` VALUES ('20118', '2');
INSERT INTO `battlemaster_entry` VALUES ('20003', '2');
INSERT INTO `battlemaster_entry` VALUES ('19925', '6');
INSERT INTO `battlemaster_entry` VALUES ('19923', '6');
INSERT INTO `battlemaster_entry` VALUES ('19915', '6');
INSERT INTO `battlemaster_entry` VALUES ('19912', '6');
INSERT INTO `battlemaster_entry` VALUES ('19911', '6');
INSERT INTO `battlemaster_entry` VALUES ('19910', '2');
INSERT INTO `battlemaster_entry` VALUES ('19909', '6');
INSERT INTO `battlemaster_entry` VALUES ('19908', '2');
INSERT INTO `battlemaster_entry` VALUES ('19907', '1');
INSERT INTO `battlemaster_entry` VALUES ('19906', '1');
INSERT INTO `battlemaster_entry` VALUES ('19905', '3');
INSERT INTO `battlemaster_entry` VALUES ('19858', '6');
INSERT INTO `battlemaster_entry` VALUES ('19855', '3');
INSERT INTO `battlemaster_entry` VALUES ('18895', '6');
INSERT INTO `battlemaster_entry` VALUES ('17507', '2');
INSERT INTO `battlemaster_entry` VALUES ('17506', '1');
INSERT INTO `battlemaster_entry` VALUES ('16711', '3');
INSERT INTO `battlemaster_entry` VALUES ('16696', '2');
INSERT INTO `battlemaster_entry` VALUES ('14942', '1');
INSERT INTO `battlemaster_entry` VALUES ('2302', '2');
INSERT INTO `battlemaster_entry` VALUES ('3890', '2');
INSERT INTO `battlemaster_entry` VALUES ('16695', '1');
INSERT INTO `battlemaster_entry` VALUES ('16694', '3');
INSERT INTO `battlemaster_entry` VALUES ('14753', '2');
INSERT INTO `battlemaster_entry` VALUES ('14981', '2');
INSERT INTO `battlemaster_entry` VALUES ('14982', '2');
INSERT INTO `battlemaster_entry` VALUES ('15106', '7');
INSERT INTO `battlemaster_entry` VALUES ('15103', '7');
INSERT INTO `battlemaster_entry` VALUES ('857', '3');
INSERT INTO `battlemaster_entry` VALUES ('907', '3');
INSERT INTO `battlemaster_entry` VALUES ('12198', '3');
INSERT INTO `battlemaster_entry` VALUES ('15102', '2');
INSERT INTO `battlemaster_entry` VALUES ('14991', '3');
INSERT INTO `battlemaster_entry` VALUES ('14990', '3');
INSERT INTO `battlemaster_entry` VALUES ('10360', '2');
INSERT INTO `battlemaster_entry` VALUES ('15006', '3');
INSERT INTO `battlemaster_entry` VALUES ('15007', '3');
INSERT INTO `battlemaster_entry` VALUES ('15008', '3');
INSERT INTO `battlemaster_entry` VALUES ('15126', '3');
INSERT INTO `battlemaster_entry` VALUES ('15127', '3');
INSERT INTO `battlemaster_entry` VALUES ('2804', '2');
INSERT INTO `battlemaster_entry` VALUES ('20002', '2');
INSERT INTO `battlemaster_entry` VALUES ('1923', '6');
INSERT INTO `battlemaster_entry` VALUES ('20385', '7');
INSERT INTO `battlemaster_entry` VALUES ('25991', '6');
