/*
Navicat MySQL Data Transfer

Source Server         : Hometown
Source Server Version : 50523
Source Host           : localhost:3306
Source Database       : mangos

Target Server Type    : MYSQL
Target Server Version : 50523
File Encoding         : 65001

Date: 2013-09-26 18:45:14
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `conditions`
-- ----------------------------
DROP TABLE IF EXISTS `conditions`;
CREATE TABLE `conditions` (
  `condition_entry` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Identifier',
  `type` tinyint(3) NOT NULL DEFAULT '0' COMMENT 'Type of the condition',
  `value1` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT 'data field one for the condition',
  `value2` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT 'data field two for the condition',
  PRIMARY KEY (`condition_entry`),
  UNIQUE KEY `unique_conditions` (`type`,`value1`,`value2`)
) ENGINE=MyISAM AUTO_INCREMENT=1005 DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='Condition System';

-- ----------------------------
-- Records of conditions
-- ----------------------------
INSERT INTO `conditions` VALUES ('1', '6', '469', '0');
INSERT INTO `conditions` VALUES ('2', '2', '13370', '1');
INSERT INTO `conditions` VALUES ('3', '6', '67', '0');
INSERT INTO `conditions` VALUES ('4', '2', '12384', '1');
INSERT INTO `conditions` VALUES ('5', '7', '755', '1');
INSERT INTO `conditions` VALUES ('6', '7', '197', '1');
INSERT INTO `conditions` VALUES ('7', '7', '333', '1');
INSERT INTO `conditions` VALUES ('8', '7', '165', '1');
INSERT INTO `conditions` VALUES ('9', '8', '10970', '0');
INSERT INTO `conditions` VALUES ('10', '2', '20402', '1');
INSERT INTO `conditions` VALUES ('11', '7', '164', '1');
INSERT INTO `conditions` VALUES ('12', '7', '202', '1');
INSERT INTO `conditions` VALUES ('13', '7', '171', '1');
INSERT INTO `conditions` VALUES ('14', '8', '7786', '0');
INSERT INTO `conditions` VALUES ('15', '9', '10855', '0');
INSERT INTO `conditions` VALUES ('16', '9', '10565', '0');
INSERT INTO `conditions` VALUES ('17', '7', '185', '1');
INSERT INTO `conditions` VALUES ('18', '9', '11541', '0');
INSERT INTO `conditions` VALUES ('19', '1', '33377', '0');
INSERT INTO `conditions` VALUES ('20', '2', '11511', '1');
INSERT INTO `conditions` VALUES ('21', '2', '12766', '1');
INSERT INTO `conditions` VALUES ('22', '2', '12765', '1');
INSERT INTO `conditions` VALUES ('1001', '18', '1', '0');
INSERT INTO `conditions` VALUES ('1002', '18', '2', '0');
INSERT INTO `conditions` VALUES ('1003', '18', '3', '0');
INSERT INTO `conditions` VALUES ('1004', '18', '4', '0');
INSERT INTO `conditions` VALUES ('874', '33', '2', '1');
INSERT INTO `conditions` VALUES ('875', '33', '8', '0');
INSERT INTO `conditions` VALUES ('876', '33', '8', '2');
INSERT INTO `conditions` VALUES ('877', '33', '10', '2');
INSERT INTO `conditions` VALUES ('878', '-1', '874', '877');
INSERT INTO `conditions` VALUES ('879', '-1', '874', '876');
