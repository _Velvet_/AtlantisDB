/*
Navicat MySQL Data Transfer

Source Server         : Hometown
Source Server Version : 50523
Source Host           : localhost:3306
Source Database       : mangos

Target Server Type    : MYSQL
Target Server Version : 50523
File Encoding         : 65001

Date: 2013-09-26 22:09:09
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `creature_linking`
-- ----------------------------
DROP TABLE IF EXISTS `creature_linking`;
CREATE TABLE `creature_linking` (
  `guid` int(10) unsigned NOT NULL COMMENT 'creature.guid of the slave mob that is linked',
  `master_guid` int(10) unsigned NOT NULL COMMENT 'master to trigger events',
  `flag` mediumint(8) unsigned NOT NULL COMMENT 'flag - describing what should happen when',
  PRIMARY KEY (`guid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='Creature Linking System';

-- ----------------------------
-- Records of creature_linking
-- ----------------------------
INSERT INTO `creature_linking` VALUES ('139490', '126627', '15');
INSERT INTO `creature_linking` VALUES ('126624', '126627', '15');
INSERT INTO `creature_linking` VALUES ('126625', '126627', '15');
INSERT INTO `creature_linking` VALUES ('126628', '126627', '15');
INSERT INTO `creature_linking` VALUES ('126641', '126637', '15');
INSERT INTO `creature_linking` VALUES ('139493', '126627', '15');
INSERT INTO `creature_linking` VALUES ('139492', '126627', '15');
INSERT INTO `creature_linking` VALUES ('126629', '126632', '15');
INSERT INTO `creature_linking` VALUES ('126635', '126632', '15');
INSERT INTO `creature_linking` VALUES ('126631', '126632', '15');
INSERT INTO `creature_linking` VALUES ('126630', '126632', '15');
INSERT INTO `creature_linking` VALUES ('126640', '126637', '15');
INSERT INTO `creature_linking` VALUES ('139494', '126632', '15');
INSERT INTO `creature_linking` VALUES ('126634', '126632', '15');
INSERT INTO `creature_linking` VALUES ('139884', '126637', '15');
INSERT INTO `creature_linking` VALUES ('126638', '126637', '15');
INSERT INTO `creature_linking` VALUES ('126636', '126637', '15');
INSERT INTO `creature_linking` VALUES ('127317', '126637', '15');
INSERT INTO `creature_linking` VALUES ('126647', '139540', '15');
INSERT INTO `creature_linking` VALUES ('126643', '139540', '15');
INSERT INTO `creature_linking` VALUES ('139885', '139540', '15');
INSERT INTO `creature_linking` VALUES ('126646', '139540', '15');
INSERT INTO `creature_linking` VALUES ('126642', '139540', '15');
INSERT INTO `creature_linking` VALUES ('126644', '139540', '15');
INSERT INTO `creature_linking` VALUES ('139890', '139887', '15');
INSERT INTO `creature_linking` VALUES ('139888', '139887', '15');
INSERT INTO `creature_linking` VALUES ('127724', '139542', '15');
INSERT INTO `creature_linking` VALUES ('127821', '139542', '15');
INSERT INTO `creature_linking` VALUES ('139541', '139542', '15');
INSERT INTO `creature_linking` VALUES ('127735', '139542', '15');
INSERT INTO `creature_linking` VALUES ('139543', '139542', '15');
INSERT INTO `creature_linking` VALUES ('139545', '127776', '15');
INSERT INTO `creature_linking` VALUES ('127740', '127776', '15');
INSERT INTO `creature_linking` VALUES ('139544', '127776', '15');
INSERT INTO `creature_linking` VALUES ('127760', '127776', '15');
INSERT INTO `creature_linking` VALUES ('127720', '127756', '15');
INSERT INTO `creature_linking` VALUES ('127739', '127756', '15');
INSERT INTO `creature_linking` VALUES ('139619', '127769', '15');
INSERT INTO `creature_linking` VALUES ('139505', '139501', '15');
INSERT INTO `creature_linking` VALUES ('139506', '139501', '15');
INSERT INTO `creature_linking` VALUES ('139507', '139501', '15');
INSERT INTO `creature_linking` VALUES ('139508', '139501', '15');
INSERT INTO `creature_linking` VALUES ('139892', '139501', '15');
INSERT INTO `creature_linking` VALUES ('139896', '139893', '15');
INSERT INTO `creature_linking` VALUES ('139894', '139893', '15');
INSERT INTO `creature_linking` VALUES ('139895', '139893', '15');
INSERT INTO `creature_linking` VALUES ('139511', '139510', '15');
INSERT INTO `creature_linking` VALUES ('139513', '139510', '15');
INSERT INTO `creature_linking` VALUES ('139512', '139510', '15');
INSERT INTO `creature_linking` VALUES ('139515', '139510', '15');
INSERT INTO `creature_linking` VALUES ('199897', '139510', '15');
INSERT INTO `creature_linking` VALUES ('139518', '139516', '15');
INSERT INTO `creature_linking` VALUES ('139521', '139516', '15');
INSERT INTO `creature_linking` VALUES ('139520', '139516', '15');
INSERT INTO `creature_linking` VALUES ('139519', '139516', '15');
INSERT INTO `creature_linking` VALUES ('139898', '139516', '15');
INSERT INTO `creature_linking` VALUES ('139899', '139902', '15');
INSERT INTO `creature_linking` VALUES ('139900', '139902', '15');
INSERT INTO `creature_linking` VALUES ('139903', '139902', '15');
INSERT INTO `creature_linking` VALUES ('139523', '139522', '15');
INSERT INTO `creature_linking` VALUES ('139525', '139522', '15');
INSERT INTO `creature_linking` VALUES ('139527', '139522', '15');
INSERT INTO `creature_linking` VALUES ('139524', '139522', '15');
INSERT INTO `creature_linking` VALUES ('139526', '139522', '15');
INSERT INTO `creature_linking` VALUES ('127785', '139627', '15');
INSERT INTO `creature_linking` VALUES ('127778', '139627', '15');
INSERT INTO `creature_linking` VALUES ('139628', '139627', '15');
INSERT INTO `creature_linking` VALUES ('139630', '139627', '15');
INSERT INTO `creature_linking` VALUES ('139629', '139627', '15');
INSERT INTO `creature_linking` VALUES ('127742', '127750', '15');
INSERT INTO `creature_linking` VALUES ('127775', '127750', '15');
INSERT INTO `creature_linking` VALUES ('127738', '127750', '15');
INSERT INTO `creature_linking` VALUES ('127767', '127750', '15');
INSERT INTO `creature_linking` VALUES ('127728', '127750', '15');
INSERT INTO `creature_linking` VALUES ('127723', '127750', '15');
INSERT INTO `creature_linking` VALUES ('127789', '127750', '15');
