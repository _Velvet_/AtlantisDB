/*
Navicat MySQL Data Transfer

Source Server         : Hometown
Source Server Version : 50523
Source Host           : localhost:3306
Source Database       : mangos

Target Server Type    : MYSQL
Target Server Version : 50523
File Encoding         : 65001

Date: 2013-09-26 22:09:14
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `creature_linking_template`
-- ----------------------------
DROP TABLE IF EXISTS `creature_linking_template`;
CREATE TABLE `creature_linking_template` (
  `entry` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT 'creature_template.entry of the slave mob that is linked',
  `map` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Id of map of the mobs',
  `master_entry` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT 'master to trigger events',
  `flag` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT 'flag - describing what should happen when',
  `search_range` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`entry`,`map`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='Creature Linking System';

-- ----------------------------
-- Records of creature_linking_template
-- ----------------------------
INSERT INTO `creature_linking_template` VALUES ('17007', '532', '15687', '3', '0');
INSERT INTO `creature_linking_template` VALUES ('19872', '532', '15687', '3', '0');
INSERT INTO `creature_linking_template` VALUES ('19873', '532', '15687', '3', '0');
INSERT INTO `creature_linking_template` VALUES ('19874', '532', '15687', '3', '0');
INSERT INTO `creature_linking_template` VALUES ('19875', '532', '15687', '3', '0');
INSERT INTO `creature_linking_template` VALUES ('19876', '532', '15687', '3', '0');
INSERT INTO `creature_linking_template` VALUES ('17543', '532', '17535', '4096', '0');
INSERT INTO `creature_linking_template` VALUES ('17546', '532', '17535', '4096', '0');
INSERT INTO `creature_linking_template` VALUES ('17547', '532', '17535', '4096', '0');
INSERT INTO `creature_linking_template` VALUES ('18412', '532', '17535', '4096', '0');
INSERT INTO `creature_linking_template` VALUES ('24143', '568', '23577', '4096', '0');
INSERT INTO `creature_linking_template` VALUES ('24722', '585', '24723', '36', '0');
INSERT INTO `creature_linking_template` VALUES ('24858', '568', '23574', '4112', '0');
INSERT INTO `creature_linking_template` VALUES ('15334', '531', '15727', '4128', '0');
INSERT INTO `creature_linking_template` VALUES ('15725', '531', '15727', '4', '0');
INSERT INTO `creature_linking_template` VALUES ('15726', '531', '15589', '4096', '0');
INSERT INTO `creature_linking_template` VALUES ('15728', '531', '15727', '4128', '0');
INSERT INTO `creature_linking_template` VALUES ('15802', '531', '15727', '4128', '0');
INSERT INTO `creature_linking_template` VALUES ('15910', '531', '15727', '4128', '0');
INSERT INTO `creature_linking_template` VALUES ('15904', '531', '15727', '4128', '0');
INSERT INTO `creature_linking_template` VALUES ('17647', '0', '17635', '515', '0');
INSERT INTO `creature_linking_template` VALUES ('17996', '0', '17995', '515', '0');
INSERT INTO `creature_linking_template` VALUES ('17646', '532', '15690', '4112', '0');
INSERT INTO `creature_linking_template` VALUES ('17265', '532', '15688', '4112', '0');
INSERT INTO `creature_linking_template` VALUES ('17267', '532', '15688', '4112', '0');
INSERT INTO `creature_linking_template` VALUES ('17917', '545', '17797', '1031', '0');
INSERT INTO `creature_linking_template` VALUES ('17954', '545', '17798', '4', '0');
INSERT INTO `creature_linking_template` VALUES ('22035', '548', '21216', '4112', '0');
INSERT INTO `creature_linking_template` VALUES ('22036', '548', '21216', '4112', '0');
INSERT INTO `creature_linking_template` VALUES ('24745', '585', '24744', '4112', '0');
INSERT INTO `creature_linking_template` VALUES ('24815', '585', '24686', '4112', '0');
INSERT INTO `creature_linking_template` VALUES ('17838', '269', '15608', '16', '0');
INSERT INTO `creature_linking_template` VALUES ('17879', '269', '15608', '16', '0');
INSERT INTO `creature_linking_template` VALUES ('17880', '269', '15608', '16', '0');
INSERT INTO `creature_linking_template` VALUES ('17881', '269', '15608', '16', '0');
INSERT INTO `creature_linking_template` VALUES ('21697', '269', '15608', '16', '0');
INSERT INTO `creature_linking_template` VALUES ('21698', '269', '15608', '16', '0');
INSERT INTO `creature_linking_template` VALUES ('21104', '269', '15608', '16', '0');
INSERT INTO `creature_linking_template` VALUES ('17839', '269', '15608', '16', '0');
INSERT INTO `creature_linking_template` VALUES ('17835', '269', '15608', '16', '0');
INSERT INTO `creature_linking_template` VALUES ('21818', '269', '15608', '16', '0');
INSERT INTO `creature_linking_template` VALUES ('17892', '269', '15608', '16', '0');
INSERT INTO `creature_linking_template` VALUES ('18994', '269', '15608', '16', '0');
INSERT INTO `creature_linking_template` VALUES ('18995', '269', '15608', '16', '0');
INSERT INTO `creature_linking_template` VALUES ('18553', '269', '15608', '16', '0');
INSERT INTO `creature_linking_template` VALUES ('23085', '564', '22898', '4112', '0');
INSERT INTO `creature_linking_template` VALUES ('23095', '564', '22898', '4112', '0');
INSERT INTO `creature_linking_template` VALUES ('23215', '564', '22990', '4112', '0');
INSERT INTO `creature_linking_template` VALUES ('23216', '564', '22990', '4112', '0');
INSERT INTO `creature_linking_template` VALUES ('23523', '564', '22990', '4112', '0');
INSERT INTO `creature_linking_template` VALUES ('23318', '564', '22990', '4112', '0');
INSERT INTO `creature_linking_template` VALUES ('23524', '564', '22990', '4112', '0');
INSERT INTO `creature_linking_template` VALUES ('23469', '564', '22856', '4112', '0');
INSERT INTO `creature_linking_template` VALUES ('22951', '564', '22949', '143', '0');
INSERT INTO `creature_linking_template` VALUES ('22950', '564', '22949', '143', '0');
INSERT INTO `creature_linking_template` VALUES ('22952', '564', '22949', '143', '0');
INSERT INTO `creature_linking_template` VALUES ('23089', '564', '22917', '4096', '0');
INSERT INTO `creature_linking_template` VALUES ('23197', '564', '22917', '4096', '0');
INSERT INTO `creature_linking_template` VALUES ('23226', '564', '22917', '4112', '0');
INSERT INTO `creature_linking_template` VALUES ('23498', '564', '22917', '4112', '0');
INSERT INTO `creature_linking_template` VALUES ('22996', '564', '22917', '4096', '0');
INSERT INTO `creature_linking_template` VALUES ('22997', '564', '22917', '4096', '0');
INSERT INTO `creature_linking_template` VALUES ('23375', '564', '22917', '4096', '0');
INSERT INTO `creature_linking_template` VALUES ('17540', '543', '17308', '4112', '0');
INSERT INTO `creature_linking_template` VALUES ('17309', '543', '17306', '4', '0');
INSERT INTO `creature_linking_template` VALUES ('20481', '554', '19221', '4112', '0');
INSERT INTO `creature_linking_template` VALUES ('25588', '580', '25315', '4', '0');
INSERT INTO `creature_linking_template` VALUES ('17951', '545', '17796', '4112', '0');
INSERT INTO `creature_linking_template` VALUES ('21865', '548', '21217', '4096', '0');
INSERT INTO `creature_linking_template` VALUES ('21873', '548', '21217', '4096', '0');
INSERT INTO `creature_linking_template` VALUES ('21806', '548', '21806', '3', '0');
INSERT INTO `creature_linking_template` VALUES ('21875', '548', '21806', '16', '0');
INSERT INTO `creature_linking_template` VALUES ('21964', '548', '21214', '15', '0');
INSERT INTO `creature_linking_template` VALUES ('21966', '548', '21214', '15', '0');
INSERT INTO `creature_linking_template` VALUES ('21965', '548', '21214', '15', '0');
INSERT INTO `creature_linking_template` VALUES ('21913', '548', '21213', '4112', '0');
INSERT INTO `creature_linking_template` VALUES ('21920', '548', '21213', '4112', '0');
INSERT INTO `creature_linking_template` VALUES ('18836', '565', '18831', '143', '0');
INSERT INTO `creature_linking_template` VALUES ('18835', '565', '18831', '143', '0');
INSERT INTO `creature_linking_template` VALUES ('18834', '565', '18831', '143', '0');
INSERT INTO `creature_linking_template` VALUES ('18832', '565', '18831', '143', '0');
INSERT INTO `creature_linking_template` VALUES ('17454', '544', '17256', '4112', '0');
INSERT INTO `creature_linking_template` VALUES ('17256', '544', '17256', '15', '0');
INSERT INTO `creature_linking_template` VALUES ('18925', '550', '18805', '4112', '0');
INSERT INTO `creature_linking_template` VALUES ('18806', '550', '18805', '4112', '0');
INSERT INTO `creature_linking_template` VALUES ('24240', '568', '24239', '3', '0');
INSERT INTO `creature_linking_template` VALUES ('24241', '568', '24239', '3', '0');
INSERT INTO `creature_linking_template` VALUES ('24242', '568', '24239', '3', '0');
INSERT INTO `creature_linking_template` VALUES ('24243', '568', '24239', '3', '0');
INSERT INTO `creature_linking_template` VALUES ('24244', '568', '24239', '3', '0');
INSERT INTO `creature_linking_template` VALUES ('24245', '568', '24239', '3', '0');
INSERT INTO `creature_linking_template` VALUES ('24246', '568', '24239', '3', '0');
INSERT INTO `creature_linking_template` VALUES ('24247', '568', '24239', '3', '0');
INSERT INTO `creature_linking_template` VALUES ('14988', '309', '11382', '4096', '0');
INSERT INTO `creature_linking_template` VALUES ('15117', '309', '11382', '4096', '0');
INSERT INTO `creature_linking_template` VALUES ('10316', '229', '10316', '3', '0');
INSERT INTO `creature_linking_template` VALUES ('15544', '531', '15511', '143', '0');
INSERT INTO `creature_linking_template` VALUES ('15543', '531', '15511', '143', '0');
INSERT INTO `creature_linking_template` VALUES ('12557', '469', '12435', '135', '0');
INSERT INTO `creature_linking_template` VALUES ('14456', '469', '12557', '143', '0');
INSERT INTO `creature_linking_template` VALUES ('21686', '530', '21685', '3', '0');
INSERT INTO `creature_linking_template` VALUES ('21687', '530', '21685', '3', '0');
INSERT INTO `creature_linking_template` VALUES ('17911', '329', '17910', '143', '0');
INSERT INTO `creature_linking_template` VALUES ('17912', '329', '17910', '143', '0');
INSERT INTO `creature_linking_template` VALUES ('17913', '329', '17910', '143', '0');
INSERT INTO `creature_linking_template` VALUES ('17914', '329', '17910', '143', '0');
INSERT INTO `creature_linking_template` VALUES ('25865', '547', '25740', '4112', '0');
INSERT INTO `creature_linking_template` VALUES ('25755', '547', '25740', '4112', '0');
INSERT INTO `creature_linking_template` VALUES ('15514', '509', '15370', '1030', '0');
INSERT INTO `creature_linking_template` VALUES ('15546', '509', '15369', '4112', '0');
INSERT INTO `creature_linking_template` VALUES ('15934', '509', '15369', '4096', '0');
INSERT INTO `creature_linking_template` VALUES ('21466', '552', '20912', '4112', '0');
INSERT INTO `creature_linking_template` VALUES ('21467', '552', '20912', '4112', '0');
INSERT INTO `creature_linking_template` VALUES ('19953', '553', '17975', '4112', '0');
INSERT INTO `creature_linking_template` VALUES ('19958', '553', '17975', '4112', '0');
INSERT INTO `creature_linking_template` VALUES ('19969', '553', '17975', '4112', '0');
INSERT INTO `creature_linking_template` VALUES ('19962', '553', '17975', '4112', '0');
INSERT INTO `creature_linking_template` VALUES ('19964', '553', '17975', '4112', '0');
INSERT INTO `creature_linking_template` VALUES ('19919', '553', '17980', '4112', '0');
INSERT INTO `creature_linking_template` VALUES ('19920', '553', '17980', '4112', '0');
INSERT INTO `creature_linking_template` VALUES ('19949', '553', '17977', '4112', '0');
INSERT INTO `creature_linking_template` VALUES ('18431', '557', '18344', '4112', '0');
INSERT INTO `creature_linking_template` VALUES ('19203', '556', '18472', '4112', '0');
INSERT INTO `creature_linking_template` VALUES ('19204', '556', '18472', '4112', '0');
INSERT INTO `creature_linking_template` VALUES ('19205', '556', '18472', '4112', '0');
INSERT INTO `creature_linking_template` VALUES ('19206', '556', '18472', '4112', '0');
INSERT INTO `creature_linking_template` VALUES ('25756', '547', '25740', '4112', '0');
INSERT INTO `creature_linking_template` VALUES ('25757', '547', '25740', '4112', '0');
