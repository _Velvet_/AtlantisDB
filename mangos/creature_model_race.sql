/*
Navicat MySQL Data Transfer

Source Server         : Hometown
Source Server Version : 50523
Source Host           : localhost:3306
Source Database       : mangos

Target Server Type    : MYSQL
Target Server Version : 50523
File Encoding         : 65001

Date: 2013-09-26 22:10:24
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `creature_model_race`
-- ----------------------------
DROP TABLE IF EXISTS `creature_model_race`;
CREATE TABLE `creature_model_race` (
  `modelid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `racemask` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `creature_entry` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT 'option 1, modelid_N from creature_template',
  `modelid_racial` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT 'option 2, explicit modelid',
  PRIMARY KEY (`modelid`,`racemask`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Model system';

-- ----------------------------
-- Records of creature_model_race
-- ----------------------------
INSERT INTO `creature_model_race` VALUES ('892', '690', '0', '8571');
INSERT INTO `creature_model_race` VALUES ('2281', '690', '0', '2289');
INSERT INTO `creature_model_race` VALUES ('15374', '690', '0', '15375');
INSERT INTO `creature_model_race` VALUES ('21243', '690', '0', '21244');
INSERT INTO `creature_model_race` VALUES ('20857', '690', '0', '20872');
