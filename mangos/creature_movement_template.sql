/*
Navicat MySQL Data Transfer

Source Server         : Hometown
Source Server Version : 50523
Source Host           : localhost:3306
Source Database       : mangos

Target Server Type    : MYSQL
Target Server Version : 50523
File Encoding         : 65001

Date: 2013-09-26 22:10:36
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `creature_movement_template`
-- ----------------------------
DROP TABLE IF EXISTS `creature_movement_template`;
CREATE TABLE `creature_movement_template` (
  `entry` mediumint(8) unsigned NOT NULL COMMENT 'Creature entry',
  `point` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `position_x` float NOT NULL DEFAULT '0',
  `position_y` float NOT NULL DEFAULT '0',
  `position_z` float NOT NULL DEFAULT '0',
  `waittime` int(10) unsigned NOT NULL DEFAULT '0',
  `script_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `textid1` int(11) NOT NULL DEFAULT '0',
  `textid2` int(11) NOT NULL DEFAULT '0',
  `textid3` int(11) NOT NULL DEFAULT '0',
  `textid4` int(11) NOT NULL DEFAULT '0',
  `textid5` int(11) NOT NULL DEFAULT '0',
  `emote` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `spell` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `wpguid` int(11) NOT NULL DEFAULT '0',
  `orientation` float NOT NULL DEFAULT '0',
  `model1` mediumint(9) NOT NULL DEFAULT '0',
  `model2` mediumint(9) NOT NULL DEFAULT '0',
  PRIMARY KEY (`entry`,`point`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='Creature waypoint system';

-- ----------------------------
-- Records of creature_movement_template
-- ----------------------------
INSERT INTO `creature_movement_template` VALUES ('17635', '1', '2501.35', '-4725.99', '90.974', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('17635', '2', '2491.21', '-4693.16', '82.363', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('17635', '3', '2493.06', '-4655.49', '75.194', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('17635', '4', '2562.23', '-4646.4', '79.003', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('17635', '5', '2699.75', '-4567.38', '87.46', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('17635', '6', '2757.27', '-4527.59', '89.08', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('17635', '7', '2850.87', '-4417.56', '89.421', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('17635', '8', '2888.34', '-4328.49', '90.562', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('17635', '9', '2913.27', '-4167.14', '93.919', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('17635', '10', '3035.66', '-4260.18', '96.141', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('17635', '11', '3088.54', '-4250.21', '97.769', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('17635', '12', '3147.3', '-4318.8', '130.41', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('17635', '13', '3166.81', '-4349.2', '137.569', '0', '1763501', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('17995', '1', '2501.35', '-4725.99', '90.974', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('17995', '2', '2491.21', '-4693.16', '82.363', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('17995', '3', '2493.06', '-4655.49', '75.194', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('17995', '4', '2562.23', '-4646.4', '79.003', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('17995', '5', '2699.75', '-4567.38', '87.46', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('17995', '6', '2757.27', '-4527.59', '89.08', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('17995', '7', '2850.87', '-4417.56', '89.421', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('17995', '8', '2888.34', '-4328.49', '90.562', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('17995', '9', '2913.27', '-4167.14', '93.919', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('17995', '10', '3035.66', '-4260.18', '96.141', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('17995', '11', '3088.54', '-4250.21', '97.769', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('17995', '12', '3147.3', '-4318.8', '130.41', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('17995', '13', '3166.81', '-4349.2', '137.569', '0', '1799501', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('17848', '1', '2125.84', '87.2535', '54.883', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('17848', '2', '2111.01', '93.8022', '52.6356', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('17848', '3', '2106.7', '114.753', '53.1965', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('17848', '4', '2107.76', '138.746', '52.5109', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('17848', '5', '2114.83', '160.142', '52.4738', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('17848', '6', '2125.24', '178.909', '52.7283', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('17848', '7', '2151.02', '208.901', '53.1551', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('17848', '8', '2177', '233.069', '52.4409', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('17848', '9', '2190.71', '227.831', '53.2742', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('17848', '10', '2178.14', '214.219', '53.0779', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('17848', '11', '2154.99', '202.795', '52.6446', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('17848', '12', '2132', '191.834', '52.5709', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('17848', '13', '2117.59', '166.708', '52.7686', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('17848', '14', '2093.61', '139.441', '52.7616', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('17848', '15', '2086.29', '104.95', '52.9246', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('17848', '16', '2094.23', '81.2788', '52.6946', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('17848', '17', '2108.7', '85.3075', '53.3294', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('17848', '18', '2125.5', '88.9481', '54.7953', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('17848', '19', '2128.2', '70.9763', '64.422', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('20535', '1', '2125.84', '87.2535', '54.883', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('20535', '2', '2111.01', '93.8022', '52.6356', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('20535', '3', '2106.7', '114.753', '53.1965', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('20535', '4', '2107.76', '138.746', '52.5109', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('20535', '5', '2114.83', '160.142', '52.4738', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('20535', '6', '2125.24', '178.909', '52.7283', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('20535', '7', '2151.02', '208.901', '53.1551', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('20535', '8', '2177', '233.069', '52.4409', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('20535', '9', '2190.71', '227.831', '53.2742', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('20535', '10', '2178.14', '214.219', '53.0779', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('20535', '11', '2154.99', '202.795', '52.6446', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('20535', '12', '2132', '191.834', '52.5709', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('20535', '13', '2117.59', '166.708', '52.7686', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('20535', '14', '2093.61', '139.441', '52.7616', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('20535', '15', '2086.29', '104.95', '52.9246', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('20535', '16', '2094.23', '81.2788', '52.6946', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('20535', '17', '2108.7', '85.3075', '53.3294', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('20535', '18', '2125.5', '88.9481', '54.7953', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('20535', '19', '2128.2', '70.9763', '64.422', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('7607', '1', '1881.05', '1297.36', '48.419', '1000', '760701', '0', '0', '0', '0', '0', '0', '0', '0', '5.41', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('7607', '2', '1881.74', '1295.99', '48.323', '1000', '760702', '0', '0', '0', '0', '0', '0', '0', '0', '5.41', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('7607', '3', '1881', '1293.34', '47.627', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('7607', '4', '1880.21', '1290.43', '45.97', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('7607', '5', '1880.21', '1290.43', '45.97', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('7607', '6', '1891.08', '1284.61', '43.58', '1000', '760706', '0', '0', '0', '0', '0', '0', '0', '0', '4.71', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('7607', '7', '1883.29', '1263.76', '41.576', '2000', '760707', '0', '0', '0', '0', '0', '0', '0', '0', '4.71', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('7607', '8', '1889.49', '1271.97', '41.626', '1000', '760701', '0', '0', '0', '0', '0', '0', '0', '0', '4.71', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('7607', '9', '1895.36', '1227.6', '9.521', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('7607', '10', '1893.28', '1206.29', '8.877', '1000', '760701', '0', '0', '0', '0', '0', '0', '0', '0', '4.61', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('7607', '11', '1891.67', '1181.69', '8.877', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('7607', '12', '1876.01', '1161.77', '9.653', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('7607', '13', '1857.49', '1145.53', '15.119', '1000', '760713', '0', '0', '0', '0', '0', '0', '0', '0', '3.86', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('7607', '14', '1877.11', '1148.82', '10.316', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('7607', '15', '1886.31', '1137.95', '9.352', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('7607', '16', '1869.98', '1092.89', '8.876', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('7604', '1', '1882.89', '1299.27', '48.3843', '1000', '760401', '0', '0', '0', '0', '0', '0', '0', '0', '4.83', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('7604', '2', '1883.4', '1297.18', '48.293', '1000', '760402', '0', '0', '0', '0', '0', '0', '0', '0', '4.83', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('7604', '3', '1881', '1293.34', '47.627', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('7604', '4', '1880.21', '1290.43', '45.97', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('7604', '5', '1880.21', '1290.43', '45.97', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('7604', '6', '1886.9', '1264.08', '41.494', '1000', '760401', '0', '0', '0', '0', '0', '0', '0', '0', '4.71', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('7604', '7', '1886.7', '1227.96', '9.9242', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('7604', '8', '1884.29', '1202.94', '8.8781', '60000', '0', '0', '0', '0', '0', '0', '0', '0', '0', '4.61', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('7604', '9', '1884.29', '1202.94', '8.8781', '15000', '760409', '0', '0', '0', '0', '0', '0', '0', '0', '4.61', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('7605', '1', '1886.64', '1299.11', '48.3146', '1000', '760501', '0', '0', '0', '0', '0', '0', '0', '0', '4.36', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('7605', '2', '1886.12', '1297.25', '48.241', '1000', '760502', '0', '0', '0', '0', '0', '0', '0', '0', '4.36', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('7605', '3', '1881', '1293.34', '47.627', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('7605', '4', '1880.21', '1290.43', '45.97', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('7605', '5', '1880.21', '1290.43', '45.97', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('7605', '6', '1890.22', '1264.3', '41.42', '1000', '760501', '0', '0', '0', '0', '0', '0', '0', '0', '4.71', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('7605', '7', '1895.36', '1227.6', '9.521', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('7605', '8', '1889.01', '1201.98', '8.878', '60000', '0', '0', '0', '0', '0', '0', '0', '0', '0', '4.54', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('7605', '9', '1889.01', '1201.98', '8.878', '15000', '760509', '0', '0', '0', '0', '0', '0', '0', '0', '4.54', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('7606', '1', '1889.62', '1298.01', '48.2581', '1000', '760601', '0', '0', '0', '0', '0', '0', '0', '0', '3.94', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('7606', '2', '1888.2', '1296.76', '48.203', '1000', '760602', '0', '0', '0', '0', '0', '0', '0', '0', '3.94', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('7606', '3', '1881', '1293.34', '47.627', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('7606', '4', '1880.21', '1290.43', '45.97', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('7606', '5', '1880.21', '1290.43', '45.97', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('7606', '6', '1883.21', '1272', '41.85', '1000', '760601', '0', '0', '0', '0', '0', '0', '0', '0', '4.71', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('7606', '7', '1879.25', '1227.15', '9.276', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('7606', '8', '1876.14', '1207.26', '8.877', '60000', '0', '0', '0', '0', '0', '0', '0', '0', '0', '4.64', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('7606', '9', '1876.14', '1207.26', '8.877', '15000', '760609', '0', '0', '0', '0', '0', '0', '0', '0', '4.64', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('7608', '1', '1891.07', '1294.78', '48.2347', '1000', '760801', '0', '0', '0', '0', '0', '0', '0', '0', '3.31', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('7608', '2', '1889.02', '1294.43', '48.189', '1000', '760802', '0', '0', '0', '0', '0', '0', '0', '0', '3.31', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('7608', '3', '1881', '1293.34', '47.627', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('7608', '4', '1880.21', '1290.43', '45.97', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('7608', '5', '1880.21', '1290.43', '45.97', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('7608', '6', '1886.34', '1271.89', '41.735', '1000', '760801', '0', '0', '0', '0', '0', '0', '0', '0', '4.71', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('7608', '7', '1886.7', '1227.96', '9.924', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('7608', '8', '1884.86', '1208.98', '8.878', '60000', '0', '0', '0', '0', '0', '0', '0', '0', '0', '4.61', '0', '0');
INSERT INTO `creature_movement_template` VALUES ('7608', '9', '1884.86', '1208.98', '8.878', '15000', '760809', '0', '0', '0', '0', '0', '0', '0', '0', '4.61', '0', '0');
