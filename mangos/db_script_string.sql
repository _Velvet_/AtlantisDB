/*
Navicat MySQL Data Transfer

Source Server         : Hometown
Source Server Version : 50523
Source Host           : localhost:3306
Source Database       : mangos

Target Server Type    : MYSQL
Target Server Version : 50523
File Encoding         : 65001

Date: 2013-09-26 22:11:16
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `db_script_string`
-- ----------------------------
DROP TABLE IF EXISTS `db_script_string`;
CREATE TABLE `db_script_string` (
  `entry` int(11) unsigned NOT NULL DEFAULT '0',
  `content_default` text NOT NULL,
  `content_loc1` text,
  `content_loc2` text,
  `content_loc3` text,
  `content_loc4` text,
  `content_loc5` text,
  `content_loc6` text,
  `content_loc7` text,
  `content_loc8` text,
  `sound` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `language` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `emote` smallint(5) unsigned NOT NULL DEFAULT '0',
  `comment` text,
  PRIMARY KEY (`entry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of db_script_string
-- ----------------------------
INSERT INTO `db_script_string` VALUES ('2000000023', 'The battle is about to begin!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000024', '$N versus the ferocious clefthoof.', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000001', 'Let\'s go.', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000002', 'It is not here! hmmm....', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000004', 'It is not here too. Strange...... Where it is?', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000008', 'All right, let\'s go.', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000005', 'Hmm. It is not here. Maybe Hollee has it.......', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000007', 'I can make it the rest of the way. $N. THANKS!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000006', 'Ok, let\'s go!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000061', 'If you hear this whipser, you\'re dying.', null, null, null, null, null, null, null, null, '0', '4', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000060', 'If you hear this whipser, you\'re dying.', null, null, null, null, null, null, null, null, '0', '4', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000059', 'You saved me Braves !', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000058', 'You saved me Braves !', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000055', 'The threat be over! $N be savin\' us all!', null, null, null, null, null, null, null, null, '0', '1', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000053', 'Well, that should do it. Come now, back to the shop to finish our business!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000052', 'Follow me, good $C. I\'ll have your barding done faster than I could down a Dwarven stout. Mmmm... stout.', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000039', 'We will suffer no demon\'s servant in our lands!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000043', 'Thank you again, $N. This rare earth will be very helpful in my experiments.', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000036', 'The invisibility liquor is ready for you, $N.', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000035', 'This shouldn\'t take long...', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000044', 'Peace and patience be with you, $N. Remain strong always.', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000050', 'BOW DOWN TO THE ALMIGHTY! BOW DOWN BEFORE MY INFERNAL DESTRO... chicken?', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000049', 'Stand back! Stand clear! The infernal will need to be given a wide berth!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000051', 'Silence, servant! Vengeance will be mine! Death to Stormwind! Death by chicken!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000041', 'It\'s a mystery of the past indeed! But a key to our future!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000034', 'You are Dismissed, $N.', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000040', 'By the stars! A spirit has been summoned!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000057', 'You saved me Braves !', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000056', 'You saved me Braves !', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000047', 'Spirits of Water, I give you praise, and I beg of you a favour. You have heard $N\'s pleas, and I trust his inent is noble. Please, will you aid us?', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000048', 'Thank you great spirit. Thank you!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000054', 'How many more of you grunts do I need to send back to mommy and daddy in a body bag before you grow a brain and realize that runnin\' head first into a Legion kill squad is suicide? And don\'t nod your thick skulls at me as if you know what I\'m talkin\' about!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000042', 'Now let us place this rare earth in my planter...', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000037', 'Hey there, Belm! Give me a mug of Thunder Ale, and one for my good friend $N.', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000045', 'There\'s nothing like some scalding mornbrew on a chilly Dun Morogh day to get things started right!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000038', 'Ribbit! No!! This cannot...ribbit...be! You have duped me with...ribbit...your foul trickery! Ribbit!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000046', 'Oooooo hot hot hot! If that won\'t put spring in your step, I don\'t know what will!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000003', 'It is Hollee\'s tent, but where Hollee?', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005000', 'Fizzles used to be a great wizard. But got turned into a rabbit when one of his spells went bad.', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005001', 'Hello, Charys. I have my list, could you get me all of that, especially the last ingredient.', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005002', 'Sure Paige. Just be gentle.', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005003', 'Thanks, Charys. C\'mon Paige, sweetie.', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005004', 'Mommy? Can I pet Fizzles?', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005005', 'I can\'t believe dad won\'t let me keep your sister.', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005006', 'Kitten for sale, looking for a good home.', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005007', 'Can anyone give my adorable, extra little kitty a home?', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005008', 'What does allergic mean anyway? And what does it have to do with either of my kitties?', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005009', 'Will someone please give my little kitten a good home?', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005010', 'Don\'t worry, I\'ll find a good home for ya.', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005011', 'Jack and Jill my wrinkled patoot! I do all the water luggin round here.', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005012', 'Where\'s the water Emma? Get the water Emma? Ifn it werent fer me that lot wouldn\'t know what water looks like.', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005013', 'Of course Im talking to myself. Only way to get a decent conversation in this city.', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005014', 'Seems like a hundred times a day I walk all the way to get more water. No respect for their elders I tell ya.', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005015', 'Deja vu. For a moment I thought I was back home... before the plague...', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005016', 'Come here Fluffy!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005017', 'Please Keep Darnassus Clean.', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005018', 'Litter Bugs pay a stiff fine.', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005019', 'Recycle to keep Mother Earth clean.', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005020', 'Poor Dorius. If I ever get my hands on those Dark Irons, so help me...', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005021', 'I WIN!!!!!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005022', 'Dang it!!!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005023', 'ANYONE DARE CHALLENGE ME!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005024', 'YA BUNCH OF COWARDS!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005025', 'NO RISING WARRIORS IN THIS GROUP!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005026', 'Welcome to Shattrath City.', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005027', 'You Seek Guidance?', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005028', 'Who Dares Bother ME...I will have your Head.', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005029', 'Welcome to our fair city!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005030', 'Please help keep our city clean..', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005031', 'Aye, that\'ll do it! They\'ll come runnin\' once they see the reward we\'ve put up!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005032', 'I FEEL DEATH ALONGSIDE!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005033', 'FEEL MY POWER!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005034', 'NEXT TIME YOU WILL DIE!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005035', 'YOU PRAY SERVANTS MY!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005036', 'TRY TO KILL ME LOLS!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005037', 'US ALREADY ALL ANYMORE AND ANYMORE!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005038', 'Trainees! Keep practicing your Kick Procedure. It will be very useful to you in the cruel world outside these walls.', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005039', 'Heavy work this is...', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005040', 'I AM A GOD OF DEMONS!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005041', 'I WILL TAKE ALL LIFES!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005042', 'I WILL DESTROY YOU ALL!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005043', 'WHERE MY FORCES OF DEMONS!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005044', 'NOBODY WILL GET THROUGH ME!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005045', 'I FEEL THE FRIED MEAT!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005046', 'I WILL DESTROY YOU ALL!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005047', 'YOU PRAY SERVANTS MY!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005048', 'NOBODY WILL GET THROUGH ME!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005049', 'I WILL DESTROY ALL!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005050', 'I WICKED OGRE!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005051', 'I FEEL DEATH ALONGSIDE!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005052', 'US ALREADY ALL ANYMORE AND ANYMORE!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005053', 'I FEEL DEATH ALONGSIDE!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005054', 'There have been some complaints filed against this... Individual. I\'m told he\'s something of a shady character. I am here to determine whether or not this business of his is legitimate... and take action if it proves otherwise.', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005055', 'Welcome to Shattrath City.', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005056', 'You Seek Guidance?', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005057', 'I need to get this right, So watch your self !!!!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005058', 'No One May Pass!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005059', 'I need to get this right, So watch your self !!!!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005060', 'I need to get this right, So watch your self !!!!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005061', 'Troops! Fall out!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005062', 'The techniques he has suggested have been most effective in preparing the troops.', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005063', 'By your leave, High Exarch, I must prepare the next troops for your review.', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005064', 'Your training is now complete. You each have your assignments. May the Light guide you.', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005065', 'Listen up Men!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005066', 'I need to get this right, So watch your self !!!!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005067', 'Brashly you have attacked my children, Illidan!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005068', 'The pact is broken. Giant will never side with elf! NEVER!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005069', 'Behemothon, King of the Colossi roars in defiance.', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005070', 'I need to get this right, So watch your self !!!!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005071', 'Practice makes Perfect!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005072', 'Skarr the Unbreakable is in the arena! The Unbreakable is here now !', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005073', 'Fight with Skarr !', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005074', 'You will die or you will have tried ...', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005075', 'Mushgog is in the arena! Mushgog is here now !', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005076', 'Fight with Mushgog ! ', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005077', 'You will die or you will have tried ...', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005078', 'Razza is in the arena! Razza is here now !', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005079', 'Fight with Razza !', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005080', 'You will die or you will have tried ...', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005081', 'Half Done!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005082', 'Almost done with this side.', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005083', 'They sure screwed up this bridge..', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005084', 'Daniel is a good worker!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005085', 'Man! I will never beat him.', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005086', 'Great Work!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005087', 'Your doing great!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005088', 'Ahh, my precious Ameenah! How wonderful to see you again.', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005089', 'Yer wearin down, princess, I can sense it!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005090', 'Hmm, don\'t mind if I do!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005091', 'Beat it, Mack! Else I\'ll have Budd cook ya fer my hounds...', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005092', 'I\'ll never stop. Never...', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005093', 'It\'s getting away!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005094', 'Hey, someone help me catch this thing!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005095', 'THE KING WILL HAVE OUR HEADS, IF WE FALL BEHIND!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005096', 'As if I dont have better things to do in my old age than carry buckets of water.', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000022', 'Get in the Ring of Blood, $N. The fight is about to start!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000011', 'Ok, $N. Now i can go the the Auberdine alone. Go to Terenthis. He waits for you. Thanks for the help.', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000010', 'Oh, hello Grimclaw.', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000009', 'Oh no! They a here!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000012', 'Ok, goodluck friend. Thanks for the help.', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000018', 'Be patient $N. The torch is almost complete.', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000019', 'It is done...', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000016', 'I need to consult Noram and Horatio.', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000017', 'That\'s interesting...', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000029', 'And even more!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000028', 'Look out! More are coming!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000027', 'Look! Minions of Terokk are approaching!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000015', 'Whoops!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000026', 'Good luck stranger, and welcome to Shattrath City.', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000025', '...', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000014', 'Follow me.', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000030', 'An Avatar of Terokk! To arms!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000021', 'This makes sense!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000020', 'come on hurry up my friend.', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000013', 'Until we meet again, brave one.', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000031', 'let\'s get out of here !', null, null, null, null, null, null, null, null, '0', '168', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000032', 'let\'s get out of here !', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000000033', 'Thanks again.Sergeant Doryn will be glad to hear he has one less scout to replace this week.', null, null, null, null, null, null, null, null, '0', '0', '0', '0', null);
INSERT INTO `db_script_string` VALUES ('2000005554', 'The Ice Stone has melted!', null, null, null, null, null, null, null, null, '0', '0', '0', '22', 'say Ahune 1');
INSERT INTO `db_script_string` VALUES ('2000005555', 'Ahune, your strength grows no more!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', 'say Ahune 2');
INSERT INTO `db_script_string` VALUES ('2000005556', 'Your frozen reign will not come to pass!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', 'say Ahune 3');
INSERT INTO `db_script_string` VALUES ('2000005557', 'It is dying! The second part of the Cipher of Damnation is ours. I...', null, null, null, null, null, null, null, null, '0', '0', '0', '22', 'spirit hunter - say epilogue 1');
INSERT INTO `db_script_string` VALUES ('2000005558', 'I am fading... Return to Ar\'tor... Ret... rn... to...', null, null, null, null, null, null, null, null, '0', '0', '0', '0', 'spirit hunter - say epilogue 2');
INSERT INTO `db_script_string` VALUES ('2000005547', 'Oh no! Here they come!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', 'Weegli Blastfuse - say event start');
INSERT INTO `db_script_string` VALUES ('2000005548', 'Ok, here I go!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', 'Weegli Blastfuse - say blow door normal');
INSERT INTO `db_script_string` VALUES ('2000005549', 'What? How dare you say that to me?!?', null, null, null, null, null, null, null, null, '0', '0', '0', '6', 'Sergeant Bly - say faction change 1');
INSERT INTO `db_script_string` VALUES ('2000005550', 'After all we\'ve been through? Well, I didn\'t like you anyway!!', null, null, null, null, null, null, null, null, '0', '0', '0', '5', 'Sergeant Bly - say faction change 2');
INSERT INTO `db_script_string` VALUES ('2000005551', 'I\'m out of here!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', 'Weegli Blastfuse - say blow door forced');
INSERT INTO `db_script_string` VALUES ('2000005552', 'Who dares step into my domain! Come! Come, and be consumed!', null, null, null, null, null, null, null, null, '0', '6', '0', '0', 'Ukorz Sandscalp - say after door blown');
INSERT INTO `db_script_string` VALUES ('2000005553', 'Let\'s move forward!', null, null, null, null, null, null, null, null, '0', '0', '0', '0', 'Sergeant Bly - move downstairs');
