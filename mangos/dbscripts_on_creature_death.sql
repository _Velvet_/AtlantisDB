/*
Navicat MySQL Data Transfer

Source Server         : Hometown
Source Server Version : 50523
Source Host           : localhost:3306
Source Database       : mangos

Target Server Type    : MYSQL
Target Server Version : 50523
File Encoding         : 65001

Date: 2013-09-26 22:11:44
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `dbscripts_on_creature_death`
-- ----------------------------
DROP TABLE IF EXISTS `dbscripts_on_creature_death`;
CREATE TABLE `dbscripts_on_creature_death` (
  `id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `delay` int(10) unsigned NOT NULL DEFAULT '0',
  `command` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `datalong` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `datalong2` int(10) unsigned NOT NULL DEFAULT '0',
  `buddy_entry` int(10) unsigned NOT NULL DEFAULT '0',
  `search_radius` int(10) unsigned NOT NULL DEFAULT '0',
  `data_flags` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `dataint` int(11) NOT NULL DEFAULT '0',
  `dataint2` int(11) NOT NULL DEFAULT '0',
  `dataint3` int(11) NOT NULL DEFAULT '0',
  `dataint4` int(11) NOT NULL DEFAULT '0',
  `x` float NOT NULL DEFAULT '0',
  `y` float NOT NULL DEFAULT '0',
  `z` float NOT NULL DEFAULT '0',
  `o` float NOT NULL DEFAULT '0',
  `comments` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dbscripts_on_creature_death
-- ----------------------------
INSERT INTO `dbscripts_on_creature_death` VALUES ('20427', '0', '0', '0', '0', '21332', '100', '32', '2000005557', '0', '0', '0', '0', '0', '0', '0', 'say veneratus epilogue 1');
INSERT INTO `dbscripts_on_creature_death` VALUES ('20427', '3', '0', '0', '0', '21332', '100', '32', '2000005558', '0', '0', '0', '0', '0', '0', '0', 'say veneratus epilogue 2');
INSERT INTO `dbscripts_on_creature_death` VALUES ('20427', '5', '15', '36781', '0', '21332', '100', '32', '0', '0', '0', '0', '0', '0', '0', '0', 'cast Despawn Spirit Hunter');
INSERT INTO `dbscripts_on_creature_death` VALUES ('20427', '6', '14', '36620', '0', '0', '0', '6', '0', '0', '0', '0', '0', '0', '0', '0', 'remove Spirit Hunter aura');
INSERT INTO `dbscripts_on_creature_death` VALUES ('20427', '6', '18', '0', '0', '21332', '100', '32', '0', '0', '0', '0', '0', '0', '0', '0', 'despawn spirit hunter');
