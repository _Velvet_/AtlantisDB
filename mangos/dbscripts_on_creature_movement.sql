/*
Navicat MySQL Data Transfer

Source Server         : Hometown
Source Server Version : 50523
Source Host           : localhost:3306
Source Database       : mangos

Target Server Type    : MYSQL
Target Server Version : 50523
File Encoding         : 65001

Date: 2013-09-26 22:11:47
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `dbscripts_on_creature_movement`
-- ----------------------------
DROP TABLE IF EXISTS `dbscripts_on_creature_movement`;
CREATE TABLE `dbscripts_on_creature_movement` (
  `id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `delay` int(10) unsigned NOT NULL DEFAULT '0',
  `command` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `datalong` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `datalong2` int(10) unsigned NOT NULL DEFAULT '0',
  `buddy_entry` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `search_radius` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `data_flags` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `dataint` int(11) NOT NULL DEFAULT '0',
  `dataint2` int(11) NOT NULL DEFAULT '0',
  `dataint3` int(11) NOT NULL DEFAULT '0',
  `dataint4` int(11) NOT NULL DEFAULT '0',
  `x` float NOT NULL DEFAULT '0',
  `y` float NOT NULL DEFAULT '0',
  `z` float NOT NULL DEFAULT '0',
  `o` float NOT NULL DEFAULT '0',
  `comments` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dbscripts_on_creature_movement
-- ----------------------------
INSERT INTO `dbscripts_on_creature_movement` VALUES ('1763501', '0', '20', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Lordaeron Commander - set movement to idle');
INSERT INTO `dbscripts_on_creature_movement` VALUES ('1799501', '0', '20', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Lordaeron Veteran - set movement to idle');
INSERT INTO `dbscripts_on_creature_movement` VALUES ('760701', '0', '32', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Weegli Blastfuse - stop movement');
INSERT INTO `dbscripts_on_creature_movement` VALUES ('760401', '0', '32', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Sergeant Bly - stop movement');
INSERT INTO `dbscripts_on_creature_movement` VALUES ('760501', '0', '32', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Raven - stop movement');
INSERT INTO `dbscripts_on_creature_movement` VALUES ('760601', '0', '32', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Oro Eyegouge - stop movement');
INSERT INTO `dbscripts_on_creature_movement` VALUES ('760801', '0', '32', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Murta Grimgut - stop movement');
INSERT INTO `dbscripts_on_creature_movement` VALUES ('760702', '0', '1', '71', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Sergeant Bly - emote cheer');
INSERT INTO `dbscripts_on_creature_movement` VALUES ('760402', '0', '1', '71', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Raven - emote cheer');
INSERT INTO `dbscripts_on_creature_movement` VALUES ('760502', '0', '1', '71', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Oro Eyegouge - emote cheer');
INSERT INTO `dbscripts_on_creature_movement` VALUES ('760602', '0', '1', '71', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Weegli Blastfuse - emote cheer');
INSERT INTO `dbscripts_on_creature_movement` VALUES ('760802', '0', '1', '71', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Murta Grimgut - emote cheer');
INSERT INTO `dbscripts_on_creature_movement` VALUES ('760702', '0', '22', '495', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Weegli Blastfuse - update faction');
INSERT INTO `dbscripts_on_creature_movement` VALUES ('760402', '0', '22', '495', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Sergeant Bly - update faction');
INSERT INTO `dbscripts_on_creature_movement` VALUES ('760502', '0', '22', '495', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Raven - update faction');
INSERT INTO `dbscripts_on_creature_movement` VALUES ('760602', '0', '22', '495', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Oro Eyegouge - update faction');
INSERT INTO `dbscripts_on_creature_movement` VALUES ('760802', '0', '22', '495', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Murta Grimgut - update faction');
INSERT INTO `dbscripts_on_creature_movement` VALUES ('760706', '0', '25', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Weegli Blastfuse - set run on');
INSERT INTO `dbscripts_on_creature_movement` VALUES ('760707', '0', '0', '0', '0', '0', '0', '0', '2000005547', '0', '0', '0', '0', '0', '0', '0', 'Weegli Blastfuse - say event begin');
INSERT INTO `dbscripts_on_creature_movement` VALUES ('760713', '0', '15', '10772', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Weegli Blastfuse - cast Create Weegli\'s Barrel');
INSERT INTO `dbscripts_on_creature_movement` VALUES ('760713', '2', '13', '0', '0', '141612', '20', '1', '0', '0', '0', '0', '0', '0', '0', '0', 'Weegli Blastfuse - use Weegli\'s Barrel');
INSERT INTO `dbscripts_on_creature_movement` VALUES ('760713', '5', '0', '6', '0', '7267', '200', '0', '2000005552', '0', '0', '0', '0', '0', '0', '0', 'Ukorz Sandscalp - yell intro');
INSERT INTO `dbscripts_on_creature_movement` VALUES ('760409', '0', '15', '11365', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Sergeant Bly - cast Bly\'s Band\'s Escape');
INSERT INTO `dbscripts_on_creature_movement` VALUES ('760409', '0', '29', '1', '2', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Sergeant Bly - remove gossip flag');
INSERT INTO `dbscripts_on_creature_movement` VALUES ('760809', '0', '15', '11365', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Raven - cast Bly\'s Band\'s Escape');
INSERT INTO `dbscripts_on_creature_movement` VALUES ('760609', '0', '15', '11365', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Oro Eyegouge - cast Bly\'s Band\'s Escape');
INSERT INTO `dbscripts_on_creature_movement` VALUES ('760509', '0', '15', '11365', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Murta Grimgut - cast Bly\'s Band\'s Escape');
