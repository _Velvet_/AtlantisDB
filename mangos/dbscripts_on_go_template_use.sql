/*
Navicat MySQL Data Transfer

Source Server         : Hometown
Source Server Version : 50523
Source Host           : localhost:3306
Source Database       : mangos

Target Server Type    : MYSQL
Target Server Version : 50523
File Encoding         : 65001

Date: 2013-09-26 22:11:54
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `dbscripts_on_go_template_use`
-- ----------------------------
DROP TABLE IF EXISTS `dbscripts_on_go_template_use`;
CREATE TABLE `dbscripts_on_go_template_use` (
  `id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `delay` int(10) unsigned NOT NULL DEFAULT '0',
  `command` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `datalong` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `datalong2` int(10) unsigned NOT NULL DEFAULT '0',
  `buddy_entry` int(10) unsigned NOT NULL DEFAULT '0',
  `search_radius` int(10) unsigned NOT NULL DEFAULT '0',
  `data_flags` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `dataint` int(11) NOT NULL DEFAULT '0',
  `dataint2` int(11) NOT NULL DEFAULT '0',
  `dataint3` int(11) NOT NULL DEFAULT '0',
  `dataint4` int(11) NOT NULL DEFAULT '0',
  `x` float NOT NULL DEFAULT '0',
  `y` float NOT NULL DEFAULT '0',
  `z` float NOT NULL DEFAULT '0',
  `o` float NOT NULL DEFAULT '0',
  `comments` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dbscripts_on_go_template_use
-- ----------------------------
INSERT INTO `dbscripts_on_go_template_use` VALUES ('179985', '1', '10', '15041', '60000', '0', '0', '2', '0', '0', '0', '0', '0', '0', '0', '0', 'Spider Egg - Summon Spawn of Mar\'li');
INSERT INTO `dbscripts_on_go_template_use` VALUES ('182267', '0', '17', '24538', '10', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Add 10 Fire Bombs to inventory');
INSERT INTO `dbscripts_on_go_template_use` VALUES ('182267', '0', '30', '520', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Send Taxi path 520');
INSERT INTO `dbscripts_on_go_template_use` VALUES ('182280', '0', '17', '24538', '10', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Add 10 Fire Bombs to inventory');
INSERT INTO `dbscripts_on_go_template_use` VALUES ('182280', '0', '30', '523', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Send Taxi path 523');
INSERT INTO `dbscripts_on_go_template_use` VALUES ('182281', '0', '17', '24538', '10', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Add 10 Fire Bombs to inventory');
INSERT INTO `dbscripts_on_go_template_use` VALUES ('182281', '0', '30', '522', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Send Taxi path 522');
INSERT INTO `dbscripts_on_go_template_use` VALUES ('182282', '0', '17', '24538', '10', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Add 10 Fire Bombs to inventory');
INSERT INTO `dbscripts_on_go_template_use` VALUES ('182282', '0', '30', '524', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Send Taxi path 524');
INSERT INTO `dbscripts_on_go_template_use` VALUES ('182301', '0', '17', '24538', '10', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Add 10 Fire Bombs to inventory');
INSERT INTO `dbscripts_on_go_template_use` VALUES ('182301', '0', '30', '520', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Send Taxi path 520');
INSERT INTO `dbscripts_on_go_template_use` VALUES ('182302', '0', '17', '24538', '10', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Add 10 Fire Bombs to inventory');
INSERT INTO `dbscripts_on_go_template_use` VALUES ('182302', '0', '30', '523', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Send Taxi path 523');
INSERT INTO `dbscripts_on_go_template_use` VALUES ('182303', '0', '17', '24538', '10', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Add 10 Fire Bombs to inventory');
INSERT INTO `dbscripts_on_go_template_use` VALUES ('182303', '0', '30', '522', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Send Taxi path 522');
INSERT INTO `dbscripts_on_go_template_use` VALUES ('182304', '0', '17', '24538', '10', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Add 10 Fire Bombs to inventory');
INSERT INTO `dbscripts_on_go_template_use` VALUES ('182304', '0', '30', '524', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Send Taxi path 524');
INSERT INTO `dbscripts_on_go_template_use` VALUES ('185547', '1', '10', '22994', '60000', '0', '0', '8', '0', '0', '0', '0', '0', '0', '0', '0', 'summon Guardian of the Falcon');
INSERT INTO `dbscripts_on_go_template_use` VALUES ('185553', '1', '10', '22993', '60000', '0', '0', '8', '0', '0', '0', '0', '0', '0', '0', '0', 'summon Guardian of the Eagle');
INSERT INTO `dbscripts_on_go_template_use` VALUES ('185551', '1', '10', '22992', '60000', '0', '0', '8', '0', '0', '0', '0', '0', '0', '0', '0', 'summon Guardian of the Hawk');
