/*
Navicat MySQL Data Transfer

Source Server         : Hometown
Source Server Version : 50523
Source Host           : localhost:3306
Source Database       : mangos

Target Server Type    : MYSQL
Target Server Version : 50523
File Encoding         : 65001

Date: 2013-09-26 22:11:58
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `dbscripts_on_go_use`
-- ----------------------------
DROP TABLE IF EXISTS `dbscripts_on_go_use`;
CREATE TABLE `dbscripts_on_go_use` (
  `id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `delay` int(10) unsigned NOT NULL DEFAULT '0',
  `command` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `datalong` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `datalong2` int(10) unsigned NOT NULL DEFAULT '0',
  `buddy_entry` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `search_radius` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `data_flags` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `dataint` int(11) NOT NULL DEFAULT '0',
  `dataint2` int(11) NOT NULL DEFAULT '0',
  `dataint3` int(11) NOT NULL DEFAULT '0',
  `dataint4` int(11) NOT NULL DEFAULT '0',
  `x` float NOT NULL DEFAULT '0',
  `y` float NOT NULL DEFAULT '0',
  `z` float NOT NULL DEFAULT '0',
  `o` float NOT NULL DEFAULT '0',
  `comments` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dbscripts_on_go_use
-- ----------------------------
INSERT INTO `dbscripts_on_go_use` VALUES ('3139', '0', '11', '7439', '20', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '');
INSERT INTO `dbscripts_on_go_use` VALUES ('14120', '0', '11', '9670', '20', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '');
INSERT INTO `dbscripts_on_go_use` VALUES ('14721', '0', '11', '7439', '15', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '');
INSERT INTO `dbscripts_on_go_use` VALUES ('14722', '0', '11', '9670', '15', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '');
INSERT INTO `dbscripts_on_go_use` VALUES ('11894', '0', '11', '53375', '300', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '');
INSERT INTO `dbscripts_on_go_use` VALUES ('11881', '0', '11', '21908', '20', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '');
INSERT INTO `dbscripts_on_go_use` VALUES ('59258', '3', '10', '6492', '300000', '0', '0', '0', '0', '0', '0', '0', '-9098.22', '837.644', '97.6282', '3.19264', '');
INSERT INTO `dbscripts_on_go_use` VALUES ('59258', '3', '10', '6492', '300000', '0', '0', '0', '0', '0', '0', '0', '-9103.27', '832.988', '97.6282', '2.61145', '');
INSERT INTO `dbscripts_on_go_use` VALUES ('59258', '3', '10', '6492', '300000', '0', '0', '0', '0', '0', '0', '0', '-9109.02', '830.471', '97.6264', '1.32339', '');
INSERT INTO `dbscripts_on_go_use` VALUES ('59258', '3', '10', '6492', '300000', '0', '0', '0', '0', '0', '0', '0', '-9113.81', '834.917', '97.6263', '6.10176', '');
INSERT INTO `dbscripts_on_go_use` VALUES ('59257', '3', '10', '6492', '300000', '0', '0', '0', '0', '0', '0', '0', '1406.23', '335.627', '-66', '4.15', '');
INSERT INTO `dbscripts_on_go_use` VALUES ('59257', '3', '10', '6492', '300000', '0', '0', '0', '0', '0', '0', '0', '1416.12', '358.485', '-66', '1.162', '');
INSERT INTO `dbscripts_on_go_use` VALUES ('59257', '3', '10', '6492', '300000', '0', '0', '0', '0', '0', '0', '0', '1411.44', '343.888', '-66', '5.275', '');
INSERT INTO `dbscripts_on_go_use` VALUES ('59257', '3', '10', '6492', '300000', '0', '0', '0', '0', '0', '0', '0', '1403.48', '356.513', '-66', '5.263', '');
INSERT INTO `dbscripts_on_go_use` VALUES ('4499', '0', '11', '53411', '15', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '');
INSERT INTO `dbscripts_on_go_use` VALUES ('16113', '0', '11', '52980', '15', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '');
INSERT INTO `dbscripts_on_go_use` VALUES ('51770', '0', '11', '21907', '15', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '');
INSERT INTO `dbscripts_on_go_use` VALUES ('59265', '0', '11', '21898', '15', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '');
INSERT INTO `dbscripts_on_go_use` VALUES ('31676', '0', '11', '32978', '15', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '');
INSERT INTO `dbscripts_on_go_use` VALUES ('31677', '0', '11', '31438', '15', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '');
INSERT INTO `dbscripts_on_go_use` VALUES ('33117', '0', '11', '32294', '15', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '');
INSERT INTO `dbscripts_on_go_use` VALUES ('33291', '0', '11', '59266', '15', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '');
INSERT INTO `dbscripts_on_go_use` VALUES ('33294', '0', '11', '59267', '15', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '');
INSERT INTO `dbscripts_on_go_use` VALUES ('33299', '0', '11', '59268', '15', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '');
INSERT INTO `dbscripts_on_go_use` VALUES ('59269', '0', '11', '21896', '15', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '');
INSERT INTO `dbscripts_on_go_use` VALUES ('59270', '0', '11', '59271', '120', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '');
INSERT INTO `dbscripts_on_go_use` VALUES ('59272', '0', '11', '21904', '120', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '');
INSERT INTO `dbscripts_on_go_use` VALUES ('59273', '0', '11', '59274', '15', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '');
INSERT INTO `dbscripts_on_go_use` VALUES ('53447', '0', '11', '53452', '10000', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '');
INSERT INTO `dbscripts_on_go_use` VALUES ('53448', '0', '11', '53452', '10000', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '');
INSERT INTO `dbscripts_on_go_use` VALUES ('53449', '0', '11', '53452', '10000', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '');
INSERT INTO `dbscripts_on_go_use` VALUES ('53450', '0', '11', '53452', '10000', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '');
INSERT INTO `dbscripts_on_go_use` VALUES ('100001', '3', '10', '22930', '990000', '0', '0', '0', '0', '0', '0', '0', '-224.035', '-9.92103', '16.6866', '0.130538', '');
INSERT INTO `dbscripts_on_go_use` VALUES ('66908', '2', '25', '1', '0', '14020', '70', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Set Chromaggus run = true');
INSERT INTO `dbscripts_on_go_use` VALUES ('66908', '3', '3', '0', '0', '14020', '70', '0', '0', '0', '0', '0', '-7484.91', '-1072.98', '476.55', '2.18', 'Move Chromaggus in the center of the room');
INSERT INTO `dbscripts_on_go_use` VALUES ('66908', '1', '11', '66900', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Open Chromaggus side door');
