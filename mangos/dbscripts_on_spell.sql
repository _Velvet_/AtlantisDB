/*
Navicat MySQL Data Transfer

Source Server         : Hometown
Source Server Version : 50523
Source Host           : localhost:3306
Source Database       : mangos

Target Server Type    : MYSQL
Target Server Version : 50523
File Encoding         : 65001

Date: 2013-09-26 22:12:11
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `dbscripts_on_spell`
-- ----------------------------
DROP TABLE IF EXISTS `dbscripts_on_spell`;
CREATE TABLE `dbscripts_on_spell` (
  `id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `delay` int(10) unsigned NOT NULL DEFAULT '0',
  `command` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `datalong` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `datalong2` int(10) unsigned NOT NULL DEFAULT '0',
  `buddy_entry` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `search_radius` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `data_flags` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `dataint` int(11) NOT NULL DEFAULT '0',
  `dataint2` int(11) NOT NULL DEFAULT '0',
  `dataint3` int(11) NOT NULL DEFAULT '0',
  `dataint4` int(11) NOT NULL DEFAULT '0',
  `x` float NOT NULL DEFAULT '0',
  `y` float NOT NULL DEFAULT '0',
  `z` float NOT NULL DEFAULT '0',
  `o` float NOT NULL DEFAULT '0',
  `comments` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dbscripts_on_spell
-- ----------------------------
INSERT INTO `dbscripts_on_spell` VALUES ('25650', '0', '6', '530', '0', '0', '0', '0', '0', '0', '0', '0', '-593.429', '4077.95', '93.8245', '5.32893', '');
INSERT INTO `dbscripts_on_spell` VALUES ('25652', '0', '6', '530', '0', '0', '0', '0', '0', '0', '0', '0', '-589.976', '4078.31', '143.258', '4.48305', '');
INSERT INTO `dbscripts_on_spell` VALUES ('41931', '0', '10', '11876', '180000', '0', '0', '0', '0', '0', '0', '0', '-348.231', '1763.85', '138.371', '4.42728', '');
INSERT INTO `dbscripts_on_spell` VALUES ('29395', '0', '10', '17035', '200000', '0', '0', '0', '0', '0', '0', '0', '-985.932', '4227.08', '42.4585', '1.5732', '');
INSERT INTO `dbscripts_on_spell` VALUES ('29395', '0', '10', '17035', '200000', '0', '0', '0', '0', '0', '0', '0', '-1076.14', '4176.77', '38.1325', '-0.872665', '');
INSERT INTO `dbscripts_on_spell` VALUES ('29395', '0', '10', '17035', '200000', '0', '0', '0', '0', '0', '0', '0', '-1097.43', '4250.01', '16.8586', '1.45762', '');
INSERT INTO `dbscripts_on_spell` VALUES ('29395', '0', '10', '17035', '200000', '0', '0', '0', '0', '0', '0', '0', '-1100.84', '4254.16', '16.1055', '0.0330392', '');
INSERT INTO `dbscripts_on_spell` VALUES ('29395', '0', '10', '17035', '200000', '0', '0', '0', '0', '0', '0', '0', '-1102.93', '4210.66', '55.6402', '-0.733038', '');
INSERT INTO `dbscripts_on_spell` VALUES ('29395', '0', '10', '17035', '200000', '0', '0', '0', '0', '0', '0', '0', '-1108.3', '4177.58', '40.9812', '0.163756', '');
INSERT INTO `dbscripts_on_spell` VALUES ('29395', '0', '10', '17035', '200000', '0', '0', '0', '0', '0', '0', '0', '-1116.22', '4181.1', '19.4384', '4.79727', '');
INSERT INTO `dbscripts_on_spell` VALUES ('29395', '0', '10', '17035', '200000', '0', '0', '0', '0', '0', '0', '0', '-1137.26', '4242.49', '14.0351', '4.87109', '');
INSERT INTO `dbscripts_on_spell` VALUES ('29395', '0', '10', '17035', '200000', '0', '0', '0', '0', '0', '0', '0', '-1141.49', '4209.96', '50.3676', '0.718242', '');
INSERT INTO `dbscripts_on_spell` VALUES ('29395', '0', '10', '17035', '200000', '0', '0', '0', '0', '0', '0', '0', '-1151.03', '4263.01', '13.9897', '3.04112', '');
INSERT INTO `dbscripts_on_spell` VALUES ('29395', '0', '10', '17035', '200000', '0', '0', '0', '0', '0', '0', '0', '-1167.66', '4214.58', '49.9546', '-2.46091', '');
INSERT INTO `dbscripts_on_spell` VALUES ('29395', '0', '10', '17035', '200000', '0', '0', '0', '0', '0', '0', '0', '-1199.55', '4115.92', '61.2455', '6.13243', '');
INSERT INTO `dbscripts_on_spell` VALUES ('29395', '0', '10', '17035', '200000', '0', '0', '0', '0', '0', '0', '0', '-1325.42', '4041.12', '116.713', '3.96866', '');
INSERT INTO `dbscripts_on_spell` VALUES ('29395', '0', '10', '17035', '200000', '0', '0', '0', '0', '0', '0', '0', '-1332.81', '4061.28', '116.803', '2.00124', '');
INSERT INTO `dbscripts_on_spell` VALUES ('39291', '0', '10', '22452', '600000', '0', '0', '0', '0', '0', '0', '0', '-3361.74', '5151.89', '-9.00056', '1.55138', '');
INSERT INTO `dbscripts_on_spell` VALUES ('37834', '2', '7', '10637', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '');
INSERT INTO `dbscripts_on_spell` VALUES ('37834', '2', '7', '10688', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '');
INSERT INTO `dbscripts_on_spell` VALUES ('37751', '0', '2', '159', '9', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '');
INSERT INTO `dbscripts_on_spell` VALUES ('37751', '1', '1', '373', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '');
INSERT INTO `dbscripts_on_spell` VALUES ('37751', '1', '4', '46', '33554434', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '');
INSERT INTO `dbscripts_on_spell` VALUES ('37752', '1', '5', '46', '33554434', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '');
INSERT INTO `dbscripts_on_spell` VALUES ('37752', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '');
INSERT INTO `dbscripts_on_spell` VALUES ('37752', '1', '1', '26', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '');
INSERT INTO `dbscripts_on_spell` VALUES ('34874', '0', '18', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Despawn Underbog Mushroom');
INSERT INTO `dbscripts_on_spell` VALUES ('46650', '0', '15', '46652', '0', '0', '0', '8', '0', '0', '0', '0', '0', '0', '0', '0', 'Cast Open Brutallus Back Door on Fire Barrier');
INSERT INTO `dbscripts_on_spell` VALUES ('46609', '0', '15', '46610', '0', '0', '0', '8', '0', '0', '0', '0', '0', '0', '0', '0', 'Cast Freeze on Ice Barrier');
INSERT INTO `dbscripts_on_spell` VALUES ('37366', '0', '18', '30000', '0', '0', '0', '6', '0', '0', '0', '0', '0', '0', '0', '0', 'despawn self');
INSERT INTO `dbscripts_on_spell` VALUES ('39398', '0', '18', '30000', '0', '0', '0', '6', '0', '0', '0', '0', '0', '0', '0', '0', 'despawn self');
INSERT INTO `dbscripts_on_spell` VALUES ('45930', '0', '15', '46339', '0', '0', '0', '6', '0', '0', '0', '0', '0', '0', '0', '0', 'Cast Bonfire Disguise');
INSERT INTO `dbscripts_on_spell` VALUES ('45941', '0', '15', '45939', '0', '0', '0', '6', '0', '0', '0', '0', '0', '0', '0', '0', 'cast Summon Ahune\'s Loot');
INSERT INTO `dbscripts_on_spell` VALUES ('46623', '0', '15', '46622', '0', '0', '0', '6', '0', '0', '0', '0', '0', '0', '0', '0', 'cast Summon Ahune\'s Loot, Heroic');
INSERT INTO `dbscripts_on_spell` VALUES ('48218', '0', '8', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'create from quest_template');
INSERT INTO `dbscripts_on_spell` VALUES ('11365', '0', '18', '1000', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'despawn self');
