/*
Navicat MySQL Data Transfer

Source Server         : Hometown
Source Server Version : 50523
Source Host           : localhost:3306
Source Database       : mangos

Target Server Type    : MYSQL
Target Server Version : 50523
File Encoding         : 65001

Date: 2013-09-26 22:12:29
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `game_event`
-- ----------------------------
DROP TABLE IF EXISTS `game_event`;
CREATE TABLE `game_event` (
  `entry` mediumint(8) unsigned NOT NULL COMMENT 'Entry of the game event',
  `start_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Absolute start date, the event will never start before',
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Absolute end date, the event will never start afler',
  `occurence` bigint(20) unsigned NOT NULL DEFAULT '5184000' COMMENT 'Delay in minutes between occurences of the event',
  `length` bigint(20) unsigned NOT NULL DEFAULT '2592000' COMMENT 'Length in minutes of the event',
  `holiday` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `description` varchar(255) DEFAULT NULL COMMENT 'Description of the event displayed in console',
  PRIMARY KEY (`entry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of game_event
-- ----------------------------
INSERT INTO `game_event` VALUES ('1000', '2007-03-03 13:00:00', '2020-01-01 13:00:00', '131400', '10080', '0', 'Darkmoon Faire Elwynn');
INSERT INTO `game_event` VALUES ('1001', '2007-03-01 13:00:00', '2020-01-01 13:00:00', '131400', '4320', '0', 'Darkmoon Farie Elwynn PRE');
INSERT INTO `game_event` VALUES ('1100', '2007-01-03 13:00:00', '2020-01-01 13:00:00', '131400', '10080', '0', 'Darkmoon Farie Mulgore');
INSERT INTO `game_event` VALUES ('1101', '2007-01-01 13:00:00', '2020-01-01 13:00:00', '131400', '4320', '0', 'Darkmoon Farie Mulgore PRE');
INSERT INTO `game_event` VALUES ('1200', '2007-02-03 13:00:00', '2020-01-01 13:00:00', '131400', '10080', '0', 'Darkmoon Farie Terokkar');
INSERT INTO `game_event` VALUES ('1201', '2007-02-01 13:00:00', '2020-01-01 13:00:00', '131400', '4320', '0', 'Darkmoon Farie Terokkar PRE');
INSERT INTO `game_event` VALUES ('400', '2007-01-01 13:00:00', '2020-01-01 13:00:00', '180', '30', '0', 'Lil Timmy');
INSERT INTO `game_event` VALUES ('500', '2007-01-01 13:00:00', '2020-01-01 13:00:00', '180', '60', '0', 'Gurubashi Arena Booty Run');
INSERT INTO `game_event` VALUES ('5000', '2007-12-15 13:00:00', '2020-01-02 13:00:00', '525600', '25920', '0', 'Feast of Winter Veil');
INSERT INTO `game_event` VALUES ('9', '2007-08-12 15:00:00', '2010-01-01 01:00:00', '1320', '240', '0', 'Gurubashi Arena Call to Fight (Day)');
INSERT INTO `game_event` VALUES ('10', '2007-08-12 22:00:00', '2010-01-01 01:00:00', '660', '480', '0', 'Gurubashi Arena Call to Fight (Night)');
INSERT INTO `game_event` VALUES ('1', '2007-08-12 15:00:00', '2010-01-01 01:00:00', '1320', '120', '0', 'Orgrimmar & Stormwind Portal to Gurubashi Arena');
INSERT INTO `game_event` VALUES ('3150', '2007-09-10 07:00:00', '2020-01-02 11:00:00', '1090', '60', '0', 'Dire Maul Arena Skarr The Unbreakable');
INSERT INTO `game_event` VALUES ('3151', '2007-09-10 15:00:00', '2020-01-02 17:00:00', '1090', '60', '0', 'Dire Maul Arena Mushgog');
INSERT INTO `game_event` VALUES ('3152', '2007-09-10 23:00:00', '2020-01-02 23:00:00', '1090', '60', '0', 'Dire Maul Arena The Razza');
INSERT INTO `game_event` VALUES ('11', '2007-09-16 15:00:00', '2020-09-16 15:00:00', '10080', '180', '0', 'Fishing Extravaganza By Sanaell');
INSERT INTO `game_event` VALUES ('300', '2008-06-21 01:00:00', '2020-12-31 01:00:00', '525600', '21600', '0', 'Midsummer Fire Festival');
INSERT INTO `game_event` VALUES ('301', '2008-01-01 00:00:00', '2020-12-31 01:00:00', '525600', '2880', '0', 'New Year\'s Eve');
INSERT INTO `game_event` VALUES ('302', '2008-02-16 13:00:00', '2020-01-01 13:00:00', '525600', '31680', '0', 'Lunar Festival');
INSERT INTO `game_event` VALUES ('303', '2008-02-11 01:00:00', '2020-12-31 01:00:00', '525600', '5760', '0', 'Love is in the Air');
INSERT INTO `game_event` VALUES ('304', '2007-04-09 01:00:00', '2020-12-31 01:00:00', '524160', '1440', '0', 'Noblegarden');
INSERT INTO `game_event` VALUES ('305', '2008-05-22 01:00:00', '2020-12-31 01:00:00', '525600', '10080', '0', 'Children\'s Week ');
INSERT INTO `game_event` VALUES ('306', '2008-09-24 01:00:00', '2020-12-31 01:00:00', '525600', '10080', '0', 'Harvest Festival');
INSERT INTO `game_event` VALUES ('307', '2008-10-18 01:00:00', '2020-12-31 01:00:00', '525600', '20160', '0', 'Hallow\'s End');
INSERT INTO `game_event` VALUES ('311', '2007-08-10 00:00:00', '2020-12-31 01:00:00', '40320', '6240', '0', 'Call to Arms: Warsong Gulch!');
INSERT INTO `game_event` VALUES ('312', '2007-08-17 00:00:00', '2020-12-31 01:00:00', '40320', '6240', '0', 'Call to Arms: Arathi Basin!');
INSERT INTO `game_event` VALUES ('315', '2007-08-31 06:00:00', '2020-12-31 07:00:00', '40320', '6240', '0', 'Call to Arms: Alterac Valley!');
INSERT INTO `game_event` VALUES ('316', '2008-10-02 01:00:00', '2020-12-31 01:00:00', '525600', '20160', '0', 'Brewfest');
INSERT INTO `game_event` VALUES ('318', '2007-12-03 15:05:00', '2020-12-31 01:00:00', '1090', '60', '0', 'Zulian Stalker1');
INSERT INTO `game_event` VALUES ('319', '2007-12-03 07:05:00', '2020-12-31 01:00:00', '1090', '60', '0', 'Zulian Stalker2 ');
INSERT INTO `game_event` VALUES ('320', '2007-12-03 23:05:00', '2020-12-31 01:00:00', '1090', '60', '0', 'Zulian Stalker3');
INSERT INTO `game_event` VALUES ('317', '2007-08-24 06:00:00', '2020-12-31 07:00:00', '40320', '6240', '0', 'Call to Arms: Eye of the Storm!');
INSERT INTO `game_event` VALUES ('27', '2008-01-02 03:00:00', '2020-12-31 06:00:00', '1440', '720', '0', 'Nights');
INSERT INTO `game_event` VALUES ('34', '2008-05-15 21:00:00', '2020-01-01 08:00:00', '10080', '5', '0', 'L70ETC Concert');
INSERT INTO `game_event` VALUES ('2', '2008-08-08 07:00:00', '2020-08-24 07:00:00', '525600', '23040', '0', 'The Spirit of Competition');
INSERT INTO `game_event` VALUES ('1402', '2012-04-07 15:00:00', '2020-01-01 00:00:00', '1440', '510', '0', 'Arena Battlemasters Event');
INSERT INTO `game_event` VALUES ('1403', '2012-04-11 23:30:00', '2020-01-01 00:00:00', '10080', '60', '0', 'Arena Battlemasters Event Wednesday');
