/*
Navicat MySQL Data Transfer

Source Server         : Hometown
Source Server Version : 50523
Source Host           : localhost:3306
Source Database       : mangos

Target Server Type    : MYSQL
Target Server Version : 50523
File Encoding         : 65001

Date: 2013-09-26 22:12:34
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `game_event_creature`
-- ----------------------------
DROP TABLE IF EXISTS `game_event_creature`;
CREATE TABLE `game_event_creature` (
  `guid` int(10) unsigned NOT NULL,
  `event` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Put negatives values to remove during event',
  PRIMARY KEY (`guid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of game_event_creature
-- ----------------------------
INSERT INTO `game_event_creature` VALUES ('5006', '1000');
INSERT INTO `game_event_creature` VALUES ('52726', '1000');
INSERT INTO `game_event_creature` VALUES ('4980', '1000');
INSERT INTO `game_event_creature` VALUES ('4975', '1000');
INSERT INTO `game_event_creature` VALUES ('4982', '1000');
INSERT INTO `game_event_creature` VALUES ('4969', '1000');
INSERT INTO `game_event_creature` VALUES ('4955', '1000');
INSERT INTO `game_event_creature` VALUES ('4958', '1000');
INSERT INTO `game_event_creature` VALUES ('4930', '1000');
INSERT INTO `game_event_creature` VALUES ('4939', '1000');
INSERT INTO `game_event_creature` VALUES ('4905', '1000');
INSERT INTO `game_event_creature` VALUES ('4903', '1000');
INSERT INTO `game_event_creature` VALUES ('4898', '1000');
INSERT INTO `game_event_creature` VALUES ('4880', '1000');
INSERT INTO `game_event_creature` VALUES ('4899', '1000');
INSERT INTO `game_event_creature` VALUES ('4901', '1000');
INSERT INTO `game_event_creature` VALUES ('4957', '1000');
INSERT INTO `game_event_creature` VALUES ('4938', '1000');
INSERT INTO `game_event_creature` VALUES ('125147', '1200');
INSERT INTO `game_event_creature` VALUES ('4907', '1000');
INSERT INTO `game_event_creature` VALUES ('4906', '1000');
INSERT INTO `game_event_creature` VALUES ('82222', '1000');
INSERT INTO `game_event_creature` VALUES ('125164', '1200');
INSERT INTO `game_event_creature` VALUES ('51373', '1100');
INSERT INTO `game_event_creature` VALUES ('51372', '1100');
INSERT INTO `game_event_creature` VALUES ('51371', '1100');
INSERT INTO `game_event_creature` VALUES ('51370', '1100');
INSERT INTO `game_event_creature` VALUES ('51366', '1100');
INSERT INTO `game_event_creature` VALUES ('51367', '1100');
INSERT INTO `game_event_creature` VALUES ('51378', '1100');
INSERT INTO `game_event_creature` VALUES ('51377', '1100');
INSERT INTO `game_event_creature` VALUES ('51376', '1100');
INSERT INTO `game_event_creature` VALUES ('51375', '1100');
INSERT INTO `game_event_creature` VALUES ('51374', '1100');
INSERT INTO `game_event_creature` VALUES ('51368', '1100');
INSERT INTO `game_event_creature` VALUES ('51369', '1100');
INSERT INTO `game_event_creature` VALUES ('51365', '1100');
INSERT INTO `game_event_creature` VALUES ('47354', '1100');
INSERT INTO `game_event_creature` VALUES ('51363', '1100');
INSERT INTO `game_event_creature` VALUES ('47505', '1100');
INSERT INTO `game_event_creature` VALUES ('51364', '1100');
INSERT INTO `game_event_creature` VALUES ('47436', '1100');
INSERT INTO `game_event_creature` VALUES ('47307', '1100');
INSERT INTO `game_event_creature` VALUES ('52730', '1100');
INSERT INTO `game_event_creature` VALUES ('79682', '300');
INSERT INTO `game_event_creature` VALUES ('79683', '300');
INSERT INTO `game_event_creature` VALUES ('79684', '300');
INSERT INTO `game_event_creature` VALUES ('79685', '300');
INSERT INTO `game_event_creature` VALUES ('6299', '400');
INSERT INTO `game_event_creature` VALUES ('52734', '5000');
INSERT INTO `game_event_creature` VALUES ('86685', '5000');
INSERT INTO `game_event_creature` VALUES ('9365', '5000');
INSERT INTO `game_event_creature` VALUES ('92197', '5000');
INSERT INTO `game_event_creature` VALUES ('9298', '5000');
INSERT INTO `game_event_creature` VALUES ('18799', '5000');
INSERT INTO `game_event_creature` VALUES ('9290', '5000');
INSERT INTO `game_event_creature` VALUES ('53263', '5000');
INSERT INTO `game_event_creature` VALUES ('60935', '5000');
INSERT INTO `game_event_creature` VALUES ('80311', '5000');
INSERT INTO `game_event_creature` VALUES ('53259', '5000');
INSERT INTO `game_event_creature` VALUES ('53262', '5000');
INSERT INTO `game_event_creature` VALUES ('73003', '5000');
INSERT INTO `game_event_creature` VALUES ('69172', '5000');
INSERT INTO `game_event_creature` VALUES ('52752', '5000');
INSERT INTO `game_event_creature` VALUES ('153', '9');
INSERT INTO `game_event_creature` VALUES ('98870', '9');
INSERT INTO `game_event_creature` VALUES ('343', '9');
INSERT INTO `game_event_creature` VALUES ('98869', '9');
INSERT INTO `game_event_creature` VALUES ('98882', '1');
INSERT INTO `game_event_creature` VALUES ('98883', '1');
INSERT INTO `game_event_creature` VALUES ('120007', '300');
INSERT INTO `game_event_creature` VALUES ('120006', '300');
INSERT INTO `game_event_creature` VALUES ('120008', '300');
INSERT INTO `game_event_creature` VALUES ('120009', '300');
INSERT INTO `game_event_creature` VALUES ('120005', '300');
INSERT INTO `game_event_creature` VALUES ('100016', '3150');
INSERT INTO `game_event_creature` VALUES ('100017', '3150');
INSERT INTO `game_event_creature` VALUES ('100019', '3151');
INSERT INTO `game_event_creature` VALUES ('100021', '3152');
INSERT INTO `game_event_creature` VALUES ('52741', '11');
INSERT INTO `game_event_creature` VALUES ('52740', '11');
INSERT INTO `game_event_creature` VALUES ('101178', '11');
INSERT INTO `game_event_creature` VALUES ('121700', '318');
INSERT INTO `game_event_creature` VALUES ('121701', '319');
INSERT INTO `game_event_creature` VALUES ('121702', '320');
INSERT INTO `game_event_creature` VALUES ('120004', '300');
INSERT INTO `game_event_creature` VALUES ('120002', '300');
INSERT INTO `game_event_creature` VALUES ('98105', '300');
INSERT INTO `game_event_creature` VALUES ('93944', '300');
INSERT INTO `game_event_creature` VALUES ('91414', '300');
INSERT INTO `game_event_creature` VALUES ('90894', '300');
INSERT INTO `game_event_creature` VALUES ('90122', '300');
INSERT INTO `game_event_creature` VALUES ('87241', '300');
INSERT INTO `game_event_creature` VALUES ('87240', '300');
INSERT INTO `game_event_creature` VALUES ('87219', '300');
INSERT INTO `game_event_creature` VALUES ('87218', '300');
INSERT INTO `game_event_creature` VALUES ('77593', '300');
INSERT INTO `game_event_creature` VALUES ('58028', '300');
INSERT INTO `game_event_creature` VALUES ('58027', '300');
INSERT INTO `game_event_creature` VALUES ('120210', '316');
INSERT INTO `game_event_creature` VALUES ('120209', '316');
INSERT INTO `game_event_creature` VALUES ('120207', '316');
INSERT INTO `game_event_creature` VALUES ('120206', '316');
INSERT INTO `game_event_creature` VALUES ('120205', '316');
INSERT INTO `game_event_creature` VALUES ('120204', '316');
INSERT INTO `game_event_creature` VALUES ('120203', '316');
INSERT INTO `game_event_creature` VALUES ('120202', '316');
INSERT INTO `game_event_creature` VALUES ('120201', '316');
INSERT INTO `game_event_creature` VALUES ('120200', '316');
INSERT INTO `game_event_creature` VALUES ('120198', '316');
INSERT INTO `game_event_creature` VALUES ('120196', '316');
INSERT INTO `game_event_creature` VALUES ('120194', '316');
INSERT INTO `game_event_creature` VALUES ('120193', '316');
INSERT INTO `game_event_creature` VALUES ('120192', '316');
INSERT INTO `game_event_creature` VALUES ('120191', '316');
INSERT INTO `game_event_creature` VALUES ('120190', '316');
INSERT INTO `game_event_creature` VALUES ('120189', '316');
INSERT INTO `game_event_creature` VALUES ('120187', '316');
INSERT INTO `game_event_creature` VALUES ('120186', '316');
INSERT INTO `game_event_creature` VALUES ('120185', '316');
INSERT INTO `game_event_creature` VALUES ('120184', '316');
INSERT INTO `game_event_creature` VALUES ('120183', '316');
INSERT INTO `game_event_creature` VALUES ('120180', '316');
INSERT INTO `game_event_creature` VALUES ('120179', '316');
INSERT INTO `game_event_creature` VALUES ('120178', '316');
INSERT INTO `game_event_creature` VALUES ('120176', '316');
INSERT INTO `game_event_creature` VALUES ('120175', '316');
INSERT INTO `game_event_creature` VALUES ('120174', '316');
INSERT INTO `game_event_creature` VALUES ('120173', '316');
INSERT INTO `game_event_creature` VALUES ('120171', '316');
INSERT INTO `game_event_creature` VALUES ('120170', '316');
INSERT INTO `game_event_creature` VALUES ('120169', '316');
INSERT INTO `game_event_creature` VALUES ('120168', '316');
INSERT INTO `game_event_creature` VALUES ('120167', '316');
INSERT INTO `game_event_creature` VALUES ('120164', '316');
INSERT INTO `game_event_creature` VALUES ('120163', '316');
INSERT INTO `game_event_creature` VALUES ('120162', '316');
INSERT INTO `game_event_creature` VALUES ('120161', '316');
INSERT INTO `game_event_creature` VALUES ('120158', '316');
INSERT INTO `game_event_creature` VALUES ('120157', '316');
INSERT INTO `game_event_creature` VALUES ('120156', '316');
INSERT INTO `game_event_creature` VALUES ('120155', '316');
INSERT INTO `game_event_creature` VALUES ('120154', '316');
INSERT INTO `game_event_creature` VALUES ('120153', '316');
INSERT INTO `game_event_creature` VALUES ('120152', '316');
INSERT INTO `game_event_creature` VALUES ('120151', '316');
INSERT INTO `game_event_creature` VALUES ('120150', '316');
INSERT INTO `game_event_creature` VALUES ('120149', '316');
INSERT INTO `game_event_creature` VALUES ('120148', '316');
INSERT INTO `game_event_creature` VALUES ('120147', '316');
INSERT INTO `game_event_creature` VALUES ('120146', '316');
INSERT INTO `game_event_creature` VALUES ('120145', '316');
INSERT INTO `game_event_creature` VALUES ('120144', '316');
INSERT INTO `game_event_creature` VALUES ('120143', '316');
INSERT INTO `game_event_creature` VALUES ('120142', '316');
INSERT INTO `game_event_creature` VALUES ('120141', '316');
INSERT INTO `game_event_creature` VALUES ('120140', '316');
INSERT INTO `game_event_creature` VALUES ('120139', '316');
INSERT INTO `game_event_creature` VALUES ('120137', '316');
INSERT INTO `game_event_creature` VALUES ('120136', '316');
INSERT INTO `game_event_creature` VALUES ('120133', '316');
INSERT INTO `game_event_creature` VALUES ('120132', '316');
INSERT INTO `game_event_creature` VALUES ('120130', '316');
INSERT INTO `game_event_creature` VALUES ('120128', '316');
INSERT INTO `game_event_creature` VALUES ('120126', '316');
INSERT INTO `game_event_creature` VALUES ('120125', '316');
INSERT INTO `game_event_creature` VALUES ('120124', '316');
INSERT INTO `game_event_creature` VALUES ('120123', '316');
INSERT INTO `game_event_creature` VALUES ('120122', '316');
INSERT INTO `game_event_creature` VALUES ('120121', '316');
INSERT INTO `game_event_creature` VALUES ('120120', '316');
INSERT INTO `game_event_creature` VALUES ('120114', '316');
INSERT INTO `game_event_creature` VALUES ('120113', '316');
INSERT INTO `game_event_creature` VALUES ('120112', '316');
INSERT INTO `game_event_creature` VALUES ('120111', '316');
INSERT INTO `game_event_creature` VALUES ('120110', '316');
INSERT INTO `game_event_creature` VALUES ('120109', '316');
INSERT INTO `game_event_creature` VALUES ('120108', '316');
INSERT INTO `game_event_creature` VALUES ('120107', '316');
INSERT INTO `game_event_creature` VALUES ('120106', '316');
INSERT INTO `game_event_creature` VALUES ('120105', '316');
INSERT INTO `game_event_creature` VALUES ('120104', '316');
INSERT INTO `game_event_creature` VALUES ('120103', '316');
INSERT INTO `game_event_creature` VALUES ('120102', '316');
INSERT INTO `game_event_creature` VALUES ('120101', '316');
INSERT INTO `game_event_creature` VALUES ('120100', '316');
INSERT INTO `game_event_creature` VALUES ('120099', '316');
INSERT INTO `game_event_creature` VALUES ('120098', '316');
INSERT INTO `game_event_creature` VALUES ('120097', '316');
INSERT INTO `game_event_creature` VALUES ('120025', '307');
INSERT INTO `game_event_creature` VALUES ('99590', '307');
INSERT INTO `game_event_creature` VALUES ('120045', '307');
INSERT INTO `game_event_creature` VALUES ('120044', '307');
INSERT INTO `game_event_creature` VALUES ('120043', '307');
INSERT INTO `game_event_creature` VALUES ('120039', '307');
INSERT INTO `game_event_creature` VALUES ('120059', '307');
INSERT INTO `game_event_creature` VALUES ('120027', '307');
INSERT INTO `game_event_creature` VALUES ('120046', '307');
INSERT INTO `game_event_creature` VALUES ('120033', '307');
INSERT INTO `game_event_creature` VALUES ('120026', '307');
INSERT INTO `game_event_creature` VALUES ('6309', '305');
INSERT INTO `game_event_creature` VALUES ('39350', '305');
INSERT INTO `game_event_creature` VALUES ('39381', '305');
INSERT INTO `game_event_creature` VALUES ('53232', '305');
INSERT INTO `game_event_creature` VALUES ('99706', '305');
INSERT INTO `game_event_creature` VALUES ('120029', '305');
INSERT INTO `game_event_creature` VALUES ('120031', '305');
INSERT INTO `game_event_creature` VALUES ('98716', '302');
INSERT INTO `game_event_creature` VALUES ('57893', '302');
INSERT INTO `game_event_creature` VALUES ('120211', '316');
INSERT INTO `game_event_creature` VALUES ('120212', '316');
INSERT INTO `game_event_creature` VALUES ('120213', '316');
INSERT INTO `game_event_creature` VALUES ('120214', '316');
INSERT INTO `game_event_creature` VALUES ('120215', '316');
INSERT INTO `game_event_creature` VALUES ('120216', '316');
INSERT INTO `game_event_creature` VALUES ('120217', '316');
INSERT INTO `game_event_creature` VALUES ('120218', '316');
INSERT INTO `game_event_creature` VALUES ('120219', '316');
INSERT INTO `game_event_creature` VALUES ('120220', '316');
INSERT INTO `game_event_creature` VALUES ('120221', '316');
INSERT INTO `game_event_creature` VALUES ('120222', '316');
INSERT INTO `game_event_creature` VALUES ('120223', '316');
INSERT INTO `game_event_creature` VALUES ('120224', '316');
INSERT INTO `game_event_creature` VALUES ('120225', '316');
INSERT INTO `game_event_creature` VALUES ('120226', '316');
INSERT INTO `game_event_creature` VALUES ('120227', '316');
INSERT INTO `game_event_creature` VALUES ('120228', '316');
INSERT INTO `game_event_creature` VALUES ('120229', '316');
INSERT INTO `game_event_creature` VALUES ('120230', '316');
INSERT INTO `game_event_creature` VALUES ('120231', '316');
INSERT INTO `game_event_creature` VALUES ('120232', '316');
INSERT INTO `game_event_creature` VALUES ('120233', '316');
INSERT INTO `game_event_creature` VALUES ('120234', '316');
INSERT INTO `game_event_creature` VALUES ('120235', '316');
INSERT INTO `game_event_creature` VALUES ('120115', '316');
INSERT INTO `game_event_creature` VALUES ('120116', '316');
INSERT INTO `game_event_creature` VALUES ('120117', '316');
INSERT INTO `game_event_creature` VALUES ('120118', '316');
INSERT INTO `game_event_creature` VALUES ('120119', '316');
INSERT INTO `game_event_creature` VALUES ('120127', '316');
INSERT INTO `game_event_creature` VALUES ('120129', '316');
INSERT INTO `game_event_creature` VALUES ('120131', '316');
INSERT INTO `game_event_creature` VALUES ('120134', '316');
INSERT INTO `game_event_creature` VALUES ('120135', '316');
INSERT INTO `game_event_creature` VALUES ('120199', '316');
INSERT INTO `game_event_creature` VALUES ('120208', '316');
INSERT INTO `game_event_creature` VALUES ('120015', '305');
INSERT INTO `game_event_creature` VALUES ('120016', '305');
INSERT INTO `game_event_creature` VALUES ('120017', '305');
INSERT INTO `game_event_creature` VALUES ('120018', '305');
INSERT INTO `game_event_creature` VALUES ('120019', '305');
INSERT INTO `game_event_creature` VALUES ('120020', '305');
INSERT INTO `game_event_creature` VALUES ('120021', '305');
INSERT INTO `game_event_creature` VALUES ('120022', '305');
INSERT INTO `game_event_creature` VALUES ('122691', '302');
INSERT INTO `game_event_creature` VALUES ('122692', '302');
INSERT INTO `game_event_creature` VALUES ('122693', '302');
INSERT INTO `game_event_creature` VALUES ('122694', '302');
INSERT INTO `game_event_creature` VALUES ('122695', '302');
INSERT INTO `game_event_creature` VALUES ('122696', '302');
INSERT INTO `game_event_creature` VALUES ('122697', '302');
INSERT INTO `game_event_creature` VALUES ('122698', '302');
INSERT INTO `game_event_creature` VALUES ('122699', '302');
INSERT INTO `game_event_creature` VALUES ('122700', '302');
INSERT INTO `game_event_creature` VALUES ('122701', '302');
INSERT INTO `game_event_creature` VALUES ('122702', '302');
INSERT INTO `game_event_creature` VALUES ('122703', '302');
INSERT INTO `game_event_creature` VALUES ('122704', '302');
INSERT INTO `game_event_creature` VALUES ('122705', '302');
INSERT INTO `game_event_creature` VALUES ('122706', '302');
INSERT INTO `game_event_creature` VALUES ('122707', '302');
INSERT INTO `game_event_creature` VALUES ('122708', '302');
INSERT INTO `game_event_creature` VALUES ('122709', '302');
INSERT INTO `game_event_creature` VALUES ('122710', '302');
INSERT INTO `game_event_creature` VALUES ('122711', '302');
INSERT INTO `game_event_creature` VALUES ('122712', '302');
INSERT INTO `game_event_creature` VALUES ('122713', '302');
INSERT INTO `game_event_creature` VALUES ('122714', '302');
INSERT INTO `game_event_creature` VALUES ('122715', '302');
INSERT INTO `game_event_creature` VALUES ('122716', '302');
INSERT INTO `game_event_creature` VALUES ('122717', '302');
INSERT INTO `game_event_creature` VALUES ('122718', '302');
INSERT INTO `game_event_creature` VALUES ('122719', '302');
INSERT INTO `game_event_creature` VALUES ('122720', '302');
INSERT INTO `game_event_creature` VALUES ('122721', '302');
INSERT INTO `game_event_creature` VALUES ('122722', '302');
INSERT INTO `game_event_creature` VALUES ('122723', '302');
INSERT INTO `game_event_creature` VALUES ('122724', '302');
INSERT INTO `game_event_creature` VALUES ('122725', '302');
INSERT INTO `game_event_creature` VALUES ('122726', '302');
INSERT INTO `game_event_creature` VALUES ('122727', '302');
INSERT INTO `game_event_creature` VALUES ('122728', '302');
INSERT INTO `game_event_creature` VALUES ('122729', '302');
INSERT INTO `game_event_creature` VALUES ('122730', '302');
INSERT INTO `game_event_creature` VALUES ('122731', '302');
INSERT INTO `game_event_creature` VALUES ('122732', '302');
INSERT INTO `game_event_creature` VALUES ('122733', '302');
INSERT INTO `game_event_creature` VALUES ('122734', '302');
INSERT INTO `game_event_creature` VALUES ('122735', '302');
INSERT INTO `game_event_creature` VALUES ('122736', '302');
INSERT INTO `game_event_creature` VALUES ('122737', '302');
INSERT INTO `game_event_creature` VALUES ('122738', '302');
INSERT INTO `game_event_creature` VALUES ('122739', '302');
INSERT INTO `game_event_creature` VALUES ('122740', '302');
INSERT INTO `game_event_creature` VALUES ('122741', '302');
INSERT INTO `game_event_creature` VALUES ('122742', '302');
INSERT INTO `game_event_creature` VALUES ('122743', '302');
INSERT INTO `game_event_creature` VALUES ('122744', '302');
INSERT INTO `game_event_creature` VALUES ('122745', '302');
INSERT INTO `game_event_creature` VALUES ('122746', '302');
INSERT INTO `game_event_creature` VALUES ('122747', '302');
INSERT INTO `game_event_creature` VALUES ('122748', '302');
INSERT INTO `game_event_creature` VALUES ('122749', '302');
INSERT INTO `game_event_creature` VALUES ('122750', '302');
INSERT INTO `game_event_creature` VALUES ('122751', '302');
INSERT INTO `game_event_creature` VALUES ('122752', '302');
INSERT INTO `game_event_creature` VALUES ('122753', '302');
INSERT INTO `game_event_creature` VALUES ('122754', '302');
INSERT INTO `game_event_creature` VALUES ('122755', '302');
INSERT INTO `game_event_creature` VALUES ('122756', '302');
INSERT INTO `game_event_creature` VALUES ('122757', '302');
INSERT INTO `game_event_creature` VALUES ('122758', '302');
INSERT INTO `game_event_creature` VALUES ('122759', '302');
INSERT INTO `game_event_creature` VALUES ('122760', '302');
INSERT INTO `game_event_creature` VALUES ('122761', '302');
INSERT INTO `game_event_creature` VALUES ('122762', '302');
INSERT INTO `game_event_creature` VALUES ('122763', '302');
INSERT INTO `game_event_creature` VALUES ('122764', '302');
INSERT INTO `game_event_creature` VALUES ('122765', '302');
INSERT INTO `game_event_creature` VALUES ('122766', '302');
INSERT INTO `game_event_creature` VALUES ('122767', '302');
INSERT INTO `game_event_creature` VALUES ('122768', '302');
INSERT INTO `game_event_creature` VALUES ('122769', '302');
INSERT INTO `game_event_creature` VALUES ('122770', '302');
INSERT INTO `game_event_creature` VALUES ('122771', '302');
INSERT INTO `game_event_creature` VALUES ('122772', '302');
INSERT INTO `game_event_creature` VALUES ('122773', '302');
INSERT INTO `game_event_creature` VALUES ('122774', '302');
INSERT INTO `game_event_creature` VALUES ('122775', '302');
INSERT INTO `game_event_creature` VALUES ('122776', '302');
INSERT INTO `game_event_creature` VALUES ('122777', '302');
INSERT INTO `game_event_creature` VALUES ('122778', '302');
INSERT INTO `game_event_creature` VALUES ('122779', '302');
INSERT INTO `game_event_creature` VALUES ('122780', '302');
INSERT INTO `game_event_creature` VALUES ('122781', '302');
INSERT INTO `game_event_creature` VALUES ('122782', '302');
INSERT INTO `game_event_creature` VALUES ('122783', '302');
INSERT INTO `game_event_creature` VALUES ('122784', '302');
INSERT INTO `game_event_creature` VALUES ('122785', '302');
INSERT INTO `game_event_creature` VALUES ('122786', '302');
INSERT INTO `game_event_creature` VALUES ('122787', '302');
INSERT INTO `game_event_creature` VALUES ('122788', '302');
INSERT INTO `game_event_creature` VALUES ('122789', '302');
INSERT INTO `game_event_creature` VALUES ('122790', '302');
INSERT INTO `game_event_creature` VALUES ('122791', '302');
INSERT INTO `game_event_creature` VALUES ('122792', '302');
INSERT INTO `game_event_creature` VALUES ('122793', '302');
INSERT INTO `game_event_creature` VALUES ('122794', '302');
INSERT INTO `game_event_creature` VALUES ('122795', '302');
INSERT INTO `game_event_creature` VALUES ('122796', '302');
INSERT INTO `game_event_creature` VALUES ('122797', '302');
INSERT INTO `game_event_creature` VALUES ('122798', '302');
INSERT INTO `game_event_creature` VALUES ('122799', '302');
INSERT INTO `game_event_creature` VALUES ('122800', '302');
INSERT INTO `game_event_creature` VALUES ('122801', '302');
INSERT INTO `game_event_creature` VALUES ('122802', '302');
INSERT INTO `game_event_creature` VALUES ('122803', '302');
INSERT INTO `game_event_creature` VALUES ('122804', '302');
INSERT INTO `game_event_creature` VALUES ('122805', '302');
INSERT INTO `game_event_creature` VALUES ('122806', '302');
INSERT INTO `game_event_creature` VALUES ('122807', '302');
INSERT INTO `game_event_creature` VALUES ('122808', '302');
INSERT INTO `game_event_creature` VALUES ('122809', '302');
INSERT INTO `game_event_creature` VALUES ('122810', '302');
INSERT INTO `game_event_creature` VALUES ('122811', '302');
INSERT INTO `game_event_creature` VALUES ('122812', '302');
INSERT INTO `game_event_creature` VALUES ('122813', '302');
INSERT INTO `game_event_creature` VALUES ('122814', '302');
INSERT INTO `game_event_creature` VALUES ('122815', '302');
INSERT INTO `game_event_creature` VALUES ('122816', '302');
INSERT INTO `game_event_creature` VALUES ('122817', '302');
INSERT INTO `game_event_creature` VALUES ('122818', '302');
INSERT INTO `game_event_creature` VALUES ('122819', '302');
INSERT INTO `game_event_creature` VALUES ('122820', '302');
INSERT INTO `game_event_creature` VALUES ('122821', '302');
INSERT INTO `game_event_creature` VALUES ('122822', '302');
INSERT INTO `game_event_creature` VALUES ('122823', '302');
INSERT INTO `game_event_creature` VALUES ('122824', '302');
INSERT INTO `game_event_creature` VALUES ('122827', '316');
INSERT INTO `game_event_creature` VALUES ('122826', '316');
INSERT INTO `game_event_creature` VALUES ('122825', '303');
INSERT INTO `game_event_creature` VALUES ('122828', '303');
INSERT INTO `game_event_creature` VALUES ('122829', '303');
INSERT INTO `game_event_creature` VALUES ('122830', '303');
INSERT INTO `game_event_creature` VALUES ('15185', '306');
INSERT INTO `game_event_creature` VALUES ('120071', '306');
INSERT INTO `game_event_creature` VALUES ('120072', '306');
INSERT INTO `game_event_creature` VALUES ('120073', '306');
INSERT INTO `game_event_creature` VALUES ('120074', '306');
INSERT INTO `game_event_creature` VALUES ('120075', '306');
INSERT INTO `game_event_creature` VALUES ('120076', '306');
INSERT INTO `game_event_creature` VALUES ('120077', '306');
INSERT INTO `game_event_creature` VALUES ('120078', '306');
INSERT INTO `game_event_creature` VALUES ('120079', '306');
INSERT INTO `game_event_creature` VALUES ('120080', '306');
INSERT INTO `game_event_creature` VALUES ('120081', '306');
INSERT INTO `game_event_creature` VALUES ('120082', '306');
INSERT INTO `game_event_creature` VALUES ('125073', '5000');
INSERT INTO `game_event_creature` VALUES ('125072', '5000');
INSERT INTO `game_event_creature` VALUES ('125071', '5000');
INSERT INTO `game_event_creature` VALUES ('125070', '5000');
INSERT INTO `game_event_creature` VALUES ('125069', '5000');
INSERT INTO `game_event_creature` VALUES ('125068', '5000');
INSERT INTO `game_event_creature` VALUES ('125067', '5000');
INSERT INTO `game_event_creature` VALUES ('125066', '5000');
INSERT INTO `game_event_creature` VALUES ('125065', '5000');
INSERT INTO `game_event_creature` VALUES ('125064', '5000');
INSERT INTO `game_event_creature` VALUES ('125063', '5000');
INSERT INTO `game_event_creature` VALUES ('125062', '5000');
INSERT INTO `game_event_creature` VALUES ('125061', '5000');
INSERT INTO `game_event_creature` VALUES ('125060', '5000');
INSERT INTO `game_event_creature` VALUES ('125059', '5000');
INSERT INTO `game_event_creature` VALUES ('125058', '5000');
INSERT INTO `game_event_creature` VALUES ('125057', '5000');
INSERT INTO `game_event_creature` VALUES ('125056', '5000');
INSERT INTO `game_event_creature` VALUES ('125055', '5000');
INSERT INTO `game_event_creature` VALUES ('125054', '5000');
INSERT INTO `game_event_creature` VALUES ('125053', '5000');
INSERT INTO `game_event_creature` VALUES ('125052', '5000');
INSERT INTO `game_event_creature` VALUES ('125051', '5000');
INSERT INTO `game_event_creature` VALUES ('125050', '5000');
INSERT INTO `game_event_creature` VALUES ('125049', '5000');
INSERT INTO `game_event_creature` VALUES ('125048', '5000');
INSERT INTO `game_event_creature` VALUES ('125047', '5000');
INSERT INTO `game_event_creature` VALUES ('125046', '5000');
INSERT INTO `game_event_creature` VALUES ('125045', '5000');
INSERT INTO `game_event_creature` VALUES ('125044', '5000');
INSERT INTO `game_event_creature` VALUES ('125043', '5000');
INSERT INTO `game_event_creature` VALUES ('125042', '5000');
INSERT INTO `game_event_creature` VALUES ('125041', '5000');
INSERT INTO `game_event_creature` VALUES ('125040', '5000');
INSERT INTO `game_event_creature` VALUES ('125039', '5000');
INSERT INTO `game_event_creature` VALUES ('125038', '5000');
INSERT INTO `game_event_creature` VALUES ('125037', '5000');
INSERT INTO `game_event_creature` VALUES ('125036', '5000');
INSERT INTO `game_event_creature` VALUES ('125035', '5000');
INSERT INTO `game_event_creature` VALUES ('125034', '5000');
INSERT INTO `game_event_creature` VALUES ('125033', '5000');
INSERT INTO `game_event_creature` VALUES ('125032', '5000');
INSERT INTO `game_event_creature` VALUES ('125031', '5000');
INSERT INTO `game_event_creature` VALUES ('125030', '5000');
INSERT INTO `game_event_creature` VALUES ('125029', '5000');
INSERT INTO `game_event_creature` VALUES ('125028', '5000');
INSERT INTO `game_event_creature` VALUES ('125027', '5000');
INSERT INTO `game_event_creature` VALUES ('125026', '5000');
INSERT INTO `game_event_creature` VALUES ('125025', '5000');
INSERT INTO `game_event_creature` VALUES ('125024', '5000');
INSERT INTO `game_event_creature` VALUES ('125023', '5000');
INSERT INTO `game_event_creature` VALUES ('125022', '5000');
INSERT INTO `game_event_creature` VALUES ('125021', '5000');
INSERT INTO `game_event_creature` VALUES ('125020', '5000');
INSERT INTO `game_event_creature` VALUES ('125019', '5000');
INSERT INTO `game_event_creature` VALUES ('125018', '5000');
INSERT INTO `game_event_creature` VALUES ('125017', '5000');
INSERT INTO `game_event_creature` VALUES ('125106', '300');
INSERT INTO `game_event_creature` VALUES ('125169', '1200');
INSERT INTO `game_event_creature` VALUES ('125168', '1200');
INSERT INTO `game_event_creature` VALUES ('125149', '1200');
INSERT INTO `game_event_creature` VALUES ('125148', '1200');
INSERT INTO `game_event_creature` VALUES ('125172', '1200');
INSERT INTO `game_event_creature` VALUES ('125152', '1200');
INSERT INTO `game_event_creature` VALUES ('125153', '1200');
INSERT INTO `game_event_creature` VALUES ('125154', '1200');
INSERT INTO `game_event_creature` VALUES ('125151', '1200');
INSERT INTO `game_event_creature` VALUES ('125150', '1200');
INSERT INTO `game_event_creature` VALUES ('125166', '1200');
INSERT INTO `game_event_creature` VALUES ('125167', '1200');
INSERT INTO `game_event_creature` VALUES ('125165', '1200');
INSERT INTO `game_event_creature` VALUES ('125161', '1200');
INSERT INTO `game_event_creature` VALUES ('125162', '1200');
INSERT INTO `game_event_creature` VALUES ('125159', '1200');
INSERT INTO `game_event_creature` VALUES ('125155', '1200');
INSERT INTO `game_event_creature` VALUES ('125171', '1200');
INSERT INTO `game_event_creature` VALUES ('125160', '1200');
INSERT INTO `game_event_creature` VALUES ('125156', '1200');
INSERT INTO `game_event_creature` VALUES ('125157', '1200');
INSERT INTO `game_event_creature` VALUES ('125158', '1200');
INSERT INTO `game_event_creature` VALUES ('125170', '1200');
INSERT INTO `game_event_creature` VALUES ('125174', '1000');
INSERT INTO `game_event_creature` VALUES ('125173', '1000');
INSERT INTO `game_event_creature` VALUES ('125175', '1100');
INSERT INTO `game_event_creature` VALUES ('99889', '302');
INSERT INTO `game_event_creature` VALUES ('99890', '302');
INSERT INTO `game_event_creature` VALUES ('99891', '302');
INSERT INTO `game_event_creature` VALUES ('99892', '302');
INSERT INTO `game_event_creature` VALUES ('99893', '302');
INSERT INTO `game_event_creature` VALUES ('99894', '302');
INSERT INTO `game_event_creature` VALUES ('99895', '302');
INSERT INTO `game_event_creature` VALUES ('99896', '302');
INSERT INTO `game_event_creature` VALUES ('120083', '306');
INSERT INTO `game_event_creature` VALUES ('99589', '316');
INSERT INTO `game_event_creature` VALUES ('120086', '316');
INSERT INTO `game_event_creature` VALUES ('120096', '306');
INSERT INTO `game_event_creature` VALUES ('120095', '306');
INSERT INTO `game_event_creature` VALUES ('120094', '306');
INSERT INTO `game_event_creature` VALUES ('120093', '306');
INSERT INTO `game_event_creature` VALUES ('120092', '306');
INSERT INTO `game_event_creature` VALUES ('120091', '306');
INSERT INTO `game_event_creature` VALUES ('120090', '306');
INSERT INTO `game_event_creature` VALUES ('120089', '306');
INSERT INTO `game_event_creature` VALUES ('120088', '306');
INSERT INTO `game_event_creature` VALUES ('120087', '306');
INSERT INTO `game_event_creature` VALUES ('120085', '306');
INSERT INTO `game_event_creature` VALUES ('120084', '306');
INSERT INTO `game_event_creature` VALUES ('120166', '316');
INSERT INTO `game_event_creature` VALUES ('120165', '316');
INSERT INTO `game_event_creature` VALUES ('120160', '316');
INSERT INTO `game_event_creature` VALUES ('120159', '316');
INSERT INTO `game_event_creature` VALUES ('9297', '5000');
INSERT INTO `game_event_creature` VALUES ('90215', '1000');
INSERT INTO `game_event_creature` VALUES ('121635', '1000');
INSERT INTO `game_event_creature` VALUES ('121627', '1000');
INSERT INTO `game_event_creature` VALUES ('121636', '1000');
INSERT INTO `game_event_creature` VALUES ('121637', '1000');
INSERT INTO `game_event_creature` VALUES ('125163', '1200');
INSERT INTO `game_event_creature` VALUES ('125566', '303');
INSERT INTO `game_event_creature` VALUES ('125567', '303');
INSERT INTO `game_event_creature` VALUES ('125568', '303');
INSERT INTO `game_event_creature` VALUES ('125569', '303');
INSERT INTO `game_event_creature` VALUES ('125570', '303');
INSERT INTO `game_event_creature` VALUES ('125571', '303');
INSERT INTO `game_event_creature` VALUES ('125572', '303');
INSERT INTO `game_event_creature` VALUES ('125573', '303');
INSERT INTO `game_event_creature` VALUES ('125574', '303');
INSERT INTO `game_event_creature` VALUES ('120024', '312');
INSERT INTO `game_event_creature` VALUES ('120013', '312');
INSERT INTO `game_event_creature` VALUES ('83996', '311');
INSERT INTO `game_event_creature` VALUES ('52746', '315');
INSERT INTO `game_event_creature` VALUES ('57824', '315');
INSERT INTO `game_event_creature` VALUES ('61919', '315');
INSERT INTO `game_event_creature` VALUES ('61920', '315');
INSERT INTO `game_event_creature` VALUES ('74658', '315');
INSERT INTO `game_event_creature` VALUES ('74660', '315');
INSERT INTO `game_event_creature` VALUES ('83997', '315');
INSERT INTO `game_event_creature` VALUES ('83998', '315');
INSERT INTO `game_event_creature` VALUES ('83999', '315');
INSERT INTO `game_event_creature` VALUES ('95383', '315');
INSERT INTO `game_event_creature` VALUES ('95384', '315');
INSERT INTO `game_event_creature` VALUES ('97929', '315');
INSERT INTO `game_event_creature` VALUES ('120028', '315');
INSERT INTO `game_event_creature` VALUES ('120036', '315');
INSERT INTO `game_event_creature` VALUES ('120037', '315');
INSERT INTO `game_event_creature` VALUES ('120038', '315');
INSERT INTO `game_event_creature` VALUES ('120060', '312');
INSERT INTO `game_event_creature` VALUES ('120012', '312');
INSERT INTO `game_event_creature` VALUES ('120061', '312');
INSERT INTO `game_event_creature` VALUES ('120062', '312');
INSERT INTO `game_event_creature` VALUES ('120064', '312');
INSERT INTO `game_event_creature` VALUES ('120063', '312');
INSERT INTO `game_event_creature` VALUES ('95382', '312');
INSERT INTO `game_event_creature` VALUES ('83995', '312');
INSERT INTO `game_event_creature` VALUES ('83993', '312');
INSERT INTO `game_event_creature` VALUES ('83994', '312');
INSERT INTO `game_event_creature` VALUES ('95380', '312');
INSERT INTO `game_event_creature` VALUES ('57815', '312');
INSERT INTO `game_event_creature` VALUES ('57817', '312');
INSERT INTO `game_event_creature` VALUES ('52738', '312');
INSERT INTO `game_event_creature` VALUES ('120023', '312');
INSERT INTO `game_event_creature` VALUES ('120001', '312');
INSERT INTO `game_event_creature` VALUES ('52739', '312');
INSERT INTO `game_event_creature` VALUES ('100650', '317');
INSERT INTO `game_event_creature` VALUES ('52748', '315');
INSERT INTO `game_event_creature` VALUES ('52749', '315');
INSERT INTO `game_event_creature` VALUES ('52736', '312');
INSERT INTO `game_event_creature` VALUES ('52737', '312');
INSERT INTO `game_event_creature` VALUES ('74890', '311');
INSERT INTO `game_event_creature` VALUES ('74885', '311');
INSERT INTO `game_event_creature` VALUES ('74886', '311');
INSERT INTO `game_event_creature` VALUES ('52747', '311');
INSERT INTO `game_event_creature` VALUES ('120237', '311');
INSERT INTO `game_event_creature` VALUES ('120236', '311');
INSERT INTO `game_event_creature` VALUES ('74891', '311');
INSERT INTO `game_event_creature` VALUES ('131000', '34');
INSERT INTO `game_event_creature` VALUES ('131001', '34');
INSERT INTO `game_event_creature` VALUES ('131002', '34');
INSERT INTO `game_event_creature` VALUES ('131003', '34');
INSERT INTO `game_event_creature` VALUES ('131004', '34');
INSERT INTO `game_event_creature` VALUES ('131005', '34');
INSERT INTO `game_event_creature` VALUES ('131006', '34');
INSERT INTO `game_event_creature` VALUES ('131007', '34');
INSERT INTO `game_event_creature` VALUES ('131008', '34');
INSERT INTO `game_event_creature` VALUES ('131009', '34');
INSERT INTO `game_event_creature` VALUES ('131101', '303');
INSERT INTO `game_event_creature` VALUES ('87606', '300');
INSERT INTO `game_event_creature` VALUES ('87607', '300');
INSERT INTO `game_event_creature` VALUES ('84104', '-1200');
INSERT INTO `game_event_creature` VALUES ('82941', '-1200');
INSERT INTO `game_event_creature` VALUES ('79436', '-1200');
INSERT INTO `game_event_creature` VALUES ('84087', '-1200');
INSERT INTO `game_event_creature` VALUES ('79311', '-1200');
INSERT INTO `game_event_creature` VALUES ('79435', '-1200');
INSERT INTO `game_event_creature` VALUES ('84103', '-1200');
INSERT INTO `game_event_creature` VALUES ('131112', '1000');
INSERT INTO `game_event_creature` VALUES ('131113', '1000');
INSERT INTO `game_event_creature` VALUES ('15609', '5000');
INSERT INTO `game_event_creature` VALUES ('85046', '-1000');
INSERT INTO `game_event_creature` VALUES ('131190', '1200');
INSERT INTO `game_event_creature` VALUES ('131191', '1000');
INSERT INTO `game_event_creature` VALUES ('120760', '1100');
INSERT INTO `game_event_creature` VALUES ('120761', '1100');
INSERT INTO `game_event_creature` VALUES ('132958', '316');
INSERT INTO `game_event_creature` VALUES ('124774', '400');
INSERT INTO `game_event_creature` VALUES ('120138', '316');
INSERT INTO `game_event_creature` VALUES ('92029', '1402');
INSERT INTO `game_event_creature` VALUES ('73034', '1402');
INSERT INTO `game_event_creature` VALUES ('65462', '1402');
INSERT INTO `game_event_creature` VALUES ('100632', '1402');
INSERT INTO `game_event_creature` VALUES ('123338', '1402');
INSERT INTO `game_event_creature` VALUES ('127572', '1402');
INSERT INTO `game_event_creature` VALUES ('100630', '1402');
INSERT INTO `game_event_creature` VALUES ('65979', '1402');
INSERT INTO `game_event_creature` VALUES ('65758', '1402');
INSERT INTO `game_event_creature` VALUES ('66450', '1402');
INSERT INTO `game_event_creature` VALUES ('66698', '1402');
INSERT INTO `game_event_creature` VALUES ('134262', '1402');
INSERT INTO `game_event_creature` VALUES ('127226', '1402');
