/*
Navicat MySQL Data Transfer

Source Server         : Hometown
Source Server Version : 50523
Source Host           : localhost:3306
Source Database       : mangos

Target Server Type    : MYSQL
Target Server Version : 50523
File Encoding         : 65001

Date: 2013-09-26 22:12:38
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `game_event_creature_data`
-- ----------------------------
DROP TABLE IF EXISTS `game_event_creature_data`;
CREATE TABLE `game_event_creature_data` (
  `guid` int(10) unsigned NOT NULL DEFAULT '0',
  `entry_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `modelid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `equipment_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `spell_start` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `spell_end` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `event` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`guid`,`event`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of game_event_creature_data
-- ----------------------------
INSERT INTO `game_event_creature_data` VALUES ('52680', '0', '0', '504', '0', '0', '27');
INSERT INTO `game_event_creature_data` VALUES ('52689', '0', '0', '504', '0', '0', '27');
INSERT INTO `game_event_creature_data` VALUES ('53378', '0', '0', '504', '0', '0', '27');
INSERT INTO `game_event_creature_data` VALUES ('53379', '0', '0', '504', '0', '0', '27');
INSERT INTO `game_event_creature_data` VALUES ('53381', '0', '0', '504', '0', '0', '27');
INSERT INTO `game_event_creature_data` VALUES ('53382', '0', '0', '504', '0', '0', '27');
INSERT INTO `game_event_creature_data` VALUES ('62570', '0', '0', '504', '0', '0', '27');
INSERT INTO `game_event_creature_data` VALUES ('62577', '0', '0', '504', '0', '0', '27');
INSERT INTO `game_event_creature_data` VALUES ('85048', '0', '0', '504', '0', '0', '27');
INSERT INTO `game_event_creature_data` VALUES ('85050', '0', '0', '504', '0', '0', '27');
