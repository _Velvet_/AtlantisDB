/*
Navicat MySQL Data Transfer

Source Server         : Hometown
Source Server Version : 50523
Source Host           : localhost:3306
Source Database       : mangos

Target Server Type    : MYSQL
Target Server Version : 50523
File Encoding         : 65001

Date: 2013-09-26 22:12:52
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `game_event_quest`
-- ----------------------------
DROP TABLE IF EXISTS `game_event_quest`;
CREATE TABLE `game_event_quest` (
  `quest` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT 'entry from quest_template',
  `event` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'entry from game_event',
  PRIMARY KEY (`quest`,`event`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Game event system';

-- ----------------------------
-- Records of game_event_quest
-- ----------------------------
INSERT INTO `game_event_quest` VALUES ('1657', '307');
INSERT INTO `game_event_quest` VALUES ('1658', '307');
INSERT INTO `game_event_quest` VALUES ('8149', '306');
INSERT INTO `game_event_quest` VALUES ('8150', '306');
INSERT INTO `game_event_quest` VALUES ('8311', '307');
INSERT INTO `game_event_quest` VALUES ('8312', '307');
INSERT INTO `game_event_quest` VALUES ('8322', '307');
INSERT INTO `game_event_quest` VALUES ('8353', '307');
INSERT INTO `game_event_quest` VALUES ('8354', '307');
INSERT INTO `game_event_quest` VALUES ('8355', '307');
INSERT INTO `game_event_quest` VALUES ('8356', '307');
INSERT INTO `game_event_quest` VALUES ('8357', '307');
INSERT INTO `game_event_quest` VALUES ('8358', '307');
INSERT INTO `game_event_quest` VALUES ('8359', '307');
INSERT INTO `game_event_quest` VALUES ('8360', '307');
INSERT INTO `game_event_quest` VALUES ('8373', '307');
INSERT INTO `game_event_quest` VALUES ('8860', '301');
INSERT INTO `game_event_quest` VALUES ('8861', '301');
INSERT INTO `game_event_quest` VALUES ('8897', '303');
INSERT INTO `game_event_quest` VALUES ('8898', '303');
INSERT INTO `game_event_quest` VALUES ('8899', '303');
INSERT INTO `game_event_quest` VALUES ('8900', '303');
INSERT INTO `game_event_quest` VALUES ('8901', '303');
INSERT INTO `game_event_quest` VALUES ('8902', '303');
INSERT INTO `game_event_quest` VALUES ('8903', '303');
INSERT INTO `game_event_quest` VALUES ('8904', '303');
INSERT INTO `game_event_quest` VALUES ('8979', '303');
INSERT INTO `game_event_quest` VALUES ('8980', '303');
INSERT INTO `game_event_quest` VALUES ('8982', '303');
INSERT INTO `game_event_quest` VALUES ('8983', '303');
INSERT INTO `game_event_quest` VALUES ('8984', '303');
INSERT INTO `game_event_quest` VALUES ('9024', '303');
INSERT INTO `game_event_quest` VALUES ('9025', '303');
INSERT INTO `game_event_quest` VALUES ('9027', '303');
INSERT INTO `game_event_quest` VALUES ('9028', '303');
INSERT INTO `game_event_quest` VALUES ('11117', '316');
INSERT INTO `game_event_quest` VALUES ('11118', '316');
INSERT INTO `game_event_quest` VALUES ('11120', '316');
INSERT INTO `game_event_quest` VALUES ('11122', '316');
INSERT INTO `game_event_quest` VALUES ('11131', '307');
INSERT INTO `game_event_quest` VALUES ('11135', '307');
INSERT INTO `game_event_quest` VALUES ('11219', '307');
INSERT INTO `game_event_quest` VALUES ('11220', '307');
INSERT INTO `game_event_quest` VALUES ('11318', '316');
INSERT INTO `game_event_quest` VALUES ('11321', '316');
INSERT INTO `game_event_quest` VALUES ('11356', '307');
INSERT INTO `game_event_quest` VALUES ('11360', '307');
INSERT INTO `game_event_quest` VALUES ('11361', '307');
INSERT INTO `game_event_quest` VALUES ('11407', '316');
INSERT INTO `game_event_quest` VALUES ('11408', '316');
INSERT INTO `game_event_quest` VALUES ('11409', '316');
INSERT INTO `game_event_quest` VALUES ('11412', '316');
INSERT INTO `game_event_quest` VALUES ('11431', '316');
INSERT INTO `game_event_quest` VALUES ('11439', '307');
INSERT INTO `game_event_quest` VALUES ('11440', '307');
INSERT INTO `game_event_quest` VALUES ('11441', '316');
INSERT INTO `game_event_quest` VALUES ('11446', '316');
INSERT INTO `game_event_quest` VALUES ('11449', '307');
INSERT INTO `game_event_quest` VALUES ('11450', '307');
