/*
Navicat MySQL Data Transfer

Source Server         : Hometown
Source Server Version : 50523
Source Host           : localhost:3306
Source Database       : mangos

Target Server Type    : MYSQL
Target Server Version : 50523
File Encoding         : 65001

Date: 2013-09-26 22:13:05
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `game_weather`
-- ----------------------------
DROP TABLE IF EXISTS `game_weather`;
CREATE TABLE `game_weather` (
  `zone` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `spring_rain_chance` tinyint(3) unsigned NOT NULL DEFAULT '25',
  `spring_snow_chance` tinyint(3) unsigned NOT NULL DEFAULT '25',
  `spring_storm_chance` tinyint(3) unsigned NOT NULL DEFAULT '25',
  `summer_rain_chance` tinyint(3) unsigned NOT NULL DEFAULT '25',
  `summer_snow_chance` tinyint(3) unsigned NOT NULL DEFAULT '25',
  `summer_storm_chance` tinyint(3) unsigned NOT NULL DEFAULT '25',
  `fall_rain_chance` tinyint(3) unsigned NOT NULL DEFAULT '25',
  `fall_snow_chance` tinyint(3) unsigned NOT NULL DEFAULT '25',
  `fall_storm_chance` tinyint(3) unsigned NOT NULL DEFAULT '25',
  `winter_rain_chance` tinyint(3) unsigned NOT NULL DEFAULT '25',
  `winter_snow_chance` tinyint(3) unsigned NOT NULL DEFAULT '25',
  `winter_storm_chance` tinyint(3) unsigned NOT NULL DEFAULT '25',
  PRIMARY KEY (`zone`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='Weather System';

-- ----------------------------
-- Records of game_weather
-- ----------------------------
INSERT INTO `game_weather` VALUES ('3', '0', '0', '30', '0', '0', '20', '0', '0', '20', '0', '0', '25');
INSERT INTO `game_weather` VALUES ('45', '33', '0', '0', '25', '0', '0', '33', '0', '0', '25', '0', '0');
INSERT INTO `game_weather` VALUES ('3358', '20', '0', '0', '20', '0', '0', '20', '0', '0', '20', '0', '0');
INSERT INTO `game_weather` VALUES ('148', '25', '0', '0', '20', '0', '0', '30', '0', '0', '20', '0', '0');
INSERT INTO `game_weather` VALUES ('85', '30', '0', '0', '25', '0', '0', '30', '0', '0', '10', '0', '0');
INSERT INTO `game_weather` VALUES ('405', '10', '0', '0', '5', '0', '0', '15', '0', '0', '5', '0', '0');
INSERT INTO `game_weather` VALUES ('41', '25', '0', '0', '15', '0', '0', '25', '0', '0', '20', '0', '0');
INSERT INTO `game_weather` VALUES ('15', '35', '0', '0', '30', '0', '0', '35', '0', '0', '20', '0', '0');
INSERT INTO `game_weather` VALUES ('10', '25', '0', '0', '25', '0', '0', '30', '0', '0', '25', '0', '0');
INSERT INTO `game_weather` VALUES ('139', '20', '0', '0', '25', '0', '0', '25', '0', '0', '20', '0', '0');
INSERT INTO `game_weather` VALUES ('28', '20', '0', '0', '25', '0', '0', '25', '0', '0', '20', '0', '0');
INSERT INTO `game_weather` VALUES ('2017', '15', '0', '0', '15', '0', '0', '15', '0', '0', '15', '0', '0');
INSERT INTO `game_weather` VALUES ('12', '30', '0', '0', '25', '0', '0', '30', '0', '0', '20', '0', '0');
INSERT INTO `game_weather` VALUES ('357', '25', '0', '0', '25', '0', '0', '25', '0', '0', '25', '0', '0');
INSERT INTO `game_weather` VALUES ('267', '25', '0', '0', '20', '0', '0', '25', '0', '0', '25', '0', '0');
INSERT INTO `game_weather` VALUES ('47', '20', '0', '0', '20', '0', '0', '25', '0', '0', '20', '0', '0');
INSERT INTO `game_weather` VALUES ('38', '25', '0', '0', '25', '0', '0', '25', '0', '0', '25', '0', '0');
INSERT INTO `game_weather` VALUES ('215', '25', '0', '0', '10', '0', '0', '30', '0', '0', '20', '0', '0');
INSERT INTO `game_weather` VALUES ('44', '25', '0', '0', '25', '0', '0', '25', '0', '0', '25', '0', '0');
INSERT INTO `game_weather` VALUES ('33', '30', '0', '0', '35', '0', '0', '35', '0', '0', '30', '0', '0');
INSERT INTO `game_weather` VALUES ('1977', '25', '0', '0', '15', '0', '0', '25', '0', '0', '15', '0', '0');
INSERT INTO `game_weather` VALUES ('141', '25', '0', '0', '15', '0', '0', '25', '0', '0', '20', '0', '0');
INSERT INTO `game_weather` VALUES ('796', '15', '0', '0', '20', '0', '0', '35', '0', '0', '35', '0', '0');
INSERT INTO `game_weather` VALUES ('490', '25', '0', '0', '20', '0', '0', '30', '0', '0', '15', '0', '0');
INSERT INTO `game_weather` VALUES ('11', '35', '0', '0', '25', '0', '0', '35', '0', '0', '25', '0', '0');
INSERT INTO `game_weather` VALUES ('36', '0', '30', '0', '0', '20', '0', '0', '25', '0', '0', '40', '0');
INSERT INTO `game_weather` VALUES ('1', '0', '25', '0', '0', '15', '0', '0', '25', '0', '0', '35', '0');
INSERT INTO `game_weather` VALUES ('618', '0', '25', '0', '0', '20', '0', '0', '30', '0', '0', '35', '0');
INSERT INTO `game_weather` VALUES ('2597', '0', '15', '0', '0', '15', '0', '0', '20', '0', '0', '25', '0');
INSERT INTO `game_weather` VALUES ('1377', '0', '0', '20', '0', '0', '35', '0', '0', '30', '0', '0', '25');
INSERT INTO `game_weather` VALUES ('3429', '0', '0', '20', '0', '0', '20', '0', '0', '20', '0', '0', '20');
INSERT INTO `game_weather` VALUES ('3428', '0', '0', '20', '0', '0', '20', '0', '0', '20', '0', '0', '20');
INSERT INTO `game_weather` VALUES ('440', '0', '0', '25', '0', '0', '25', '0', '0', '25', '0', '0', '25');
INSERT INTO `game_weather` VALUES ('3521', '20', '0', '0', '25', '0', '0', '30', '0', '0', '15', '0', '0');
INSERT INTO `game_weather` VALUES ('4080', '80', '0', '0', '80', '0', '0', '80', '0', '0', '80', '10', '0');
