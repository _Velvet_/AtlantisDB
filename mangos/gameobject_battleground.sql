/*
Navicat MySQL Data Transfer

Source Server         : Hometown
Source Server Version : 50523
Source Host           : localhost:3306
Source Database       : mangos

Target Server Type    : MYSQL
Target Server Version : 50523
File Encoding         : 65001

Date: 2013-09-26 22:13:19
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `gameobject_battleground`
-- ----------------------------
DROP TABLE IF EXISTS `gameobject_battleground`;
CREATE TABLE `gameobject_battleground` (
  `guid` int(10) unsigned NOT NULL COMMENT 'GameObject''s GUID',
  `event1` tinyint(3) unsigned NOT NULL COMMENT 'main event',
  `event2` tinyint(3) unsigned NOT NULL COMMENT 'sub event',
  PRIMARY KEY (`guid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='GameObject battleground indexing system';

-- ----------------------------
-- Records of gameobject_battleground
-- ----------------------------
INSERT INTO `gameobject_battleground` VALUES ('1503568', '254', '0');
INSERT INTO `gameobject_battleground` VALUES ('1503566', '254', '0');
INSERT INTO `gameobject_battleground` VALUES ('1503602', '0', '0');
INSERT INTO `gameobject_battleground` VALUES ('1503603', '0', '0');
INSERT INTO `gameobject_battleground` VALUES ('1503600', '0', '1');
INSERT INTO `gameobject_battleground` VALUES ('1503601', '0', '1');
INSERT INTO `gameobject_battleground` VALUES ('1503606', '0', '2');
INSERT INTO `gameobject_battleground` VALUES ('1503607', '0', '2');
INSERT INTO `gameobject_battleground` VALUES ('1503604', '0', '3');
INSERT INTO `gameobject_battleground` VALUES ('1503605', '0', '3');
INSERT INTO `gameobject_battleground` VALUES ('1503608', '1', '0');
INSERT INTO `gameobject_battleground` VALUES ('1503609', '1', '0');
INSERT INTO `gameobject_battleground` VALUES ('1503610', '1', '1');
INSERT INTO `gameobject_battleground` VALUES ('1503611', '1', '1');
INSERT INTO `gameobject_battleground` VALUES ('1503612', '1', '2');
INSERT INTO `gameobject_battleground` VALUES ('1503613', '1', '2');
INSERT INTO `gameobject_battleground` VALUES ('1503614', '1', '3');
INSERT INTO `gameobject_battleground` VALUES ('1503615', '1', '3');
INSERT INTO `gameobject_battleground` VALUES ('1503616', '2', '0');
INSERT INTO `gameobject_battleground` VALUES ('1503617', '2', '0');
INSERT INTO `gameobject_battleground` VALUES ('1503618', '2', '1');
INSERT INTO `gameobject_battleground` VALUES ('1503619', '2', '1');
INSERT INTO `gameobject_battleground` VALUES ('1503620', '2', '2');
INSERT INTO `gameobject_battleground` VALUES ('1503621', '2', '2');
INSERT INTO `gameobject_battleground` VALUES ('1503622', '2', '3');
INSERT INTO `gameobject_battleground` VALUES ('1503623', '2', '3');
INSERT INTO `gameobject_battleground` VALUES ('1503624', '3', '0');
INSERT INTO `gameobject_battleground` VALUES ('1503625', '3', '0');
INSERT INTO `gameobject_battleground` VALUES ('1503626', '3', '1');
INSERT INTO `gameobject_battleground` VALUES ('1503627', '3', '1');
INSERT INTO `gameobject_battleground` VALUES ('1503628', '3', '2');
INSERT INTO `gameobject_battleground` VALUES ('1503629', '3', '2');
INSERT INTO `gameobject_battleground` VALUES ('1503630', '3', '3');
INSERT INTO `gameobject_battleground` VALUES ('1503631', '3', '3');
INSERT INTO `gameobject_battleground` VALUES ('1503632', '3', '5');
INSERT INTO `gameobject_battleground` VALUES ('1503633', '4', '0');
INSERT INTO `gameobject_battleground` VALUES ('1503634', '4', '0');
INSERT INTO `gameobject_battleground` VALUES ('1503635', '4', '1');
INSERT INTO `gameobject_battleground` VALUES ('1503636', '4', '1');
INSERT INTO `gameobject_battleground` VALUES ('1503637', '4', '2');
INSERT INTO `gameobject_battleground` VALUES ('1503638', '4', '2');
INSERT INTO `gameobject_battleground` VALUES ('1503639', '4', '3');
INSERT INTO `gameobject_battleground` VALUES ('1503640', '4', '3');
INSERT INTO `gameobject_battleground` VALUES ('1503641', '5', '0');
INSERT INTO `gameobject_battleground` VALUES ('1503642', '5', '0');
INSERT INTO `gameobject_battleground` VALUES ('1503643', '5', '1');
INSERT INTO `gameobject_battleground` VALUES ('1503644', '5', '1');
INSERT INTO `gameobject_battleground` VALUES ('1503645', '5', '2');
INSERT INTO `gameobject_battleground` VALUES ('1503646', '5', '2');
INSERT INTO `gameobject_battleground` VALUES ('1503647', '5', '3');
INSERT INTO `gameobject_battleground` VALUES ('1503648', '5', '3');
INSERT INTO `gameobject_battleground` VALUES ('1503649', '6', '0');
INSERT INTO `gameobject_battleground` VALUES ('1503650', '6', '0');
INSERT INTO `gameobject_battleground` VALUES ('1503651', '6', '1');
INSERT INTO `gameobject_battleground` VALUES ('1503652', '6', '1');
INSERT INTO `gameobject_battleground` VALUES ('1503653', '6', '2');
INSERT INTO `gameobject_battleground` VALUES ('1503654', '6', '2');
INSERT INTO `gameobject_battleground` VALUES ('1503655', '6', '3');
INSERT INTO `gameobject_battleground` VALUES ('1503656', '6', '3');
INSERT INTO `gameobject_battleground` VALUES ('1503671', '7', '1');
INSERT INTO `gameobject_battleground` VALUES ('1503672', '7', '2');
INSERT INTO `gameobject_battleground` VALUES ('1503673', '7', '1');
INSERT INTO `gameobject_battleground` VALUES ('1503674', '7', '2');
INSERT INTO `gameobject_battleground` VALUES ('1503675', '8', '1');
INSERT INTO `gameobject_battleground` VALUES ('1503676', '8', '2');
INSERT INTO `gameobject_battleground` VALUES ('1503677', '8', '1');
INSERT INTO `gameobject_battleground` VALUES ('1503678', '8', '2');
INSERT INTO `gameobject_battleground` VALUES ('1503679', '9', '1');
INSERT INTO `gameobject_battleground` VALUES ('1503680', '9', '2');
INSERT INTO `gameobject_battleground` VALUES ('1503681', '9', '1');
INSERT INTO `gameobject_battleground` VALUES ('1503682', '9', '2');
INSERT INTO `gameobject_battleground` VALUES ('1503683', '10', '1');
INSERT INTO `gameobject_battleground` VALUES ('1503684', '10', '2');
INSERT INTO `gameobject_battleground` VALUES ('1503685', '10', '1');
INSERT INTO `gameobject_battleground` VALUES ('1503686', '10', '2');
INSERT INTO `gameobject_battleground` VALUES ('1503687', '11', '3');
INSERT INTO `gameobject_battleground` VALUES ('1503688', '11', '0');
INSERT INTO `gameobject_battleground` VALUES ('1503689', '11', '3');
INSERT INTO `gameobject_battleground` VALUES ('1503690', '11', '0');
INSERT INTO `gameobject_battleground` VALUES ('1503691', '12', '3');
INSERT INTO `gameobject_battleground` VALUES ('1503692', '12', '0');
INSERT INTO `gameobject_battleground` VALUES ('1503693', '12', '3');
INSERT INTO `gameobject_battleground` VALUES ('1503694', '12', '0');
INSERT INTO `gameobject_battleground` VALUES ('1503695', '13', '3');
INSERT INTO `gameobject_battleground` VALUES ('1503696', '13', '0');
INSERT INTO `gameobject_battleground` VALUES ('1503697', '13', '3');
INSERT INTO `gameobject_battleground` VALUES ('1503698', '13', '0');
INSERT INTO `gameobject_battleground` VALUES ('1503699', '14', '3');
INSERT INTO `gameobject_battleground` VALUES ('1503700', '14', '0');
INSERT INTO `gameobject_battleground` VALUES ('1503701', '14', '3');
INSERT INTO `gameobject_battleground` VALUES ('1503702', '14', '0');
INSERT INTO `gameobject_battleground` VALUES ('1503703', '7', '3');
INSERT INTO `gameobject_battleground` VALUES ('1503704', '7', '3');
INSERT INTO `gameobject_battleground` VALUES ('1503705', '7', '3');
INSERT INTO `gameobject_battleground` VALUES ('1503706', '7', '3');
INSERT INTO `gameobject_battleground` VALUES ('1503707', '7', '3');
INSERT INTO `gameobject_battleground` VALUES ('1503708', '8', '3');
INSERT INTO `gameobject_battleground` VALUES ('1503709', '8', '3');
INSERT INTO `gameobject_battleground` VALUES ('1503710', '8', '3');
INSERT INTO `gameobject_battleground` VALUES ('1503711', '8', '3');
INSERT INTO `gameobject_battleground` VALUES ('1503712', '8', '3');
INSERT INTO `gameobject_battleground` VALUES ('1503713', '9', '3');
INSERT INTO `gameobject_battleground` VALUES ('1503714', '9', '3');
INSERT INTO `gameobject_battleground` VALUES ('1503715', '9', '3');
INSERT INTO `gameobject_battleground` VALUES ('1503716', '9', '3');
INSERT INTO `gameobject_battleground` VALUES ('1503717', '9', '3');
INSERT INTO `gameobject_battleground` VALUES ('1503718', '10', '3');
INSERT INTO `gameobject_battleground` VALUES ('1503719', '10', '3');
INSERT INTO `gameobject_battleground` VALUES ('1503720', '10', '3');
INSERT INTO `gameobject_battleground` VALUES ('1503721', '10', '3');
INSERT INTO `gameobject_battleground` VALUES ('1503722', '10', '3');
INSERT INTO `gameobject_battleground` VALUES ('1503723', '11', '1');
INSERT INTO `gameobject_battleground` VALUES ('1503724', '11', '1');
INSERT INTO `gameobject_battleground` VALUES ('1503725', '11', '1');
INSERT INTO `gameobject_battleground` VALUES ('1503726', '11', '1');
INSERT INTO `gameobject_battleground` VALUES ('1503727', '11', '1');
INSERT INTO `gameobject_battleground` VALUES ('1503728', '12', '1');
INSERT INTO `gameobject_battleground` VALUES ('1503729', '12', '1');
INSERT INTO `gameobject_battleground` VALUES ('1503730', '12', '1');
INSERT INTO `gameobject_battleground` VALUES ('1503731', '12', '1');
INSERT INTO `gameobject_battleground` VALUES ('1503732', '12', '1');
INSERT INTO `gameobject_battleground` VALUES ('1503733', '13', '1');
INSERT INTO `gameobject_battleground` VALUES ('1503734', '13', '1');
INSERT INTO `gameobject_battleground` VALUES ('1503735', '13', '1');
INSERT INTO `gameobject_battleground` VALUES ('1503736', '13', '1');
INSERT INTO `gameobject_battleground` VALUES ('1503737', '13', '1');
INSERT INTO `gameobject_battleground` VALUES ('1503738', '14', '1');
INSERT INTO `gameobject_battleground` VALUES ('1503739', '14', '1');
INSERT INTO `gameobject_battleground` VALUES ('1503740', '14', '1');
INSERT INTO `gameobject_battleground` VALUES ('1503741', '14', '1');
INSERT INTO `gameobject_battleground` VALUES ('1503742', '14', '1');
INSERT INTO `gameobject_battleground` VALUES ('1503743', '63', '0');
INSERT INTO `gameobject_battleground` VALUES ('1503744', '63', '0');
INSERT INTO `gameobject_battleground` VALUES ('1503745', '63', '0');
INSERT INTO `gameobject_battleground` VALUES ('1503746', '63', '0');
INSERT INTO `gameobject_battleground` VALUES ('1503747', '63', '0');
INSERT INTO `gameobject_battleground` VALUES ('1503748', '63', '0');
INSERT INTO `gameobject_battleground` VALUES ('1503749', '63', '0');
INSERT INTO `gameobject_battleground` VALUES ('1503750', '63', '0');
INSERT INTO `gameobject_battleground` VALUES ('1503751', '63', '0');
INSERT INTO `gameobject_battleground` VALUES ('1503752', '64', '0');
INSERT INTO `gameobject_battleground` VALUES ('1503753', '64', '0');
INSERT INTO `gameobject_battleground` VALUES ('1503754', '64', '0');
INSERT INTO `gameobject_battleground` VALUES ('1503755', '64', '0');
INSERT INTO `gameobject_battleground` VALUES ('1503756', '64', '0');
INSERT INTO `gameobject_battleground` VALUES ('1503757', '64', '0');
INSERT INTO `gameobject_battleground` VALUES ('1503758', '64', '0');
INSERT INTO `gameobject_battleground` VALUES ('1503759', '64', '0');
INSERT INTO `gameobject_battleground` VALUES ('1503760', '64', '0');
INSERT INTO `gameobject_battleground` VALUES ('90008', '254', '0');
INSERT INTO `gameobject_battleground` VALUES ('90009', '254', '0');
INSERT INTO `gameobject_battleground` VALUES ('90010', '254', '0');
INSERT INTO `gameobject_battleground` VALUES ('90011', '254', '0');
INSERT INTO `gameobject_battleground` VALUES ('90012', '254', '0');
INSERT INTO `gameobject_battleground` VALUES ('90013', '254', '0');
INSERT INTO `gameobject_battleground` VALUES ('90000', '0', '0');
INSERT INTO `gameobject_battleground` VALUES ('90001', '1', '0');
INSERT INTO `gameobject_battleground` VALUES ('90015', '0', '1');
INSERT INTO `gameobject_battleground` VALUES ('90016', '0', '4');
INSERT INTO `gameobject_battleground` VALUES ('90017', '0', '2');
INSERT INTO `gameobject_battleground` VALUES ('90018', '0', '3');
INSERT INTO `gameobject_battleground` VALUES ('90019', '0', '4');
INSERT INTO `gameobject_battleground` VALUES ('90020', '0', '1');
INSERT INTO `gameobject_battleground` VALUES ('90021', '0', '0');
INSERT INTO `gameobject_battleground` VALUES ('90022', '1', '3');
INSERT INTO `gameobject_battleground` VALUES ('90023', '1', '1');
INSERT INTO `gameobject_battleground` VALUES ('90024', '1', '4');
INSERT INTO `gameobject_battleground` VALUES ('90025', '1', '2');
INSERT INTO `gameobject_battleground` VALUES ('90026', '1', '3');
INSERT INTO `gameobject_battleground` VALUES ('90027', '1', '4');
INSERT INTO `gameobject_battleground` VALUES ('90028', '1', '1');
INSERT INTO `gameobject_battleground` VALUES ('90029', '1', '0');
INSERT INTO `gameobject_battleground` VALUES ('90030', '2', '3');
INSERT INTO `gameobject_battleground` VALUES ('90031', '2', '1');
INSERT INTO `gameobject_battleground` VALUES ('90032', '2', '4');
INSERT INTO `gameobject_battleground` VALUES ('90033', '2', '2');
INSERT INTO `gameobject_battleground` VALUES ('90034', '2', '3');
INSERT INTO `gameobject_battleground` VALUES ('90035', '2', '4');
INSERT INTO `gameobject_battleground` VALUES ('90036', '2', '1');
INSERT INTO `gameobject_battleground` VALUES ('90037', '2', '0');
INSERT INTO `gameobject_battleground` VALUES ('90038', '3', '3');
INSERT INTO `gameobject_battleground` VALUES ('90039', '3', '1');
INSERT INTO `gameobject_battleground` VALUES ('90040', '3', '4');
INSERT INTO `gameobject_battleground` VALUES ('90041', '3', '2');
INSERT INTO `gameobject_battleground` VALUES ('90042', '3', '3');
INSERT INTO `gameobject_battleground` VALUES ('90043', '3', '4');
INSERT INTO `gameobject_battleground` VALUES ('90044', '3', '1');
INSERT INTO `gameobject_battleground` VALUES ('90045', '3', '0');
INSERT INTO `gameobject_battleground` VALUES ('90046', '4', '3');
INSERT INTO `gameobject_battleground` VALUES ('90047', '4', '1');
INSERT INTO `gameobject_battleground` VALUES ('90048', '4', '4');
INSERT INTO `gameobject_battleground` VALUES ('90049', '4', '2');
INSERT INTO `gameobject_battleground` VALUES ('90050', '4', '3');
INSERT INTO `gameobject_battleground` VALUES ('90051', '4', '4');
INSERT INTO `gameobject_battleground` VALUES ('90052', '4', '1');
INSERT INTO `gameobject_battleground` VALUES ('90053', '4', '0');
INSERT INTO `gameobject_battleground` VALUES ('90054', '254', '0');
INSERT INTO `gameobject_battleground` VALUES ('90055', '254', '0');
INSERT INTO `gameobject_battleground` VALUES ('90056', '0', '0');
INSERT INTO `gameobject_battleground` VALUES ('90057', '0', '0');
INSERT INTO `gameobject_battleground` VALUES ('90058', '0', '0');
INSERT INTO `gameobject_battleground` VALUES ('90059', '1', '0');
INSERT INTO `gameobject_battleground` VALUES ('90060', '1', '0');
INSERT INTO `gameobject_battleground` VALUES ('90061', '1', '0');
INSERT INTO `gameobject_battleground` VALUES ('90062', '2', '0');
INSERT INTO `gameobject_battleground` VALUES ('90063', '2', '0');
INSERT INTO `gameobject_battleground` VALUES ('90064', '2', '0');
INSERT INTO `gameobject_battleground` VALUES ('90065', '3', '0');
INSERT INTO `gameobject_battleground` VALUES ('90066', '3', '0');
INSERT INTO `gameobject_battleground` VALUES ('90067', '3', '0');
INSERT INTO `gameobject_battleground` VALUES ('90068', '0', '1');
INSERT INTO `gameobject_battleground` VALUES ('90069', '0', '1');
INSERT INTO `gameobject_battleground` VALUES ('90070', '0', '1');
INSERT INTO `gameobject_battleground` VALUES ('90071', '1', '1');
INSERT INTO `gameobject_battleground` VALUES ('90072', '1', '1');
INSERT INTO `gameobject_battleground` VALUES ('90073', '1', '1');
INSERT INTO `gameobject_battleground` VALUES ('90074', '2', '1');
INSERT INTO `gameobject_battleground` VALUES ('90075', '2', '1');
INSERT INTO `gameobject_battleground` VALUES ('90076', '2', '1');
INSERT INTO `gameobject_battleground` VALUES ('90077', '3', '1');
INSERT INTO `gameobject_battleground` VALUES ('90078', '3', '1');
INSERT INTO `gameobject_battleground` VALUES ('90079', '3', '1');
INSERT INTO `gameobject_battleground` VALUES ('90080', '0', '2');
INSERT INTO `gameobject_battleground` VALUES ('90081', '0', '2');
INSERT INTO `gameobject_battleground` VALUES ('90082', '0', '2');
INSERT INTO `gameobject_battleground` VALUES ('90083', '1', '2');
INSERT INTO `gameobject_battleground` VALUES ('90084', '1', '2');
INSERT INTO `gameobject_battleground` VALUES ('90085', '1', '2');
INSERT INTO `gameobject_battleground` VALUES ('90086', '2', '2');
INSERT INTO `gameobject_battleground` VALUES ('90087', '2', '2');
INSERT INTO `gameobject_battleground` VALUES ('90088', '2', '2');
INSERT INTO `gameobject_battleground` VALUES ('90089', '3', '2');
INSERT INTO `gameobject_battleground` VALUES ('90090', '3', '2');
INSERT INTO `gameobject_battleground` VALUES ('90091', '3', '2');
INSERT INTO `gameobject_battleground` VALUES ('90092', '4', '4');
INSERT INTO `gameobject_battleground` VALUES ('90093', '4', '0');
INSERT INTO `gameobject_battleground` VALUES ('90094', '4', '1');
INSERT INTO `gameobject_battleground` VALUES ('90095', '4', '2');
INSERT INTO `gameobject_battleground` VALUES ('90096', '4', '3');
INSERT INTO `gameobject_battleground` VALUES ('90101', '254', '0');
INSERT INTO `gameobject_battleground` VALUES ('90102', '254', '0');
INSERT INTO `gameobject_battleground` VALUES ('90014', '0', '3');
INSERT INTO `gameobject_battleground` VALUES ('90103', '254', '0');
INSERT INTO `gameobject_battleground` VALUES ('90104', '254', '0');
INSERT INTO `gameobject_battleground` VALUES ('90107', '253', '0');
INSERT INTO `gameobject_battleground` VALUES ('90108', '253', '0');
INSERT INTO `gameobject_battleground` VALUES ('90109', '254', '0');
INSERT INTO `gameobject_battleground` VALUES ('90110', '254', '0');
INSERT INTO `gameobject_battleground` VALUES ('90113', '253', '0');
INSERT INTO `gameobject_battleground` VALUES ('90114', '253', '0');
INSERT INTO `gameobject_battleground` VALUES ('90115', '254', '0');
INSERT INTO `gameobject_battleground` VALUES ('90116', '254', '0');
INSERT INTO `gameobject_battleground` VALUES ('90117', '253', '0');
INSERT INTO `gameobject_battleground` VALUES ('90118', '253', '0');
