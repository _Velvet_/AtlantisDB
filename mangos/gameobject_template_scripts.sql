/*
Navicat MySQL Data Transfer

Source Server         : hometown
Source Server Version : 50523
Source Host           : localhost:3306
Source Database       : mangos

Target Server Type    : MYSQL
Target Server Version : 50523
File Encoding         : 65001

Date: 2012-09-14 21:45:04
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `gameobject_template_scripts`
-- ----------------------------
DROP TABLE IF EXISTS `gameobject_template_scripts`;
CREATE TABLE `gameobject_template_scripts` (
  `id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `delay` int(10) unsigned NOT NULL DEFAULT '0',
  `command` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `datalong` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `datalong2` int(10) unsigned NOT NULL DEFAULT '0',
  `buddy_entry` int(10) unsigned NOT NULL DEFAULT '0',
  `search_radius` int(10) unsigned NOT NULL DEFAULT '0',
  `data_flags` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `dataint` int(11) NOT NULL DEFAULT '0',
  `dataint2` int(11) NOT NULL DEFAULT '0',
  `dataint3` int(11) NOT NULL DEFAULT '0',
  `dataint4` int(11) NOT NULL DEFAULT '0',
  `x` float NOT NULL DEFAULT '0',
  `y` float NOT NULL DEFAULT '0',
  `z` float NOT NULL DEFAULT '0',
  `o` float NOT NULL DEFAULT '0',
  `comments` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gameobject_template_scripts
-- ----------------------------
INSERT INTO `gameobject_template_scripts` VALUES ('179985', '1', '10', '15041', '60000', '0', '0', '2', '0', '0', '0', '0', '0', '0', '0', '0', 'Spider Egg - Summon Spawn of Mar\'li');
INSERT INTO `gameobject_template_scripts` VALUES ('182267', '0', '17', '24538', '10', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Add 10 Fire Bombs to inventory');
INSERT INTO `gameobject_template_scripts` VALUES ('182267', '0', '30', '520', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Send Taxi path 520');
INSERT INTO `gameobject_template_scripts` VALUES ('182280', '0', '17', '24538', '10', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Add 10 Fire Bombs to inventory');
INSERT INTO `gameobject_template_scripts` VALUES ('182280', '0', '30', '523', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Send Taxi path 523');
INSERT INTO `gameobject_template_scripts` VALUES ('182281', '0', '17', '24538', '10', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Add 10 Fire Bombs to inventory');
INSERT INTO `gameobject_template_scripts` VALUES ('182281', '0', '30', '522', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Send Taxi path 522');
INSERT INTO `gameobject_template_scripts` VALUES ('182282', '0', '17', '24538', '10', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Add 10 Fire Bombs to inventory');
INSERT INTO `gameobject_template_scripts` VALUES ('182282', '0', '30', '524', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Send Taxi path 524');
INSERT INTO `gameobject_template_scripts` VALUES ('182301', '0', '17', '24538', '10', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Add 10 Fire Bombs to inventory');
INSERT INTO `gameobject_template_scripts` VALUES ('182301', '0', '30', '520', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Send Taxi path 520');
INSERT INTO `gameobject_template_scripts` VALUES ('182302', '0', '17', '24538', '10', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Add 10 Fire Bombs to inventory');
INSERT INTO `gameobject_template_scripts` VALUES ('182302', '0', '30', '523', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Send Taxi path 523');
INSERT INTO `gameobject_template_scripts` VALUES ('182303', '0', '17', '24538', '10', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Add 10 Fire Bombs to inventory');
INSERT INTO `gameobject_template_scripts` VALUES ('182303', '0', '30', '522', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Send Taxi path 522');
INSERT INTO `gameobject_template_scripts` VALUES ('182304', '0', '17', '24538', '10', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Add 10 Fire Bombs to inventory');
INSERT INTO `gameobject_template_scripts` VALUES ('182304', '0', '30', '524', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Send Taxi path 524');
