/*
Navicat MySQL Data Transfer

Source Server         : Hometown
Source Server Version : 50523
Source Host           : localhost:3306
Source Database       : mangos

Target Server Type    : MYSQL
Target Server Version : 50523
File Encoding         : 65001

Date: 2013-09-26 22:13:53
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `instance_template`
-- ----------------------------
DROP TABLE IF EXISTS `instance_template`;
CREATE TABLE `instance_template` (
  `map` smallint(5) unsigned NOT NULL,
  `parent` smallint(5) unsigned NOT NULL DEFAULT '0',
  `levelMin` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `levelMax` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `maxPlayers` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `reset_delay` int(10) unsigned NOT NULL DEFAULT '0',
  `ScriptName` varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (`map`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of instance_template
-- ----------------------------
INSERT INTO `instance_template` VALUES ('33', '0', '14', '30', '5', '0', 'instance_shadowfang_keep');
INSERT INTO `instance_template` VALUES ('34', '0', '15', '32', '10', '0', '');
INSERT INTO `instance_template` VALUES ('36', '0', '10', '20', '5', '0', 'instance_deadmines');
INSERT INTO `instance_template` VALUES ('43', '0', '10', '21', '5', '0', 'instance_wailing_caverns');
INSERT INTO `instance_template` VALUES ('47', '0', '17', '38', '5', '0', 'instance_razorfen_kraul');
INSERT INTO `instance_template` VALUES ('48', '0', '19', '32', '5', '0', 'instance_blackfathom_deeps');
INSERT INTO `instance_template` VALUES ('70', '0', '30', '47', '5', '0', 'instance_uldaman');
INSERT INTO `instance_template` VALUES ('90', '0', '15', '38', '5', '0', 'instance_gnomeregan');
INSERT INTO `instance_template` VALUES ('109', '0', '35', '55', '5', '0', 'instance_sunken_temple');
INSERT INTO `instance_template` VALUES ('129', '0', '25', '46', '10', '0', '');
INSERT INTO `instance_template` VALUES ('189', '0', '20', '45', '5', '0', 'instance_scarlet_monastery');
INSERT INTO `instance_template` VALUES ('209', '0', '35', '54', '5', '0', 'instance_zulfarrak');
INSERT INTO `instance_template` VALUES ('229', '0', '45', '70', '5', '0', 'instance_blackrock_spire');
INSERT INTO `instance_template` VALUES ('230', '0', '40', '70', '5', '0', 'instance_blackrock_depths');
INSERT INTO `instance_template` VALUES ('249', '0', '50', '70', '40', '0', 'instance_onyxias_lair');
INSERT INTO `instance_template` VALUES ('289', '0', '45', '70', '5', '0', 'instance_scholomance');
INSERT INTO `instance_template` VALUES ('309', '0', '50', '70', '20', '0', 'instance_zulgurub');
INSERT INTO `instance_template` VALUES ('329', '0', '45', '70', '5', '0', 'instance_stratholme');
INSERT INTO `instance_template` VALUES ('349', '0', '30', '55', '10', '0', '');
INSERT INTO `instance_template` VALUES ('389', '0', '8', '18', '10', '0', '');
INSERT INTO `instance_template` VALUES ('409', '0', '50', '70', '40', '0', 'instance_molten_core');
INSERT INTO `instance_template` VALUES ('429', '0', '45', '70', '5', '0', 'instance_dire_maul');
INSERT INTO `instance_template` VALUES ('469', '0', '60', '70', '40', '0', 'instance_blackwing_lair');
INSERT INTO `instance_template` VALUES ('509', '0', '50', '70', '20', '0', 'instance_ruins_of_ahnqiraj');
INSERT INTO `instance_template` VALUES ('531', '0', '50', '70', '40', '0', 'instance_temple_of_ahnqiraj');
INSERT INTO `instance_template` VALUES ('533', '0', '51', '70', '40', '0', 'instance_naxxramas');
INSERT INTO `instance_template` VALUES ('565', '0', '65', '70', '25', '0', 'instance_gruuls_lair');
INSERT INTO `instance_template` VALUES ('564', '0', '70', '70', '25', '0', 'instance_black_temple');
INSERT INTO `instance_template` VALUES ('560', '0', '66', '70', '5', '0', 'instance_old_hillsbrad');
INSERT INTO `instance_template` VALUES ('558', '0', '55', '70', '5', '0', '');
INSERT INTO `instance_template` VALUES ('557', '0', '55', '70', '5', '0', '');
INSERT INTO `instance_template` VALUES ('556', '0', '64', '70', '5', '0', 'instance_sethekk_halls');
INSERT INTO `instance_template` VALUES ('555', '0', '65', '70', '5', '0', 'instance_shadow_labyrinth');
INSERT INTO `instance_template` VALUES ('554', '0', '68', '70', '5', '0', 'instance_mechanar');
INSERT INTO `instance_template` VALUES ('553', '0', '68', '70', '5', '0', '');
INSERT INTO `instance_template` VALUES ('552', '0', '68', '70', '5', '0', 'instance_arcatraz');
INSERT INTO `instance_template` VALUES ('550', '0', '68', '70', '25', '0', 'instance_the_eye');
INSERT INTO `instance_template` VALUES ('548', '0', '68', '70', '25', '0', 'instance_serpent_shrine');
INSERT INTO `instance_template` VALUES ('547', '0', '55', '70', '5', '0', '');
INSERT INTO `instance_template` VALUES ('546', '0', '55', '70', '5', '0', '');
INSERT INTO `instance_template` VALUES ('545', '0', '55', '70', '5', '0', 'instance_steam_vault');
INSERT INTO `instance_template` VALUES ('544', '0', '65', '70', '25', '0', 'instance_magtheridons_lair');
INSERT INTO `instance_template` VALUES ('543', '0', '55', '62', '5', '0', 'instance_ramparts');
INSERT INTO `instance_template` VALUES ('542', '0', '55', '63', '5', '0', 'instance_blood_furnace');
INSERT INTO `instance_template` VALUES ('540', '0', '55', '70', '5', '0', 'instance_shattered_halls');
INSERT INTO `instance_template` VALUES ('534', '0', '70', '70', '25', '0', 'instance_hyjal');
INSERT INTO `instance_template` VALUES ('532', '0', '68', '70', '10', '0', 'instance_karazhan');
INSERT INTO `instance_template` VALUES ('269', '0', '66', '70', '5', '0', 'instance_dark_portal');
INSERT INTO `instance_template` VALUES ('568', '0', '68', '70', '10', '0', 'instance_zulaman');
INSERT INTO `instance_template` VALUES ('580', '0', '70', '70', '25', '0', 'instance_sunwell_plateau');
INSERT INTO `instance_template` VALUES ('585', '0', '70', '70', '5', '0', 'instance_magisters_terrace');
INSERT INTO `instance_template` VALUES ('489', '0', '10', '0', '50', '0', '');
INSERT INTO `instance_template` VALUES ('30', '0', '10', '0', '50', '0', '');
INSERT INTO `instance_template` VALUES ('529', '0', '10', '0', '50', '0', '');
INSERT INTO `instance_template` VALUES ('566', '0', '10', '0', '50', '0', '');
INSERT INTO `instance_template` VALUES ('559', '0', '10', '0', '50', '0', '');
INSERT INTO `instance_template` VALUES ('562', '0', '10', '0', '50', '0', '');
INSERT INTO `instance_template` VALUES ('572', '0', '10', '0', '50', '0', '');
