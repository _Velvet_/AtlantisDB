/*
Navicat MySQL Data Transfer

Source Server         : Hometown
Source Server Version : 50523
Source Host           : localhost:3306
Source Database       : mangos

Target Server Type    : MYSQL
Target Server Version : 50523
File Encoding         : 65001

Date: 2013-09-26 22:18:00
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `reference_loot_template`
-- ----------------------------
DROP TABLE IF EXISTS `reference_loot_template`;
CREATE TABLE `reference_loot_template` (
  `entry` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `item` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `ChanceOrQuestChance` float NOT NULL DEFAULT '100',
  `groupid` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `mincountOrRef` mediumint(9) NOT NULL DEFAULT '1',
  `maxcount` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `condition_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`entry`,`item`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='Loot System';

-- ----------------------------
-- Records of reference_loot_template
-- ----------------------------
INSERT INTO `reference_loot_template` VALUES ('50000', '31318', '0.005', '0', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('50000', '31319', '0.005', '0', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('50000', '31320', '0.005', '0', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('50000', '31321', '0.005', '0', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('50000', '31322', '0.005', '0', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('50000', '31323', '0.005', '0', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('50000', '31326', '0.005', '0', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('50000', '31328', '0.005', '0', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('50000', '31329', '0.005', '0', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('50000', '31330', '0.005', '0', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('50000', '31331', '0.005', '0', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('50000', '31332', '0.005', '0', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('50000', '31333', '0.005', '0', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('50000', '31334', '0.005', '0', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('50000', '31335', '0.005', '0', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('50000', '31336', '0.005', '0', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('50000', '31338', '0.005', '0', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('50000', '31339', '0.005', '0', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('50000', '31340', '0.005', '0', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('50000', '31342', '0.005', '0', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('50000', '31343', '0.005', '0', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('50000', '31883', '0.005', '0', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('50000', '31884', '0.005', '0', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('50000', '31885', '0.005', '0', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('50000', '31886', '0.005', '0', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('50000', '31887', '0.005', '0', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('50000', '31888', '0.005', '0', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('50000', '31889', '0.005', '0', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('50000', '31893', '0.005', '0', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('50000', '31894', '0.005', '0', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('50000', '31895', '0.005', '0', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('50000', '31896', '0.005', '0', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('50000', '31898', '0.005', '0', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('50000', '31899', '0.005', '0', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('50000', '31900', '0.005', '0', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('50000', '31902', '0.005', '0', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('50000', '31903', '0.005', '0', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('50000', '31904', '0.005', '0', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('50000', '31905', '0.005', '0', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('50000', '31906', '0.005', '0', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('50000', '31908', '0.005', '0', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('50000', '31909', '0.005', '0', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('50000', '31911', '0.005', '0', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('50000', '31912', '0.005', '0', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('50000', '31913', '0.005', '0', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('50000', '31915', '0.005', '0', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('50000', '31916', '0.005', '0', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('50000', '31917', '0.005', '0', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('50000', '31918', '0.005', '0', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('50000', '34622', '0.005', '0', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('50001', '34848', '33.3', '0', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('50001', '34851', '33.4', '0', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('50001', '34852', '33.3', '0', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('29500', '33590', '0', '1', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('29500', '33481', '0', '1', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('29500', '33591', '0', '1', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('50002', '34853', '33.3', '0', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('50002', '34854', '33.4', '0', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('50002', '34855', '33.3', '0', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('29500', '33480', '0', '1', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('29500', '33805', '0', '1', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('29500', '33971', '0', '1', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('29500', '33483', '0', '1', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('29500', '33489', '0', '1', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('29500', '33495', '0', '2', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('29500', '33493', '0', '2', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('29500', '33494', '0', '2', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('29500', '33490', '0', '2', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('29500', '33492', '0', '2', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('29500', '33491', '0', '2', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('29500', '33500', '0', '3', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('29500', '33496', '0', '3', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('29500', '33497', '0', '3', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('29500', '33498', '0', '3', '1', '1', '0');
INSERT INTO `reference_loot_template` VALUES ('29500', '33499', '0', '3', '1', '1', '0');
