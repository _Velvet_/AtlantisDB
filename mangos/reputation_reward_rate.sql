/*
Navicat MySQL Data Transfer

Source Server         : Hometown
Source Server Version : 50523
Source Host           : localhost:3306
Source Database       : mangos

Target Server Type    : MYSQL
Target Server Version : 50523
File Encoding         : 65001

Date: 2013-09-26 22:18:03
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `reputation_reward_rate`
-- ----------------------------
DROP TABLE IF EXISTS `reputation_reward_rate`;
CREATE TABLE `reputation_reward_rate` (
  `faction` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `quest_rate` float NOT NULL DEFAULT '1',
  `creature_rate` float NOT NULL DEFAULT '1',
  `spell_rate` float NOT NULL DEFAULT '1',
  PRIMARY KEY (`faction`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of reputation_reward_rate
-- ----------------------------
INSERT INTO `reputation_reward_rate` VALUES ('529', '2', '1', '2');
INSERT INTO `reputation_reward_rate` VALUES ('609', '2', '1', '2');
INSERT INTO `reputation_reward_rate` VALUES ('576', '4', '1', '4');
INSERT INTO `reputation_reward_rate` VALUES ('970', '3', '1', '3');
INSERT INTO `reputation_reward_rate` VALUES ('978', '2', '1', '2');
INSERT INTO `reputation_reward_rate` VALUES ('941', '2', '1', '2');
