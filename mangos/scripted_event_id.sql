/*
Navicat MySQL Data Transfer

Source Server         : Hometown
Source Server Version : 50523
Source Host           : localhost:3306
Source Database       : mangos

Target Server Type    : MYSQL
Target Server Version : 50523
File Encoding         : 65001

Date: 2013-09-26 22:18:27
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `scripted_event_id`
-- ----------------------------
DROP TABLE IF EXISTS `scripted_event_id`;
CREATE TABLE `scripted_event_id` (
  `id` mediumint(8) NOT NULL,
  `ScriptName` char(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Script library scripted events';

-- ----------------------------
-- Records of scripted_event_id
-- ----------------------------
INSERT INTO `scripted_event_id` VALUES ('8328', 'npc_kroshius');
INSERT INTO `scripted_event_id` VALUES ('5623', 'event_spell_gandling_shadow_portal');
INSERT INTO `scripted_event_id` VALUES ('5622', 'event_spell_gandling_shadow_portal');
INSERT INTO `scripted_event_id` VALUES ('5621', 'event_spell_gandling_shadow_portal');
INSERT INTO `scripted_event_id` VALUES ('5620', 'event_spell_gandling_shadow_portal');
INSERT INTO `scripted_event_id` VALUES ('5619', 'event_spell_gandling_shadow_portal');
INSERT INTO `scripted_event_id` VALUES ('5618', 'event_spell_gandling_shadow_portal');
INSERT INTO `scripted_event_id` VALUES ('13516', 'event_spell_soul_captured_credit');
INSERT INTO `scripted_event_id` VALUES ('13515', 'event_spell_soul_captured_credit');
INSERT INTO `scripted_event_id` VALUES ('13514', 'event_spell_soul_captured_credit');
INSERT INTO `scripted_event_id` VALUES ('13513', 'event_spell_soul_captured_credit');
INSERT INTO `scripted_event_id` VALUES ('3100', 'event_antalarion_statue_activation');
INSERT INTO `scripted_event_id` VALUES ('3099', 'event_antalarion_statue_activation');
INSERT INTO `scripted_event_id` VALUES ('3098', 'event_antalarion_statue_activation');
INSERT INTO `scripted_event_id` VALUES ('3097', 'event_antalarion_statue_activation');
INSERT INTO `scripted_event_id` VALUES ('3095', 'event_antalarion_statue_activation');
INSERT INTO `scripted_event_id` VALUES ('3094', 'event_antalarion_statue_activation');
INSERT INTO `scripted_event_id` VALUES ('8502', 'event_avatar_of_hakkar');
INSERT INTO `scripted_event_id` VALUES ('2268', 'event_spell_altar_boss_aggro');
INSERT INTO `scripted_event_id` VALUES ('2228', 'event_spell_altar_boss_aggro');
INSERT INTO `scripted_event_id` VALUES ('11225', 'event_taxi_stormcrow');
INSERT INTO `scripted_event_id` VALUES ('2609', 'event_spell_unlocking');
INSERT INTO `scripted_event_id` VALUES ('4884', 'event_spell_altar_emberseer');
INSERT INTO `scripted_event_id` VALUES ('16547', 'event_go_scrying_orb');
INSERT INTO `scripted_event_id` VALUES ('14797', 'event_spell_summon_raven_god');
INSERT INTO `scripted_event_id` VALUES ('11111', 'event_go_barrel_old_hillsbrad');
INSERT INTO `scripted_event_id` VALUES ('10951', 'event_spell_medivh_journal');
INSERT INTO `scripted_event_id` VALUES ('10591', 'event_spell_summon_nightbane');
INSERT INTO `scripted_event_id` VALUES ('2488', 'event_go_zulfarrak_gong');
