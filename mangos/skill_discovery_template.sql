/*
Navicat MySQL Data Transfer

Source Server         : Hometown
Source Server Version : 50523
Source Host           : localhost:3306
Source Database       : mangos

Target Server Type    : MYSQL
Target Server Version : 50523
File Encoding         : 65001

Date: 2013-09-26 22:18:31
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `skill_discovery_template`
-- ----------------------------
DROP TABLE IF EXISTS `skill_discovery_template`;
CREATE TABLE `skill_discovery_template` (
  `spellId` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT 'SpellId of the discoverable spell',
  `reqSpell` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT 'spell requirement',
  `chance` float NOT NULL DEFAULT '0' COMMENT 'chance to discover',
  PRIMARY KEY (`spellId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='Skill Discovery System';

-- ----------------------------
-- Records of skill_discovery_template
-- ----------------------------
INSERT INTO `skill_discovery_template` VALUES ('28590', '0', '0.1');
INSERT INTO `skill_discovery_template` VALUES ('28587', '0', '0.1');
INSERT INTO `skill_discovery_template` VALUES ('28588', '0', '0.1');
INSERT INTO `skill_discovery_template` VALUES ('28591', '0', '0.1');
INSERT INTO `skill_discovery_template` VALUES ('28589', '0', '0.1');
INSERT INTO `skill_discovery_template` VALUES ('28586', '0', '0.1');
INSERT INTO `skill_discovery_template` VALUES ('28585', '0', '0.1');
INSERT INTO `skill_discovery_template` VALUES ('28584', '0', '0.1');
INSERT INTO `skill_discovery_template` VALUES ('28580', '0', '0.1');
INSERT INTO `skill_discovery_template` VALUES ('28581', '0', '0.1');
INSERT INTO `skill_discovery_template` VALUES ('28583', '0', '0.1');
INSERT INTO `skill_discovery_template` VALUES ('28582', '0', '0.1');
INSERT INTO `skill_discovery_template` VALUES ('41458', '28575', '30');
INSERT INTO `skill_discovery_template` VALUES ('41500', '28571', '30');
INSERT INTO `skill_discovery_template` VALUES ('41501', '28572', '30');
INSERT INTO `skill_discovery_template` VALUES ('41502', '28573', '30');
INSERT INTO `skill_discovery_template` VALUES ('41503', '28576', '30');
