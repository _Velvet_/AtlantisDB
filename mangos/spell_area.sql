/*
Navicat MySQL Data Transfer

Source Server         : Hometown
Source Server Version : 50523
Source Host           : localhost:3306
Source Database       : mangos

Target Server Type    : MYSQL
Target Server Version : 50523
File Encoding         : 65001

Date: 2013-09-26 22:18:55
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `spell_area`
-- ----------------------------
DROP TABLE IF EXISTS `spell_area`;
CREATE TABLE `spell_area` (
  `spell` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `area` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `quest_start` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `quest_start_active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `quest_end` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `condition_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `aura_spell` mediumint(8) NOT NULL DEFAULT '0',
  `racemask` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `gender` tinyint(1) unsigned NOT NULL DEFAULT '2',
  `autocast` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`spell`,`area`,`quest_start`,`quest_start_active`,`aura_spell`,`racemask`,`gender`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of spell_area
-- ----------------------------
INSERT INTO `spell_area` VALUES ('35480', '2367', '0', '0', '0', '0', '0', '690', '0', '1');
INSERT INTO `spell_area` VALUES ('35481', '2367', '0', '0', '0', '0', '0', '690', '1', '1');
INSERT INTO `spell_area` VALUES ('35482', '2367', '0', '0', '0', '0', '0', '1032', '0', '1');
INSERT INTO `spell_area` VALUES ('35483', '2367', '0', '0', '0', '0', '0', '1032', '1', '1');
INSERT INTO `spell_area` VALUES ('40567', '3522', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('40568', '3522', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('40572', '3522', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('40573', '3522', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('40575', '3522', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('40576', '3522', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('41608', '3606', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('41609', '3606', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('41610', '3606', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('41611', '3606', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('46837', '3606', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('46839', '3606', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('41608', '3607', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('41609', '3607', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('41610', '3607', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('41611', '3607', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('41617', '3607', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('41619', '3607', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('46837', '3607', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('46839', '3607', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('41617', '3715', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('41619', '3715', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('41617', '3716', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('41619', '3716', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('41617', '3717', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('41619', '3717', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('40214', '3759', '11013', '1', '0', '0', '0', '0', '2', '1');
INSERT INTO `spell_area` VALUES ('33836', '3803', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('41608', '3845', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('41609', '3845', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('41610', '3845', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('41611', '3845', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('41618', '3845', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('41620', '3845', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('46837', '3845', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('46839', '3845', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('41608', '3847', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('41609', '3847', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('41610', '3847', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('41611', '3847', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('41618', '3847', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('41620', '3847', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('46837', '3847', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('46839', '3847', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('41608', '3848', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('41609', '3848', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('41610', '3848', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('41611', '3848', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('41618', '3848', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('41620', '3848', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('46837', '3848', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('46839', '3848', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('41608', '3849', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('41609', '3849', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('41610', '3849', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('41611', '3849', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('41618', '3849', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('41620', '3849', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('46837', '3849', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('46839', '3849', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('40567', '3923', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('40568', '3923', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('40572', '3923', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('40573', '3923', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('40575', '3923', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('40576', '3923', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('40214', '3939', '11013', '1', '0', '0', '0', '0', '2', '1');
INSERT INTO `spell_area` VALUES ('41608', '3959', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('41609', '3959', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('41610', '3959', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('41611', '3959', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('46837', '3959', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('46839', '3959', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('40214', '3966', '11013', '1', '0', '0', '0', '0', '2', '1');
INSERT INTO `spell_area` VALUES ('41608', '4075', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('41609', '4075', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('41610', '4075', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('41611', '4075', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('45373', '4075', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('46837', '4075', '0', '0', '0', '0', '0', '0', '2', '0');
INSERT INTO `spell_area` VALUES ('46839', '4075', '0', '0', '0', '0', '0', '0', '2', '0');
