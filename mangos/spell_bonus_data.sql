/*
Navicat MySQL Data Transfer

Source Server         : Hometown
Source Server Version : 50523
Source Host           : localhost:3306
Source Database       : mangos

Target Server Type    : MYSQL
Target Server Version : 50523
File Encoding         : 65001

Date: 2013-09-26 22:18:59
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `spell_bonus_data`
-- ----------------------------
DROP TABLE IF EXISTS `spell_bonus_data`;
CREATE TABLE `spell_bonus_data` (
  `entry` smallint(5) unsigned NOT NULL,
  `direct_bonus` float NOT NULL DEFAULT '0',
  `dot_bonus` float NOT NULL DEFAULT '0',
  `ap_bonus` float NOT NULL DEFAULT '0',
  `ap_dot_bonus` float NOT NULL DEFAULT '0',
  `comments` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of spell_bonus_data
-- ----------------------------
INSERT INTO `spell_bonus_data` VALUES ('5672', '0', '0.045', '0', '0', 'Shaman - Healing Stream Totem');
INSERT INTO `spell_bonus_data` VALUES ('974', '0.2857', '0', '0', '0', 'Shaman - Earth Shield');
INSERT INTO `spell_bonus_data` VALUES ('18562', '0', '0', '0', '0', 'Druid - Swiftmend');
INSERT INTO `spell_bonus_data` VALUES ('1454', '0.8', '0', '0', '0', 'Warlock - Life Tap');
INSERT INTO `spell_bonus_data` VALUES ('339', '0', '0.1', '0', '0', 'Druid - Entangling Roots');
INSERT INTO `spell_bonus_data` VALUES ('42231', '0.12898', '0', '0', '0', 'Druid - Hurricane Triggered');
INSERT INTO `spell_bonus_data` VALUES ('5570', '0', '0.127', '0', '0', 'Druid - Insect Swarm');
INSERT INTO `spell_bonus_data` VALUES ('8921', '0.1515', '0.13', '0', '0', 'Druid - Moonfire');
INSERT INTO `spell_bonus_data` VALUES ('5176', '0.5714', '0', '0', '0', 'Druid - Wrath');
INSERT INTO `spell_bonus_data` VALUES ('7268', '0.2857', '0', '0', '0', 'Mage - Arcane Missiles Triggered Spell Rank 1');
INSERT INTO `spell_bonus_data` VALUES ('7269', '0.2857', '0', '0', '0', 'Mage - Arcane Missiles Triggered Spell Rank 2');
INSERT INTO `spell_bonus_data` VALUES ('7270', '0.2857', '0', '0', '0', 'Mage - Arcane Missiles Triggered Spell Rank 3');
INSERT INTO `spell_bonus_data` VALUES ('8419', '0.2857', '0', '0', '0', 'Mage - Arcane Missiles Triggered Spell Rank 4');
INSERT INTO `spell_bonus_data` VALUES ('8418', '0.2857', '0', '0', '0', 'Mage - Arcane Missiles Triggered Spell Rank 5');
INSERT INTO `spell_bonus_data` VALUES ('10273', '0.2857', '0', '0', '0', 'Mage - Arcane Missiles Triggered Spell Rank 6');
INSERT INTO `spell_bonus_data` VALUES ('10274', '0.2857', '0', '0', '0', 'Mage - Arcane Missiles Triggered Spell Rank 7');
INSERT INTO `spell_bonus_data` VALUES ('25346', '0.2857', '0', '0', '0', 'Mage - Arcane Missiles Triggered Spell Rank 8');
INSERT INTO `spell_bonus_data` VALUES ('27076', '0.2857', '0', '0', '0', 'Mage - Arcane Missiles Triggered Spell Rank 9');
INSERT INTO `spell_bonus_data` VALUES ('38700', '0.2857', '0', '0', '0', 'Mage - Arcane Missiles Triggered Spell Rank 10');
INSERT INTO `spell_bonus_data` VALUES ('38703', '0.2857', '0', '0', '0', 'Mage - Arcane Missiles Triggered Spell Rank 11');
INSERT INTO `spell_bonus_data` VALUES ('1463', '0.8053', '0', '0', '0', 'Mage - Mana Shield');
INSERT INTO `spell_bonus_data` VALUES ('11113', '0.1357', '0', '0', '0', 'Mage - Blast Wave Rank');
INSERT INTO `spell_bonus_data` VALUES ('31661', '0.1357', '0', '0', '0', 'Mage - Dragons Breath');
INSERT INTO `spell_bonus_data` VALUES ('133', '1', '0', '0', '0', 'Mage - Fire Ball');
INSERT INTO `spell_bonus_data` VALUES ('2120', '0.2357', '0.122', '0', '0', 'Mage - Flamestrike');
INSERT INTO `spell_bonus_data` VALUES ('42208', '0.0952', '0', '0', '0', 'Mage - Blizzard Triggered Spell Rank 1');
INSERT INTO `spell_bonus_data` VALUES ('42209', '0.0952', '0', '0', '0', 'Mage - Blizzard Triggered Spell Rank 2');
INSERT INTO `spell_bonus_data` VALUES ('42210', '0.0952', '0', '0', '0', 'Mage - Blizzard Triggered Spell Rank 3');
INSERT INTO `spell_bonus_data` VALUES ('42211', '0.0952', '0', '0', '0', 'Mage - Blizzard Triggered Spell Rank 4');
INSERT INTO `spell_bonus_data` VALUES ('42212', '0.0952', '0', '0', '0', 'Mage - Blizzard Triggered Spell Rank 5');
INSERT INTO `spell_bonus_data` VALUES ('42213', '0.0952', '0', '0', '0', 'Mage - Blizzard Triggered Spell Rank 6');
INSERT INTO `spell_bonus_data` VALUES ('42198', '0.0952', '0', '0', '0', 'Mage - Blizzard Triggered Spell Rank 7');
INSERT INTO `spell_bonus_data` VALUES ('120', '0.1357', '0', '0', '0', 'Mage - Cone of Cold');
INSERT INTO `spell_bonus_data` VALUES ('116', '0.8143', '0', '0', '0', 'Mage - Frost Bolt');
INSERT INTO `spell_bonus_data` VALUES ('30455', '0.1429', '0', '0', '0', 'Mage - Ice Lance');
INSERT INTO `spell_bonus_data` VALUES ('31935', '0.07', '0', '0.07', '0', 'Paladin - Avengers Shiled');
INSERT INTO `spell_bonus_data` VALUES ('879', '0.15', '0', '0.15', '0', 'Paladin - Exorcism');
INSERT INTO `spell_bonus_data` VALUES ('24275', '0.15', '0', '0.15', '0', 'Paladin - Hammer of Wrath');
INSERT INTO `spell_bonus_data` VALUES ('20925', '0.09', '0', '0.056', '0', 'Paladin - Holy Shield');
INSERT INTO `spell_bonus_data` VALUES ('2812', '0.07', '0', '0.07', '0', 'Paladin - Holy Wrath');
INSERT INTO `spell_bonus_data` VALUES ('31893', '0.25', '0', '0.16', '0', 'Paladin - Seal of Blood Enemy Proc');
INSERT INTO `spell_bonus_data` VALUES ('32221', '0.25', '0', '0.16', '0', 'Paladin - Seal of Blood Self Proc');
INSERT INTO `spell_bonus_data` VALUES ('20424', '0.25', '0', '0.16', '0', 'Paladin - Seal of Command Proc');
INSERT INTO `spell_bonus_data` VALUES ('379', '0', '0', '0', '0', 'Shaman - Earth Shield Triggered');
INSERT INTO `spell_bonus_data` VALUES ('20167', '0.25', '0', '0.16', '0', 'Paladin - Seal of Light Proc');
INSERT INTO `spell_bonus_data` VALUES ('25742', '0.07', '0', '0.039', '0', 'Paladin - Seal of Righteousness Dummy Proc');
INSERT INTO `spell_bonus_data` VALUES ('26573', '0', '0.04', '0', '0.04', 'Paladin - Consecration');
INSERT INTO `spell_bonus_data` VALUES ('755', '0', '0.2857', '0', '0', 'Warlock - Health Funnel');
INSERT INTO `spell_bonus_data` VALUES ('27805', '0.1606', '0', '0', '0', 'Priest - Holy Nova Heal Rank 6');
INSERT INTO `spell_bonus_data` VALUES ('27804', '0.1606', '0', '0', '0', 'Priest - Holy Nova Heal Rank 5');
INSERT INTO `spell_bonus_data` VALUES ('27803', '0.1606', '0', '0', '0', 'Priest - Holy Nova Heal Rank 4');
INSERT INTO `spell_bonus_data` VALUES ('23459', '0.1606', '0', '0', '0', 'Priest - Holy Nova Heal Rank 3');
INSERT INTO `spell_bonus_data` VALUES ('23458', '0.1606', '0', '0', '0', 'Priest - Holy Nova Heal Rank 2');
INSERT INTO `spell_bonus_data` VALUES ('5185', '1', '0', '0', '0', 'Druid - Healing Touch');
INSERT INTO `spell_bonus_data` VALUES ('774', '0', '0.16', '0', '0', 'Druid - Rejuvenation');
INSERT INTO `spell_bonus_data` VALUES ('8936', '0.3', '0.1', '0', '0', 'Druid - Regrowth');
INSERT INTO `spell_bonus_data` VALUES ('44203', '0.1825', '0', '0', '0', 'Druid - Tranquility Triggered');
INSERT INTO `spell_bonus_data` VALUES ('23455', '0.1606', '0', '0', '0', 'Priest - Holy Nova Heal Rank 1');
INSERT INTO `spell_bonus_data` VALUES ('14914', '0.5711', '0.024', '0', '0', 'Priest - Holy Fire');
INSERT INTO `spell_bonus_data` VALUES ('15237', '0.1606', '0', '0', '0', 'Priest - Holy Nova Damage');
INSERT INTO `spell_bonus_data` VALUES ('8129', '0', '0', '0', '0', 'Priest - Mana Burn');
INSERT INTO `spell_bonus_data` VALUES ('34433', '0.65', '0', '0', '0', 'Priest - Shadowfiend');
INSERT INTO `spell_bonus_data` VALUES ('585', '0.714', '0', '0', '0', 'Priest - Smite');
INSERT INTO `spell_bonus_data` VALUES ('34914', '0', '0.2', '0', '0', 'Priest - Vampiric Touch');
INSERT INTO `spell_bonus_data` VALUES ('33763', '0.3429', '0.0742', '0', '0', 'Druid - Lifebloom');
INSERT INTO `spell_bonus_data` VALUES ('8042', '0.3858', '0', '0', '0', 'Shaman - Earth Shock');
INSERT INTO `spell_bonus_data` VALUES ('8443', '0.2142', '0', '0', '0', 'Shaman - Fire Nova Totem Casted by Totem Rank 1');
INSERT INTO `spell_bonus_data` VALUES ('8504', '0.2142', '0', '0', '0', 'Shaman - Fire Nova Totem Casted by Totem Rank 2');
INSERT INTO `spell_bonus_data` VALUES ('8505', '0.2142', '0', '0', '0', 'Shaman - Fire Nova Totem Casted by Totem Rank 3');
INSERT INTO `spell_bonus_data` VALUES ('11310', '0.2142', '0', '0', '0', 'Shaman - Fire Nova Totem Casted by Totem Rank 4');
INSERT INTO `spell_bonus_data` VALUES ('11311', '0.2142', '0', '0', '0', 'Shaman - Fire Nova Totem Casted by Totem Rank 5');
INSERT INTO `spell_bonus_data` VALUES ('25538', '0.2142', '0', '0', '0', 'Shaman - Fire Nova Totem Casted by Totem Rank 6');
INSERT INTO `spell_bonus_data` VALUES ('25539', '0.2142', '0', '0', '0', 'Shaman - Fire Nova Totem Casted by Totem Rank 7');
INSERT INTO `spell_bonus_data` VALUES ('8050', '0.2142', '0.1', '0', '0', 'Shaman - Flame Shock');
INSERT INTO `spell_bonus_data` VALUES ('8026', '0.1', '0', '0', '0', 'Shaman - Flametongue Weapon Proc Rank 1');
INSERT INTO `spell_bonus_data` VALUES ('8028', '0.1', '0', '0', '0', 'Shaman - Flametongue Weapon Proc Rank 2');
INSERT INTO `spell_bonus_data` VALUES ('8029', '0.1', '0', '0', '0', 'Shaman - Flametongue Weapon Proc Rank 3');
INSERT INTO `spell_bonus_data` VALUES ('10445', '0.1', '0', '0', '0', 'Shaman - Flametongue Weapon Proc Rank 4');
INSERT INTO `spell_bonus_data` VALUES ('16343', '0.1', '0', '0', '0', 'Shaman - Flametongue Weapon Proc Rank 5');
INSERT INTO `spell_bonus_data` VALUES ('16344', '0.1', '0', '0', '0', 'Shaman - Flametongue Weapon Proc Rank 6');
INSERT INTO `spell_bonus_data` VALUES ('25488', '0.1', '0', '0', '0', 'Shaman - Flametongue Weapon Proc Rank 7');
INSERT INTO `spell_bonus_data` VALUES ('8056', '0.3858', '0', '0', '0', 'Shaman - Frost Shock');
INSERT INTO `spell_bonus_data` VALUES ('8034', '0.1', '0', '0', '0', 'Shaman - Frostbrand Attack Rank 1');
INSERT INTO `spell_bonus_data` VALUES ('5707', '0', '0', '0', '0', 'Item - Lifestone Regeneration');
INSERT INTO `spell_bonus_data` VALUES ('38395', '0', '0', '0', '0', 'Item - Siphon Essence');
INSERT INTO `spell_bonus_data` VALUES ('31024', '0', '0', '0', '0', 'Item - Living Ruby Pedant');
INSERT INTO `spell_bonus_data` VALUES ('18790', '0', '0', '0', '0', 'Warlock - Fel Stamina');
INSERT INTO `spell_bonus_data` VALUES ('40293', '0', '0', '0', '0', 'Item - Siphon Essence');
INSERT INTO `spell_bonus_data` VALUES ('403', '0.7143', '0', '0', '0', 'Shaman - Lightning Bolt');
INSERT INTO `spell_bonus_data` VALUES ('26364', '0.33', '0', '0', '0', 'Shaman - Lightning Shield Proc Rank 1');
INSERT INTO `spell_bonus_data` VALUES ('26365', '0.33', '0', '0', '0', 'Shaman - Lightning Shield Proc Rank 2');
INSERT INTO `spell_bonus_data` VALUES ('26366', '0.33', '0', '0', '0', 'Shaman - Lightning Shield Proc Rank 3');
INSERT INTO `spell_bonus_data` VALUES ('26367', '0.33', '0', '0', '0', 'Shaman - Lightning Shield Proc Rank 4');
INSERT INTO `spell_bonus_data` VALUES ('26369', '0.33', '0', '0', '0', 'Shaman - Lightning Shield Proc Rank 5');
INSERT INTO `spell_bonus_data` VALUES ('26370', '0.33', '0', '0', '0', 'Shaman - Lightning Shield Proc Rank 6');
INSERT INTO `spell_bonus_data` VALUES ('26363', '0.33', '0', '0', '0', 'Shaman - Lightning Shield Proc Rank 7');
INSERT INTO `spell_bonus_data` VALUES ('26371', '0.33', '0', '0', '0', 'Shaman - Lightning Shield Proc Rank 8');
INSERT INTO `spell_bonus_data` VALUES ('26372', '0.33', '0', '0', '0', 'Shaman - Lightning Shield Proc Rank 9');
INSERT INTO `spell_bonus_data` VALUES ('8188', '0.1', '0', '0', '0', 'Shaman - Magma Totem Passive Rank 1');
INSERT INTO `spell_bonus_data` VALUES ('10582', '0.1', '0', '0', '0', 'Shaman - Magma Totem Passive Rank 2');
INSERT INTO `spell_bonus_data` VALUES ('10583', '0.1', '0', '0', '0', 'Shaman - Magma Totem Passive Rank 3');
INSERT INTO `spell_bonus_data` VALUES ('10584', '0.1', '0', '0', '0', 'Shaman - Magma Totem Passive Rank 4');
INSERT INTO `spell_bonus_data` VALUES ('25551', '0.1', '0', '0', '0', 'Shaman - Magma Totem Passive Rank 5');
INSERT INTO `spell_bonus_data` VALUES ('3606', '0.1667', '0', '0', '0', 'Shaman - Searing Totem Attack Rank 1');
INSERT INTO `spell_bonus_data` VALUES ('6350', '0.1667', '0', '0', '0', 'Shaman - Searing Totem Attack Rank 2');
INSERT INTO `spell_bonus_data` VALUES ('6351', '0.1667', '0', '0', '0', 'Shaman - Searing Totem Attack Rank 3');
INSERT INTO `spell_bonus_data` VALUES ('6352', '0.1667', '0', '0', '0', 'Shaman - Searing Totem Attack Rank 4');
INSERT INTO `spell_bonus_data` VALUES ('10435', '0.1667', '0', '0', '0', 'Shaman - Searing Totem Attack Rank 5');
INSERT INTO `spell_bonus_data` VALUES ('10436', '0.1667', '0', '0', '0', 'Shaman - Searing Totem Attack Rank 6');
INSERT INTO `spell_bonus_data` VALUES ('25530', '0.1667', '0', '0', '0', 'Shaman - Searing Totem Attack Rank 7');
INSERT INTO `spell_bonus_data` VALUES ('980', '0', '0.1', '0', '0', 'Warlock - Curse of Agony');
INSERT INTO `spell_bonus_data` VALUES ('603', '0', '2', '0', '0', 'Warlock - Curse of Doom');
INSERT INTO `spell_bonus_data` VALUES ('172', '0', '0.156', '0', '0', 'Warlock - Corruption');
INSERT INTO `spell_bonus_data` VALUES ('348', '0.2', '0.2', '0', '0', 'Warlock - Immolate');
INSERT INTO `spell_bonus_data` VALUES ('27243', '0.22', '0.25', '0', '0', 'Warlock - Seed of Corruption');
INSERT INTO `spell_bonus_data` VALUES ('25329', '0.1606', '0', '0', '0', 'Priest - Holy Nova Heal Rank 7');
INSERT INTO `spell_bonus_data` VALUES ('30108', '0', '0.24', '0', '0', 'Warlock - Unstable Affliction');
INSERT INTO `spell_bonus_data` VALUES ('686', '0.8571', '0', '0', '0', 'Warlock - Shadow Bolt');
INSERT INTO `spell_bonus_data` VALUES ('6353', '1.15', '0', '0', '0', 'Warlock - Soul Fire');
INSERT INTO `spell_bonus_data` VALUES ('44461', '0.4', '0', '0', '0', 'Mage - Living Bomb');
INSERT INTO `spell_bonus_data` VALUES ('5138', '0', '0', '0', '0', 'Warlock - Drain Mana');
INSERT INTO `spell_bonus_data` VALUES ('1120', '0', '0.4286', '0', '0', 'Warlock - Drain Soul');
INSERT INTO `spell_bonus_data` VALUES ('10444', '0', '0', '0', '0', 'Shaman - Flametongue Attack');
INSERT INTO `spell_bonus_data` VALUES ('1949', '0', '0.0946', '0', '0', 'Warlock - Hellfire');
INSERT INTO `spell_bonus_data` VALUES ('5857', '0.1428', '0', '0', '0', 'Warlock - Hellfire Effect on Enemy Rank 1');
INSERT INTO `spell_bonus_data` VALUES ('11681', '0.1428', '0', '0', '0', 'Warlock - Hellfire Effect on Enemy Rank 2');
INSERT INTO `spell_bonus_data` VALUES ('11682', '0.1428', '0', '0', '0', 'Warlock - Hellfire Effect on Enemy Rank 3');
INSERT INTO `spell_bonus_data` VALUES ('27214', '0.1428', '0', '0', '0', 'Warlock - Hellfire Effect on Enemy Rank 4');
INSERT INTO `spell_bonus_data` VALUES ('42223', '0.952', '0', '0', '0', 'Warlock - Rain of Fire Triggered Rank 1');
INSERT INTO `spell_bonus_data` VALUES ('42224', '0.952', '0', '0', '0', 'Warlock - Rain of Fire Triggered Rank 2');
INSERT INTO `spell_bonus_data` VALUES ('42225', '0.952', '0', '0', '0', 'Warlock - Rain of Fire Triggered Rank 3');
INSERT INTO `spell_bonus_data` VALUES ('42226', '0.952', '0', '0', '0', 'Warlock - Rain of Fire Triggered Rank 4');
INSERT INTO `spell_bonus_data` VALUES ('42218', '0.952', '0', '0', '0', 'Warlock - Rain of Fire Triggered Rank 5');
INSERT INTO `spell_bonus_data` VALUES ('18220', '0.96', '0', '0', '0', 'Warlock - Dark Pact');
INSERT INTO `spell_bonus_data` VALUES ('6229', '0.3', '0', '0', '0', 'Warlock - Shadow Ward');
INSERT INTO `spell_bonus_data` VALUES ('31117', '1.8', '0', '0', '0', 'Warlock - Unstable Affliction Dispell');
INSERT INTO `spell_bonus_data` VALUES ('28176', '0', '0', '0', '0', 'Warlock - Fel Armor');
INSERT INTO `spell_bonus_data` VALUES ('34913', '0', '0', '0', '0', 'Mage - Molten Armor Triggered');
INSERT INTO `spell_bonus_data` VALUES ('44459', '0.4', '0', '0', '0', 'Mage - Living Bomb');
INSERT INTO `spell_bonus_data` VALUES ('15407', '0', '0.19', '0', '0', 'Priest - Mind Flay');
INSERT INTO `spell_bonus_data` VALUES ('331', '0.8571', '0', '0', '0', 'Shaman - Healing Wave');
INSERT INTO `spell_bonus_data` VALUES ('17712', '0', '0', '0', '0', 'Item - Lifestone Healing');
INSERT INTO `spell_bonus_data` VALUES ('46567', '0', '0', '0', '0', 'Item - Goblin Rocket Launcher');
INSERT INTO `spell_bonus_data` VALUES ('9007', '0', '0', '0', '0.03', 'Druid - Pounce Bleed');
INSERT INTO `spell_bonus_data` VALUES ('1822', '0', '0', '0', '0.02', 'Druid - Rake');
INSERT INTO `spell_bonus_data` VALUES ('33745', '0', '0', '0.01', '0.01', 'Druid - Lacerate');
INSERT INTO `spell_bonus_data` VALUES ('703', '0', '0', '0', '0.03', 'Rogue - Garrote');
INSERT INTO `spell_bonus_data` VALUES ('1495', '0', '0', '0.2', '0', 'Hunter - Mongoose Bite');
INSERT INTO `spell_bonus_data` VALUES ('1978', '0', '0', '0', '0.02', 'Hunter - Serpent Sting');
INSERT INTO `spell_bonus_data` VALUES ('3044', '0', '0', '0.15', '0', 'Hunter - Arcane Shot');
INSERT INTO `spell_bonus_data` VALUES ('13797', '0', '0', '0', '0.02', 'Hunter - Immolation Trap');
INSERT INTO `spell_bonus_data` VALUES ('13812', '0', '0', '0.1', '0.1', 'Hunter - Explosive Trap');
INSERT INTO `spell_bonus_data` VALUES ('43733', '0', '0', '0', '0', 'Item - Lightning Zap');
INSERT INTO `spell_bonus_data` VALUES ('15662', '0', '0', '0', '0', 'Item - Six Demon Bag - Fireball');
INSERT INTO `spell_bonus_data` VALUES ('11538', '0', '0', '0', '0', 'Item - Six Demon Bag - Frostbolt');
INSERT INTO `spell_bonus_data` VALUES ('21179', '0', '0', '0', '0', 'Item - Six Demon Bag - Chain Lightning');
INSERT INTO `spell_bonus_data` VALUES ('33110', '0.8057', '0', '0', '0', 'Priest - Prayer of Mending');
INSERT INTO `spell_bonus_data` VALUES ('45429', '0', '0', '0', '0', 'Shattered Sun Pendant of Acumen');
INSERT INTO `spell_bonus_data` VALUES ('45428', '0', '0', '0', '0', 'Shattered Sun Pendant of Might');
INSERT INTO `spell_bonus_data` VALUES ('45430', '0', '0', '0', '0', 'Shattered Sun Pendant of Restoration');
INSERT INTO `spell_bonus_data` VALUES ('45055', '0', '0', '0', '0', 'Timbal\'\'s Focusing Crystal');
INSERT INTO `spell_bonus_data` VALUES ('31801', '100', '0', '100', '0', 'Paladin - Seal of Vengeance');
INSERT INTO `spell_bonus_data` VALUES ('2943', '0.1', '0', '0', '0', 'Priest - Touch of Weakness 1');
INSERT INTO `spell_bonus_data` VALUES ('19249', '0.1', '0', '0', '0', 'Priest - Touch of Weakness 2');
INSERT INTO `spell_bonus_data` VALUES ('19251', '0.1', '0', '0', '0', 'Priest - Touch of Weakness 3');
INSERT INTO `spell_bonus_data` VALUES ('19252', '0.1', '0', '0', '0', 'Priest - Touch of Weakness 4');
INSERT INTO `spell_bonus_data` VALUES ('25460', '0.1', '0', '0', '0', 'Priest - Touch of Weakness 7');
INSERT INTO `spell_bonus_data` VALUES ('19253', '0.1', '0', '0', '0', 'Priest - Touch of Weakness 5');
INSERT INTO `spell_bonus_data` VALUES ('19254', '0.1', '0', '0', '0', 'Priest - Touch of Weakness 6');
INSERT INTO `spell_bonus_data` VALUES ('46579', '0', '0', '0', '0', 'Deathfrost');
INSERT INTO `spell_bonus_data` VALUES ('40972', '0', '0', '0', '0', 'Crystal Spire of Karabor');
INSERT INTO `spell_bonus_data` VALUES ('45034', '0', '0', '0', '0', 'Curse of Boundless Agony');
