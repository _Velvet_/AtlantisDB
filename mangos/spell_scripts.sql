/*
Navicat MySQL Data Transfer

Source Server         : Hometown
Source Server Version : 50523
Source Host           : localhost:3306
Source Database       : mangos

Target Server Type    : MYSQL
Target Server Version : 50523
File Encoding         : 65001

Date: 2012-12-30 20:17:57
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `spell_scripts`
-- ----------------------------
DROP TABLE IF EXISTS `spell_scripts`;
CREATE TABLE `spell_scripts` (
  `id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `delay` int(10) unsigned NOT NULL DEFAULT '0',
  `command` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `datalong` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `datalong2` int(10) unsigned NOT NULL DEFAULT '0',
  `buddy_entry` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `search_radius` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `data_flags` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `dataint` int(11) NOT NULL DEFAULT '0',
  `dataint2` int(11) NOT NULL DEFAULT '0',
  `dataint3` int(11) NOT NULL DEFAULT '0',
  `dataint4` int(11) NOT NULL DEFAULT '0',
  `x` float NOT NULL DEFAULT '0',
  `y` float NOT NULL DEFAULT '0',
  `z` float NOT NULL DEFAULT '0',
  `o` float NOT NULL DEFAULT '0',
  `comments` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of spell_scripts
-- ----------------------------
INSERT INTO `spell_scripts` VALUES ('25650', '0', '6', '530', '0', '0', '0', '0', '0', '0', '0', '0', '-593.429', '4077.95', '93.8245', '5.32893', '');
INSERT INTO `spell_scripts` VALUES ('25652', '0', '6', '530', '0', '0', '0', '0', '0', '0', '0', '0', '-589.976', '4078.31', '143.258', '4.48305', '');
INSERT INTO `spell_scripts` VALUES ('41931', '0', '10', '11876', '180000', '0', '0', '0', '0', '0', '0', '0', '-348.231', '1763.85', '138.371', '4.42728', '');
INSERT INTO `spell_scripts` VALUES ('29395', '0', '10', '17035', '200000', '0', '0', '0', '0', '0', '0', '0', '-985.932', '4227.08', '42.4585', '1.5732', '');
INSERT INTO `spell_scripts` VALUES ('29395', '0', '10', '17035', '200000', '0', '0', '0', '0', '0', '0', '0', '-1076.14', '4176.77', '38.1325', '-0.872665', '');
INSERT INTO `spell_scripts` VALUES ('29395', '0', '10', '17035', '200000', '0', '0', '0', '0', '0', '0', '0', '-1097.43', '4250.01', '16.8586', '1.45762', '');
INSERT INTO `spell_scripts` VALUES ('29395', '0', '10', '17035', '200000', '0', '0', '0', '0', '0', '0', '0', '-1100.84', '4254.16', '16.1055', '0.0330392', '');
INSERT INTO `spell_scripts` VALUES ('29395', '0', '10', '17035', '200000', '0', '0', '0', '0', '0', '0', '0', '-1102.93', '4210.66', '55.6402', '-0.733038', '');
INSERT INTO `spell_scripts` VALUES ('29395', '0', '10', '17035', '200000', '0', '0', '0', '0', '0', '0', '0', '-1108.3', '4177.58', '40.9812', '0.163756', '');
INSERT INTO `spell_scripts` VALUES ('29395', '0', '10', '17035', '200000', '0', '0', '0', '0', '0', '0', '0', '-1116.22', '4181.1', '19.4384', '4.79727', '');
INSERT INTO `spell_scripts` VALUES ('29395', '0', '10', '17035', '200000', '0', '0', '0', '0', '0', '0', '0', '-1137.26', '4242.49', '14.0351', '4.87109', '');
INSERT INTO `spell_scripts` VALUES ('29395', '0', '10', '17035', '200000', '0', '0', '0', '0', '0', '0', '0', '-1141.49', '4209.96', '50.3676', '0.718242', '');
INSERT INTO `spell_scripts` VALUES ('29395', '0', '10', '17035', '200000', '0', '0', '0', '0', '0', '0', '0', '-1151.03', '4263.01', '13.9897', '3.04112', '');
INSERT INTO `spell_scripts` VALUES ('29395', '0', '10', '17035', '200000', '0', '0', '0', '0', '0', '0', '0', '-1167.66', '4214.58', '49.9546', '-2.46091', '');
INSERT INTO `spell_scripts` VALUES ('29395', '0', '10', '17035', '200000', '0', '0', '0', '0', '0', '0', '0', '-1199.55', '4115.92', '61.2455', '6.13243', '');
INSERT INTO `spell_scripts` VALUES ('29395', '0', '10', '17035', '200000', '0', '0', '0', '0', '0', '0', '0', '-1325.42', '4041.12', '116.713', '3.96866', '');
INSERT INTO `spell_scripts` VALUES ('29395', '0', '10', '17035', '200000', '0', '0', '0', '0', '0', '0', '0', '-1332.81', '4061.28', '116.803', '2.00124', '');
INSERT INTO `spell_scripts` VALUES ('39291', '0', '10', '22452', '600000', '0', '0', '0', '0', '0', '0', '0', '-3361.74', '5151.89', '-9.00056', '1.55138', '');
INSERT INTO `spell_scripts` VALUES ('37834', '2', '7', '10637', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '');
INSERT INTO `spell_scripts` VALUES ('37834', '2', '7', '10688', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '');
INSERT INTO `spell_scripts` VALUES ('37751', '0', '2', '159', '9', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '');
INSERT INTO `spell_scripts` VALUES ('37751', '1', '1', '373', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '');
INSERT INTO `spell_scripts` VALUES ('37751', '1', '4', '46', '33554434', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '');
INSERT INTO `spell_scripts` VALUES ('37752', '1', '5', '46', '33554434', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '');
INSERT INTO `spell_scripts` VALUES ('37752', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '');
INSERT INTO `spell_scripts` VALUES ('37752', '1', '1', '26', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '');
