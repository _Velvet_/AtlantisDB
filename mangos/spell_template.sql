/*
Navicat MySQL Data Transfer

Source Server         : Hometown
Source Server Version : 50523
Source Host           : localhost:3306
Source Database       : mangos

Target Server Type    : MYSQL
Target Server Version : 50523
File Encoding         : 65001

Date: 2013-09-26 22:19:32
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `spell_template`
-- ----------------------------
DROP TABLE IF EXISTS `spell_template`;
CREATE TABLE `spell_template` (
  `id` int(11) unsigned NOT NULL DEFAULT '0',
  `proc_flags` int(11) unsigned NOT NULL DEFAULT '0',
  `proc_chance` int(11) unsigned NOT NULL DEFAULT '0',
  `duration_index` int(11) unsigned NOT NULL DEFAULT '0',
  `effect0` int(11) unsigned NOT NULL DEFAULT '0',
  `effect0_implicit_target_a` int(11) unsigned NOT NULL DEFAULT '0',
  `effect0_radius_idx` int(11) unsigned NOT NULL DEFAULT '0',
  `effect0_apply_aura_name` int(11) unsigned NOT NULL DEFAULT '0',
  `effect0_misc_value` int(11) unsigned NOT NULL DEFAULT '0',
  `effect0_misc_value_b` int(11) unsigned NOT NULL DEFAULT '0',
  `effect0_trigger_spell` int(11) unsigned NOT NULL DEFAULT '0',
  `comments` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='MaNGOS server side spells';

-- ----------------------------
-- Records of spell_template
-- ----------------------------
INSERT INTO `spell_template` VALUES ('21387', '40', '15', '21', '6', '1', '0', '42', '0', '0', '21388', 'Melt-Weapon trigger aura related used by Ragnaros');
INSERT INTO `spell_template` VALUES ('23363', '0', '101', '21', '76', '18', '0', '0', '179804', '0', '0', 'Summon Drakonid Corpse Trigger');
INSERT INTO `spell_template` VALUES ('25192', '0', '101', '21', '76', '18', '0', '0', '180619', '0', '0', 'Summon Ossirian Crystal');
INSERT INTO `spell_template` VALUES ('26133', '0', '101', '21', '76', '18', '0', '0', '180795', '0', '0', 'Summon Sandworm Base');
INSERT INTO `spell_template` VALUES ('44920', '0', '101', '21', '6', '1', '0', '56', '24941', '0', '0', 'Model - Shattered Sun Marksman - BE Male Tier 4');
INSERT INTO `spell_template` VALUES ('44924', '0', '101', '21', '6', '1', '0', '56', '24945', '0', '0', 'Model - Shattered Sun Marksman - BE Female Tier 4');
INSERT INTO `spell_template` VALUES ('44928', '0', '101', '21', '6', '1', '0', '56', '24949', '0', '0', 'Model - Shattered Sun Marksman - Draenei Male Tier 4');
INSERT INTO `spell_template` VALUES ('44932', '0', '101', '21', '6', '1', '0', '56', '24953', '0', '0', 'Model - Shattered Sun Marksman - Draenei Female Tier 4');
INSERT INTO `spell_template` VALUES ('45158', '0', '101', '21', '6', '1', '0', '56', '25119', '0', '0', 'Model - Shattered Sun Warrior - BE Female Tier 4');
INSERT INTO `spell_template` VALUES ('45162', '0', '101', '21', '6', '1', '0', '56', '25123', '0', '0', 'Model - Shattered Sun Warrior - BE Male Tier 4');
INSERT INTO `spell_template` VALUES ('45166', '0', '101', '21', '6', '1', '0', '56', '25127', '0', '0', 'Model - Shattered Sun Warrior - Draenei Female Tier 4');
INSERT INTO `spell_template` VALUES ('45170', '0', '101', '21', '6', '1', '0', '56', '25131', '0', '0', 'Model - Shattered Sun Warrior - Draenei Male Tier 4');
INSERT INTO `spell_template` VALUES ('35906', '0', '101', '21', '28', '41', '8', '0', '20405', '64', '0', 'Summon Nether Charge front of the caster');
INSERT INTO `spell_template` VALUES ('35905', '0', '101', '21', '28', '43', '8', '0', '20405', '64', '0', 'Summon Nether Charge left of the caster');
INSERT INTO `spell_template` VALUES ('35904', '0', '101', '21', '28', '44', '8', '0', '20405', '64', '0', 'Summon Nether Charge right of the caster');
INSERT INTO `spell_template` VALUES ('35153', '0', '101', '21', '28', '42', '8', '0', '20405', '64', '0', 'Summon Nether Charge behind of the caster');
INSERT INTO `spell_template` VALUES ('34819', '0', '101', '21', '28', '41', '8', '0', '20078', '64', '0', 'Summon Summoned Bloodwarder Reservist front of the caster');
INSERT INTO `spell_template` VALUES ('34818', '0', '101', '21', '28', '43', '8', '0', '20078', '64', '0', 'Summon Summoned Bloodwarder Reservist left of the caster');
INSERT INTO `spell_template` VALUES ('34817', '0', '101', '21', '28', '44', '8', '0', '20078', '64', '0', 'Summon Summoned Bloodwarder Reservist right of the caster');
INSERT INTO `spell_template` VALUES ('34810', '0', '101', '21', '28', '42', '8', '0', '20083', '64', '0', 'Summon Summoned Bloodwarder Mender behind of the caster');
INSERT INTO `spell_template` VALUES ('37264', '0', '101', '21', '28', '18', '7', '0', '21729', '64', '0', 'Power Converters: Summon Electromental (from cata)');
INSERT INTO `spell_template` VALUES ('37278', '0', '101', '21', '28', '18', '1', '0', '21737', '64', '0', 'Power Converters: Summon Mini-Electromental (from cata)');
INSERT INTO `spell_template` VALUES ('37365', '0', '101', '21', '28', '18', '1', '0', '21757', '64', '0', 'Power Converters: Summon Big Flavor Electromental (from cata)');
