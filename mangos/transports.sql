/*
Navicat MySQL Data Transfer

Source Server         : Hometown
Source Server Version : 50523
Source Host           : localhost:3306
Source Database       : mangos

Target Server Type    : MYSQL
Target Server Version : 50523
File Encoding         : 65001

Date: 2013-09-26 22:19:41
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `transports`
-- ----------------------------
DROP TABLE IF EXISTS `transports`;
CREATE TABLE `transports` (
  `entry` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `name` text,
  `period` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`entry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='Transports';

-- ----------------------------
-- Records of transports
-- ----------------------------
INSERT INTO `transports` VALUES ('176495', 'Grom\'Gol Base Camp and Undercity', '315032');
INSERT INTO `transports` VALUES ('176310', 'Menethil Harbor and Auberdine', '241778');
INSERT INTO `transports` VALUES ('176244', 'Teldrassil and Auberdine', '309295');
INSERT INTO `transports` VALUES ('176231', 'Menethil Harbor and Theramore Isle', '230162');
INSERT INTO `transports` VALUES ('175080', 'Grom\'Gol Base Camp and Orgrimmar', '248990');
INSERT INTO `transports` VALUES ('164871', 'Orgrimmar and Undercity', '239334');
INSERT INTO `transports` VALUES ('20808', 'Ratchet and Booty Bay', '231236');
INSERT INTO `transports` VALUES ('177233', 'Forgotton Coast and Feathermoon Stronghold', '259751');
INSERT INTO `transports` VALUES ('181646', 'Azuremyst and Auberdine', '238707');
