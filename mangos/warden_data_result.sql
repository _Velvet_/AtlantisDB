/*
Navicat MySQL Data Transfer

Source Server         : Hometown
Source Server Version : 50523
Source Host           : localhost:3306
Source Database       : mangos

Target Server Type    : MYSQL
Target Server Version : 50523
File Encoding         : 65001

Date: 2013-09-26 22:19:45
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `warden_data_result`
-- ----------------------------
DROP TABLE IF EXISTS `warden_data_result`;
CREATE TABLE `warden_data_result` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `check` int(3) DEFAULT NULL,
  `data` tinytext,
  `str` tinytext,
  `address` int(8) DEFAULT NULL,
  `length` int(2) DEFAULT NULL,
  `result` tinytext,
  `comment` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1010 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of warden_data_result
-- ----------------------------
INSERT INTO `warden_data_result` VALUES ('1003', '243', '', '', '8100062', '2', '7541', 'air jump');
INSERT INTO `warden_data_result` VALUES ('1002', '243', '', '', '9208923', '5', 'C0854A3340', 'gravity');
INSERT INTO `warden_data_result` VALUES ('1001', '243', '', '', '8979979', '5', 'E04D62503F', 'Hyper speed');
INSERT INTO `warden_data_result` VALUES ('1000', '243', '', '', '4840352', '2', '558B', 'lua protection');
INSERT INTO `warden_data_result` VALUES ('1004', '243', null, null, '8095301', '2', '7513', 'Air Jump 2');
INSERT INTO `warden_data_result` VALUES ('1005', '243', null, null, '9208728', '4', 'BB8D243F', 'Wallclimb');
INSERT INTO `warden_data_result` VALUES ('1006', '243', null, null, '6518444', '2', 'DECA', 'Wallclimb 1');
INSERT INTO `warden_data_result` VALUES ('1007', '243', null, null, '8095954', '3', '8B4908 ', 'Teleport To Plane 2');
INSERT INTO `warden_data_result` VALUES ('1008', '243', null, null, '8095957', '3', '894808 ', 'Teleport To Plane');
INSERT INTO `warden_data_result` VALUES ('1009', '243', null, null, '8103107', '3', '894E3C', 'No Fall DMG');
